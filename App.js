import React from 'react';
import {TouchableOpacity,PermissionsAndroid} from 'react-native';
import {NavigationContainer, StackActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginView from './src/login';
import Dashboard from './src/dashdup';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { widthPercentageToDP as wp , heightPercentageToDP as hp  } from 'react-native-responsive-screen';
// import Battery from './src/battery';
// import Logread from './src/Logdata';
import { BleManager } from 'react-native-ble-plx';

const Stack = createStackNavigator ();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.manager = new BleManager();
  }

  render () {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={({navigation}) => ({
              title: '',
              headerTransparent: 'white',
              headerLeft: null,
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
