import React, {Component} from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  View,
  AsyncStorage,
  Modal,
  ActivityIndicator,
  Image,
  ImageBackground,
  Alert,
  Button,
  Linking,
  LogBox,
  PermissionsAndroid,
  Dimensions,
  BackHandler,
} from 'react-native';
import {Table, Row} from 'react-native-table-component';
import {BleManager} from 'react-native-ble-plx';
import {Buffer} from 'buffer';
import BackgroundTimer from 'react-native-background-timer';
import base64 from 'react-native-base64';
import {Card} from 'react-native-elements';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';
import RNFS from 'react-native-fs';
import Dialog from 'react-native-dialog';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Geolocation from '@react-native-community/geolocation';
import Toast from 'react-native-tiny-toast';
// import MapboxGL from '@react-native-mapbox-gl/maps';
import moment from 'moment';
import VideoPlayer from 'react-native-video-player';
import AutoScrolling from 'react-native-auto-scrolling';
import {request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import MovToMp4 from 'react-native-mov-to-mp4';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';

const windowWidth = Dimensions.get ('screen').width;
const windowHeight = Dimensions.get ('screen').height;

// if (Platform.OS == 'android') {
// MapboxGL.setWellKnownTileServer ('Mapbox');
// 

// } else {
//   MapboxGL.setWellKnownTileServer ('mapbox');
//   MapboxGL.setAccessToken (
//     'pk.eyJ1IjoiYWxjb3Jha2UiLCJhIjoiY2w4d3hqemV3MDE0bjN2bGgzYjlvODFrcCJ9.-o4l76OqxfQ1fZIJKZLx0g'
//   );
// }

var Spinner = require ('react-native-spinkit');

class Dashboard extends Component {
  constructor (props) {
    super (props);
    this.manager = new BleManager ();
    // this.camera = null;
    this.player = null;
    this.onProgress = this.onProgress.bind (this);
    this.onLoad = this.onLoad.bind (this);
    this.onBuffer = this.onBuffer.bind (this);
    this.state = {
      filetype:null,
      testtime:"",
     
      devlist: null,
      listshow: false,
      opacity: 0,
      vbat: 0,
      wholedata: [],
      original: null,
      bracpeak: 'NA',
      waittime: false,
      waitingtime: 5,
      pressoffset: 'NA',
      alcooffset: 'NA',
      pressed: null,
      imagebase64: null,
      cameratype: RNCamera.Constants.Type.front,
      deviceid: '',
      serviceUUID: '',
      characteristicsUUID: '',
      text1: null,
      onlogout: false,
      imagepost: false,
      logpost: false,
      viewlog: false,
      bluetoothState: '',
      logsavailable: false,
      currentstate: null,
      sources: null,
      showloader: false,
      showloaderss: false,
      sources1: null,
      showtext: null,
      devices: null,
      result: null,
      mydevice: null,
      isdis: false,
      scantext: 'START SCAN',
      loadertext: 'N/A',
      loadertextss: '',
      islist: false,
      blulist: {},
      lat: '0.000',
      lon: '0.000',
      lat1: '0.000',
      lon1: '0.000',
      isVisible: false,
      otaversion: null,
      imageDialog: false,
      showLoc: false,
      recorded: false,
      isRecording: false,
      display: 'flex',
      videoUrl: undefined,
      heightScaled: null,
      viewmsg:false,
      isVideoMode:false,
      type:''
    };
  }
  onLoadStart = () => {
    this.setState ({opacity: 1});
  };

  // onLoad = () => {
  //   this.setState ({opacity: 0});
  // };

  // onBuffer = ({isBuffering}) => {
  //   this.setState ({opacity: isBuffering ? 1 : 0});
  // };
  componentWillUnmount () {
    this.setState ({onlogout: true});
    this.manager.disable ();
  }

  componentWillMount () {
    this.requestLocationPermission ();
    NetInfo.fetch ().then (state => {
      if (state.isConnected) {
        this.setState({viewmsg:false})
        console.log ('please update unposted data here');
        this.postUnpostedlogs ();
      } else {
        this.setState({viewmsg:true})
        this.setState ({logpost: true});
        this.setState ({imagepost: true});
      }
    });
    this.manager.onStateChange (state => {
      this.setState ({
        scantext: state == 'PoweredOff'
          ? 'TURN ON BLUETOOTH'
          : state == 'PoweredOn' ? 'START SCAN' : this.state.scantext,
      });
      this.setState ({bluetoothState: state});
    }, true);
    LogBox.ignoreLogs (['new NativeEventEmitter']); // Ignore log notification by message
    LogBox.ignoreAllLogs (); //Ignore all log notifications
    // if (Platform.OS == 'ios') {
    //   MapboxGL.setTelemetryEnabled (false);
    // }
  }

  requestLocationPermission = async () => {
    if (Platform.OS == 'ios' && parseInt (Platform.Version, 10) > 12) {
      Geolocation.requestAuthorization ();
      Geolocation.getCurrentPosition (
        position => {
          this.setState ({
            lat: position.coords.latitude,
            lon: position.coords.longitude,
          });
        },
        error => {
          console.log (error.code, error.message);
        }
      );
      request (PERMISSIONS.IOS.CAMERA).then (cameraStatus => {
        this.setState ({
          isAuthorized: cameraStatus === RESULTS.GRANTED,
          isAuthorizationChecked: true,
        });
      });
    } else {
      try {
        const granted = await PermissionsAndroid.requestMultiple ([
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        ]).then (async result => {
          console.log (result);
          if (result['android.permission.ACCESS_FINE_LOCATION'] === 'granted') {
            RNAndroidLocationEnabler.promptForEnableLocationIfNeeded ({
              interval: 10000,
              fastInterval: 5000,
            })
              .then (data => {
                console.log ('enabled', data);
              })
              .catch (err => {
                BackHandler.exitApp ();
              });
            Geolocation.getCurrentPosition (
              position => {
                console.log (position);
                this.setState ({
                  lat: position.coords.latitude,
                  lon: position.coords.longitude,
                });
              },
              error => {
                console.log (error.code, error.message);
              }
            );
            console.log (this.state.lat, this.state.lon);
            console.log ('You can use Geolocation');
            return true;
          } else {
            console.log ('User refuse');
            BackHandler.exitApp ();
            console.log ('You cannot use Geolocation');
            return false;
          }
        });
      } catch (err) {
        return false;
      }
    }
  };
  takeVideo () {
    console.log("RECcccccc")
    console.log (this.state.isRecording);
    this.state.isRecording ? this.stopRecord () : this.saveVideo ();
    this.state.isRecording
      ? this.setState ({isRecording: false})
      : this.setState ({isRecording: true});
  }

  stopRecord () {
    console.log (this.state.isRecording, 'stop');
    var that = this;
    if (this.camera) {
      that.camera.stopRecording ();
    }
    that.setState ({isRecording: false});
  }

  async saveVideo () {
    console.log(this.state.isVideoMode,"///////")
    console.log (this.state.isRecording, 'start');
    if(this.state.isVideoMode){
    if (this.camera) {
      if (this.state.currentstate != '0x82') {
        console.log ('yes camera');
        if (this.camera) {
          const options = {
            maxDuration: 60,
            orientation: 'portrait',
            forceUpOrientation: true,
          };
          const data = await this.camera.recordAsync (options);
          this.setState ({recorded: false});
          //check for the test detect
          if (
            this.state.currentstate == '0x93' ||
            this.state.currentstate == '0x84'
          ) {
            console.log (data);
            this.setState ({imagebase64: data.uri});
            // await RNFS.readFile(data.uri, 'base64').then(res => {
            //     this.setState({ imagebase64: res });
            //   })
            //   .catch(err => {
            //     console.log(err);
            //   });
          }
        } else {
          setTimeout (() => {
            this.saveVideo ();
          }, 500);
        }
      } else {
        console.log ('notdone1');
      }
    } else {
      console.log ('notdone2');
      setTimeout (() => {
        this.saveVideo ();
      }, 500);
    }
  } else{
   console.log("cAPTURE IMAGE WHEN B.C")
  }
  }

  imgpost (val) {
    this.startLoading (`Uploading the video don't exit the app`);
    setTimeout (() => {
      this.setState ({imagebase64: null});
      this.stopLoading ();
    }, 2000);
    //     axios.post (
    //     'https://us-central1-alcobrakea200.cloudfunctions.net/brvideoPost/video',
    //     val,
    //   {
    //     headers: {
    //       'Content-Type': 'application/json',
    //       'Authorization':  'Basic ' + base64.encode ('alcobrake:gps')
    //   },
    // })
    //       .then (resp => {
    //         console.log ('videosuccess');
    //         this.startLoading (`Success Uploading Video`);
    //         setTimeout (() => {
    //           this.stopLoading ();
    //         }, 1000);

    //         this.setState ({imagebase64: null});
    //       })
    //       .catch (error => {
    //         this.stopLoading ();
    //         console.error (error);
    //       });
  }
  onConnect (item) {
    this.setState ({mydevice: item});
    this.setState ({text1: item.name});
    this.setState ({deviceid: item.id});
    this.setState ({devices: item});
    this.manager.stopDeviceScan ();
    const folderPath = RNFS.DocumentDirectoryPath + '/Devices';
    RNFS.mkdir (folderPath).catch (err => {
      console.log("Line 356");
      console.log (err);
    });
    const path = folderPath + '/' + item.name + '.txt';
    RNFS.writeFile (path, JSON.stringify (item), 'utf8')
      .then (success => {
        console.log ('Success');
        // this.setState ({isdis: true});
        Toast.show ('Please Wait...', {
          position: 350,
          containerStyle: {
            backgroundColor: 'black',
            width: wp ('70%'),
            height: hp ('10%'),
          },
          duration: 500,
          textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
          mask: true,
          maskStyle: {},
        });
      })
      .catch (err => {
        console.log("Line 378");
        console.log (err.message);
      });
    item
      .connect ({requestMTU: 512})
      .then (item => {
        this.autosetTime (item);
        return item.discoverAllServicesAndCharacteristics ();
      })
      //monitoring reciever
      .then (() => {
        this.manager.monitorCharacteristicForDevice (
          item.id,
          '000000ff-0000-1000-8000-00805f9b34fb',
          '0000ff01-0000-1000-8000-00805f9b34fb',
          (error, characteristic) => {
            if (error) {
              console.log("Line 395");
              console.log (error.message);
              return;
            }
            var buffer = Buffer.from (characteristic.value, 'base64');
            var dec = buffer.toString ('hex');
            this.setState ({islist: true});
            var str = '';
            for (var i = 0; i < dec.length; i += 2) {
              var v = parseInt (dec.substr (i, 2), 16);
              if (v) str += String.fromCharCode (v);
            }
            dec = str;
            console.log (dec, 'dec', this.state.text1);
            this.setcurrentState (dec);
          }
        );
      })
      .then (() => {
        this.manager.onDeviceDisconnected (item.id, (error, device) => {
          if (error) {
            console.log("Line 416");
            console.log (error);
          }
          console.log ('Device is disconnected');
          this.setState ({text1: 'Device Disconnected'});
          // setTimeout(() => {
          this.setState ({islist: false});
          this.setState ({viewlog: false});
          this.manager.stopDeviceScan ();
          this.setState ({blulist: {}});
          this.setState ({imageDialog: false});
          this.setState ({showLoc: false});
          this.setState ({isdis: false});
          this.stopLoading ();
          this.manager.disable ();
          // }, 1000)
        });
      });
  }
  scanAndConnect () {
    // 000000ee-0000-1000-8000-00805f9b34fb
    this.setState ({text1: 'Scanning...'});
    this.manager.startDeviceScan (
      null,
      {allowDuplicates: false},
      async (error, device) => {
        if (null) {
          console.log ('null');
          // this.manager.stopDeviceScan();
        }
        if (error) {
          console.log ('Error in scan=> ' + error);
          // this.manager.destroy();
          // setTimeout(() => {
          // this.setState ({isdis: true});
          // alert("unknown error")
          this.setState ({waittime: true});
          var counting = 5;
          var sInt = setInterval (() => {
            this.setState ({waitingtime: counting});
            Toast.show ('Please Wait...' + counting, {
              position: 350,
              containerStyle: {
                backgroundColor: 'black',
                width: wp ('70%'),
                height: hp ('10%'),
              },
              textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
              mask: true,
              maskStyle: {},
            });
            counting--;
            if (counting < 1) {
              this.setState ({isdis: false});
              this.setState ({waittime: false});
              clearInterval (sInt);
              counting = 5;
              this.manager.disable ();
            }
          }, 1000);
          this.setState ({text1: error.message});
          this.manager.stopDeviceScan ();
          // this.manager.disable()
          // }, 1000)
          return;
        }
        this.setState ({scantext: 'STOP SCAN'});
        if (device.name != null && device.name.startsWith ('ABB_')) {
          this.setState ({
            blulist: {
              ...this.state.blulist,
              [device.name]: device,
            },
          });
        }
        setTimeout (() => {
          this.manager.stopDeviceScan ();
          this.setState ({scantext: 'START SCAN'});
        }, 1000);
      }
    );
  }
  postUnpostedlogs () {
    //merge posted and unposted here
    RNFS.readDir (RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')
      .then (result => {
        //Passing Folders(devids) as array
        return Promise.all (result);
        // return Promise.all([RNFS.stat(result), result]);
      })
      .then (async statResult => {
        //looping array to get individual paths/folders
        var promised = statResult.map (async fpath => {
          //reading dirs to get file paths
          await RNFS.readFile (fpath.path, 'utf8').then (async content => {
            Toast.show (
              content.length > 0 ? 'Uploading logs...' : 'Files Uptodate',
              {
                position: 350,
                containerStyle: {
                  backgroundColor: 'black',
                  width: wp ('70%'),
                  height: hp ('10%'),
                },
                textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
                mask: true,
                maskStyle: {},
              }
            );
            var devid = fpath.name.split ('.')[0];
            var fulldata = JSON.parse (
              '[' + content.substring (1).toString () + ']'
            );
            if (fulldata.length > 0) {
              var promises = fulldata.map (async reg => {
                this.eventpost (reg);
              });
              const allLogins = await Promise.all (promises);
              if (allLogins) {
                const folderPath =
                  RNFS.DocumentDirectoryPath + '/LogFiles/Posted';
                RNFS.mkdir (folderPath).catch (err => {
                  console.log (err);
                });
                const path = folderPath + '/' + fulldata[0].devid + '.txt';
                if (RNFS.exists (path)) {
                  RNFS.appendFile (path, content, 'utf8')
                    .then (() => {
                      // this.eventpost(mytag);
                    })
                    .catch (error => {
                      console.log (error);
                    });
                } else {
                  RNFS.writeFile (path, content, 'utf8')
                    .then (() => {
                      // this.eventpost(mytag);
                    })
                    .catch (error => {
                      console.log (error);
                    });
                }
                //write Unlink
                console.log (devid + 'read completed');
                RNFS.unlink (fpath.path);
              }
            }
          });
        });
        const allLogins2 = await Promise.all (promised);
        if (allLogins2) {
          this.setState ({logpost: true});
          console.log ('all Log folders read success');
          // console.log(allLogins2);
          this.postUnpostedimages ();
        }
      });
  }
  postUnpostedimages () {
    //merge posted and unposted here
    RNFS.readDir (RNFS.DocumentDirectoryPath + '/Images/Unposted')
      .then (result => {
        console.log ('GOT RESULT', result, result[0].path);
        //Passing Folders(devids) as array
        return Promise.all ([RNFS.stat (result[0].path), result[0].path]);
        // return Promise.all (result);
        // return Promise.all([RNFS.stat(result), result]);
      })
      .then (async statResult => {
        //looping array to get individual paths/folders
        var promises2 = await statResult.map (async fpath => {
          //reading dirs to get file paths
          await RNFS.readFile (fpath.path, 'utf8').then (async statResult2 => {
            Toast.show (
              statResult2.length > 0 ? 'Uploading Images...' : 'Files Uptodate',
              {
                position: 350,
                containerStyle: {
                  backgroundColor: 'black',
                  width: wp ('70%'),
                  height: hp ('10%'),
                },
                textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
                mask: true,
                maskStyle: {},
              }
            );
            //looping filepaths
            var devid = fpath.name.split ('.')[0];
            var fulldata = JSON.parse (
              '[' + statResult2.substring (1).toString () + ']'
            );
            if (fulldata.length > 0) {
              var promises = await fulldata.map (async resp => {
                var imgtime = resp.time_stamp;
                var imgdata = {
                  devid: devid,
                  time_stamp: imgtime,
                  content: resp.content,
                };
                this.imgpost (imgdata);
              });
              const allLogins = await Promise.all (promises);
              if (allLogins) {
                const folderPath =
                  RNFS.DocumentDirectoryPath + '/Images/Posted';
                RNFS.mkdir (folderPath).catch (err => {
                  console.log (err);
                });
                const path = folderPath + '/' + fulldata[0].devid + '.txt';
                if (RNFS.exists (path)) {
                  RNFS.appendFile (path, statResult2, 'utf8')
                    .then (() => {
                      // this.eventpost(mytag);
                    })
                    .catch (error => {
                      console.log (error);
                    });
                } else {
                  RNFS.writeFile (path, statResult2, 'utf8')
                    .then (() => {
                      // this.eventpost(mytag);
                    })
                    .catch (error => {
                      console.log (error);
                    });
                }
                //write Unlink
                console.log (devid + 'read completed');
                RNFS.unlink (fpath.path);
              }
            }
          });
        });
      
        const allLogins3 = await Promise.all (promises2);
        if (allLogins3) {
          console.log ('Images Posted Success');
          this.setState ({imagepost: true});
        }
      });
  }
  sortDate (dateA, dateB, direction = 'asc') {
    const formats = ['DD-MM-YY HH:mm:ss']; // can be several
    return (
      (moment (dateA, formats).isBefore (moment (dateB, formats))
        ? -1
        : moment (dateA, formats).isAfter (moment (dateB, formats)) ? 1 : 0) *
      (direction === 'asc' ? 1 : -1)
    );
  }

  viewLogs () {
    console.log (this.state.text1);
    this.startLoading ('Loading Logs Please wait....');
    axios
      .get (
        'https://robopower.xyz/breathalyzer/tests/getdevicetests?devid=' +
          this.state.text1
      )
      .then (resp => {
        if (resp.data != null) {
          const array = resp.data;
          const data = array.sort ((a, b) =>
            this.sortDate (a.timestamp, b.timestamp, 'desc')
          );
          const data1 = data.filter (o => {
            return o.eventtype !== '';
          });
          this.setState ({wholedata: data1});
          // console.log(this.state.wholedata)
          this.setState ({viewlog: true});
          this.stopLoading ();
        } else {
          Alert.alert ('Logs Not Found');
          this.stopLoading ();
        }
      })
      .catch (error => {
        this.startLoading ('Error : ' + error.message);
        setTimeout (() => {
          this.stopLoading ();
        }, 3000);
      });
  }
  // viewLogs () {
  //   console.log ('hii');
  //   //merge posted and unposted here
  //   // var fdata = [];
  //   //Unposted list
  //   // if (RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')) {
  //   RNFS.mkdir (RNFS.DocumentDirectoryPath + '/LogFiles').catch (err => {
  //     console.log (err);
  //   });
  //   RNFS.mkdir (RNFS.DocumentDirectoryPath + '/LogFiles/Posted').catch (err => {
  //     console.log (err);
  //   });
  //   RNFS.mkdir (
  //     RNFS.DocumentDirectoryPath + '/LogFiles/Unposted'
  //   ).catch (err => {
  //     console.log (err);
  //   });
  //   RNFS.mkdir (
  //     RNFS.DocumentDirectoryPath + '/LogFiles/Unposted'
  //   ).catch (err => {
  //     console.log (err);
  //   });
  //   RNFS.readDir (RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')
  //     .then (async result => {
  //       // console.log('GOT RESULT', result);
  //       return await Promise.all (result);
  //     })
  //     .then (async res => {
  //       // console.log()
  //       var mypath = await res.filter (res => {
  //         if (res.name.includes (this.state.text1)) {
  //           return res;
  //         }
  //       });
  //       // })
  //       // console.log(mypath)
  //       if (mypath.length > 0) {
  //         RNFS.readFile (mypath[0].path, 'utf8').then (async content => {
  //           // console.log("unposted data")
  //           var unposteddata = content.substring (1);
  //           // console.log(unposteddata);
  //           // console.log(RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted'))
  //           //check for posted list with unposted list
  //           RNFS.mkdir (
  //             RNFS.DocumentDirectoryPath + '/LogFiles/Posted'
  //           ).catch (err => {
  //             console.log (err);
  //           });
  //           await RNFS.readDir (RNFS.DocumentDirectoryPath + '/LogFiles/Posted')
  //             .then (result => {
  //               return Promise.all (result);
  //             })
  //             .then (async res => {
  //               var mypath = await res.filter (res => {
  //                 if (res.name.includes (this.state.text1)) {
  //                   return res;
  //                 }
  //               });
  //               // })
  //               if (mypath.length > 0) {
  //                 RNFS.readFile (mypath[0].path, 'utf8')
  //                   .then (async content2 => {
  //                     // console.log("posted data")
  //                     var posteddata = content2.substring (1);
  //                     // console.log(JSON.parse('['+posteddata+']'))
  //                     this.setState ({
  //                       wholedata: JSON.parse (
  //                         '[' + posteddata + ',' + unposteddata + ']'
  //                       ),
  //                     });
  //                     // console.log(JSON.parse('[' + posteddata + ',' + unposteddata + ']'))
  //                     //  posteddata +','+ unposteddata
  //                   })
  //                   .then (() => {
  //                     this.setState ({viewlog: true});
  //                     console.log ('completed whole');
  //                   });
  //               } else {
  //                 Toast.show ('Logs Empty', {
  //                   position: 350,
  //                   containerStyle: {
  //                     backgroundColor: 'black',
  //                     width: wp ('70%'),
  //                     height: hp ('10%'),
  //                   },
  //                   textStyle: {
  //                     color: 'white',
  //                     fontSize: 17,
  //                     fontWeight: 'bold',
  //                   },
  //                   mask: true,
  //                   maskStyle: {},
  //                 });
  //                 console.log ('No logs in posted file');
  //               }
  //             });
  //         });
  //       } else {
  //         //check for Only postedlist
  //         // if (RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted')) {
  //         RNFS.readDir (RNFS.DocumentDirectoryPath + '/LogFiles/Posted')
  //           .then (result => {
  //             return Promise.all (result);
  //           })
  //           .then (async res => {
  //             var mypath = await res.filter (res => {
  //               if (res.name.includes (this.state.text1)) {
  //                 return res;
  //               }
  //             });
  //             // })
  //             if (mypath.length > 0) {
  //               RNFS.readFile (mypath[0].path, 'utf8')
  //                 .then (async content3 => {
  //                   // console.log("only posted list");
  //                   var onlyposted = content3.substring (1);
  //                   // console.log(onlyposted)
  //                   this.setState ({
  //                     wholedata: JSON.parse ('[' + onlyposted + ']'),
  //                   });
  //                   if (JSON.parse ('[' + onlyposted + ']').length == 0) {
  //                     Toast.show ('Logs Empty', {
  //                       position: 350,
  //                       containerStyle: {
  //                         backgroundColor: 'black',
  //                         width: wp ('70%'),
  //                         height: hp ('10%'),
  //                       },
  //                       textStyle: {
  //                         color: 'white',
  //                         fontSize: 17,
  //                         fontWeight: 'bold',
  //                       },
  //                       mask: true,
  //                       maskStyle: {},
  //                     });
  //                   }
  //                 })
  //                 .then (() => {
  //                   // console.log("completed only posted")
  //                   this.setState ({viewlog: true});
  //                 });
  //             } else {
  //               Toast.show ('Logs Empty', {
  //                 position: 350,
  //                 containerStyle: {
  //                   backgroundColor: 'black',
  //                   width: wp ('70%'),
  //                   height: hp ('10%'),
  //                 },
  //                 textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
  //                 mask: true,
  //                 maskStyle: {},
  //               });
  //               console.log ('No logs in posted file');
  //             }
  //           });
  //         // } else {
  //         //   console.log("Posted folder Not exists")
  //         // }
  //       }
  //     })
  //     // var whole = await promises.all()
  //     .catch (err => {
  //       console.log ('fff');
  //       console.log (err.message, err.code);
  //     });
  // }

  stopScanning () {
    if (this.state.scantext == 'START SCAN') {
      if (this.state.bluetoothState == 'PoweredOn') {
        this.setState ({blulist: {}});
        console.log ('powON');
        setTimeout (() => {
          this.scanAndConnect ();
        }, 1000);
      } else if (this.state.bluetoothState == 'PoweredOff') {
        console.log ('poff');
        if (Platform.OS == 'android') {
          this.manager.disable ();
          this.manager.enable ();
          // this.scanAndConnect();
        } else {
          Alert.alert (
            '"App" would like to use Bluetooth.',
            'This app uses Bluetooth to connect to and share information with your .....',
            [
              {
                text: "Don't allow",
                onPress: () => console.log ('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Turn ON',
                onPress: () => {
                  Linking.openURL ('App-prefs:root=Bluetooth');
                  // this.scanAndConnect();
                },
              },
            ]
          );
        }
      } else {
        console.log (this.state.bluetoothState);
        Toast.show ('Bluetooth is Resetting please wait', {
          position: 350,
          duration: 3000,
          containerStyle: {
            backgroundColor: 'black',
            width: wp ('70%'),
            height: hp ('8%'),
          },
          textStyle: {
            color: 'white',
            fontSize: 17,
            fontWeight: 'bold',
          },
          mask: true,
          maskStyle: {},
          duration: 3000,
        });
        this.manager.disable ();
      }
    } else if (this.state.scantext == 'TURN ON BLUETOOTH') {
      if (Platform.OS == 'android') {
        this.manager.enable ();
      } else {
        Alert.alert (
          '"App" would like to use Bluetooth.',
          'This app uses Bluetooth to connect to and share information with your .....',
          [
            {
              text: "Don't allow",
              onPress: () => console.log ('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'Turn ON',
              onPress: () => {
                Linking.openURL ('App-prefs:root=Bluetooth');
                // this.scanAndConnect();
              },
            },
          ]
        );
      }
    } else {
      this.manager.stopDeviceScan ();
      this.setState ({scantext: 'START SCAN'});
    }
  }
  handleSwitchChange = (value: boolean) => {
    console.log("switch:"+value)
    this.setState({ isVideoMode: value });
  };

  eventpost (val) {
    axios ({
      url: 'https://robopower.xyz/breathalyzer/tests',
      method: 'POST',
      data: JSON.stringify (val),
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then (resp => {
        // this.setState({currentstate:null})
      })
      .catch (error => console.error (error));
  }


  takePicture = async () => {
    // console.log(this.camera, "pppppp");
  
    if (this.camera) {
      try {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync(options);
        // console.log(`Saved at: ${data.uri}`);
        // console.log(`Base64 image: ${data.base64}`);
  
        let date = new Date();
        const currentdate = moment().format("DD/MM/YY HH:mm:ss")

        const base64Image = data.base64;
        this.setState({base64Image:base64Image})
        // const devid = this.state.deviceid;
      //   if (base64Image != null) {
      //     const payload = JSON.stringify({
      //       devid: this.state.text1,
      //       time_stamp: this.state.testtime,
      //       content: base64Image,
      //     });
        
      //     console.log(payload, "JSON payload");
        
      //     // Make API request to upload image (using axios.post)
        
      //     axios.post('https://robopower.xyz/breathalyzer/image', payload, { // Use axios.post for POST requests
      //       headers: {
      //         'Accept': 'application/txt', // Assuming you expect plain text response
      //         'Content-Type': 'application/json' // Correctly set for JSON payload
      //       }
      //     })
      //     .then(response => { // Handle successful response
      //       if (!response.ok) {
      //         console.log(' uploading image:', response.status);
      //         return;
      //       }
      //       console.log(response.status, "insideres", response);
      //       // Process successful response here (e.g., display success message)
      //     })
      //     .catch(error => { // Handle errors
      //       console.error('Error uploading image:', error);
      //       // Handle errors gracefully (e.g., display error message)
      //     });
      //   }else{
      //   console.log("image not found inidide triger event")
      // }  
      } catch (error) {
        console.error('Error taking picture:', error);
      }
    }
  };

  async captureImageWhileRecording() {
    console.log(this.camera,"capture")
    if (this.camera) {
      const imageOptions = { quality: 0.5, base64: true };
      const imageData = await this.camera.takePictureAsync(imageOptions);
      console.log('Captured image URI:', imageData.uri);
  
      // Set image URI or base64 data as needed
      this.setState({ imagebase64: imageData.base64 || imageData.uri });
    }
  }
  setcurrentState (val) {
    if (val) {
      if (val == '0x58') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({display: 'none'});
        this.setState ({currentstate: null});
        this.setState ({pressed: null});
      } else if (val == '0x59') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({display: 'flex'});
        this.setState ({currentstate: null});
        this.setState ({pressed: null});
      } else if (val == '0x81') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Please Blow'});
        // this.setState ({islist: false});
        this.setState ({viewlog: false});
        this.setState ({imageDialog: false});
        this.setState ({showLoc: false});
        console.log (this.state.pressed);
        if (this.state.pressed != 'calibrate') {
          console.log("Videomode"+ this.state.isVideoMode)
          if(this.state.isVideoMode){
          setTimeout (() => {
            this.takeVideo ();
          }, 500);
        }
        } else {
          this.setState ({
            sources: require ('../assets/images/BREATHALYZERS.png'),
          });
        }
      } else if (val == '0x93') {
        this.setState ({currentstate: val.toString ()});
        if (this.state.pressed != 'calibrate') {
          this.setState ({showtext: 'Blow Detected'});
          console.log ('take image');
          if(!this.state.isVideoMode){
          setTimeout (() => {
            this.takePicture ();
          }, 1000);
        }
        }
      } else if (val == '0x84') {
        this.setState ({currentstate: val.toString ()});
        if (this.state.pressed == 'calibrate') {
          this.setState ({showtext: 'Calibration'});
          this.setState ({
            sources: require ('../assets/images/calibrate.png'),
          });
        } else {
          this.setState ({showtext: 'Analysing'});
          setTimeout (() => {
            console.log ('take image');
            // if(this.camera){
            this.stopRecord ();
            // }
          }, 1000);
        }
      } else if (val == '0x85') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Calibrate the Device'});
        this.setState ({
          sources: require ('../assets/images/calibrate.png'),
        });
        setTimeout (() => {
          this.setState ({currentstate: null});
          this.setState ({pressed: null});
        }, 2000);
      } else if (val == '0x83') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Hard Blow'});
        this.setState ({
          sources: require ('../assets/images/hardblow1.png'),
        });
        setTimeout (() => {
          this.setState ({imagebase64: null});
          this.stopRecord ();
          this.setState ({currentstate: null});
          this.setState ({pressed: null});
        }, 2000);
      } else if (val == '0x86') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Blow Timeout'});
        this.setState ({sources: require ('../assets/images/timeout.png')});
        setTimeout (() => {
          this.setState ({imagebase64: null});
          this.setState ({currentstate: null});
          // this.setState ({pressed: null});
          this.stopRecord ();
        }, 1000);
      } else if (val == '0x87') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Calibration Done'});
        this.setState ({
          sources: require ('../assets/images/calibrationDone.png'),
        });
        setTimeout (() => {
          this.setState ({currentstate: null});
          this.setState ({pressed: null});
        }, 2000);
      } else if (val == '0x82') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Insufficient Volume'});
        this.setState ({
          sources: require ('../assets/images/In-sufficientvolume1.png'),
        });
        setTimeout (() => {
          this.setState ({currentstate: null});
          this.setState ({pressed: null});
          // if(this.camera){
          this.stopRecord ();
          // }
        }, 1000);
      } else if (val == '0x88') {
        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: 'Calibration Failed'});
        this.setState ({
          sources: require ('../assets/images/calibrationFail.png'),
        });
        setTimeout (() => {
          this.setState ({currentstate: null});
          this.setState ({pressed: null});
        }, 2000);
      } else if (val == '0x65') {
        // else if (val == '0x5') {
        //   this.setState ({currentstate: val.toString ()});
        //   this.setState ({
        //     sources1: require ('../assets/images/battery_5.png'),
        //   });
        //   this.setState ({currentstate: null});
        // } else if (val == '0x1E') {
        //   this.setState ({currentstate: val.toString ()});
        //   this.setState ({
        //     sources1: require ('../assets/images/battery_30.png'),
        //   });
        //   this.setState ({currentstate: null});
        // } else if (val == '0x46') {
        //   this.setState ({currentstate: val.toString ()});
        //   this.setState ({
        //     sources1: require ('../assets/images/battery_70.png'),
        //   });
        //   this.setState ({currentstate: null});
        // } else if (val == '0x64') {
        //   this.setState ({currentstate: val.toString ()});
        //   this.setState ({
        //     sources1: require ('../assets/images/battery_100.png'),
        //   });
        //   this.setState ({currentstate: null});
        // }

        this.setState ({currentstate: val.toString ()});
        this.setState ({showtext: ''});
        this.setState ({sources: ''});
        setTimeout (() => {
          this.setState ({islist: false});
          this.manager.stopDeviceScan ();
          this.setState ({blulist: {}});
          this.setState ({otaversion: null});
          this.manager.disable ();
        }, 1000);
      } else if (val.startsWith ('P') || val.startsWith ('F')) {
        this.setState ({currentstate: val.toString ()});
        if (val.startsWith ('P')) {
          var spl = val.substring (2) + '.00';
          this.setState ({showtext: 'Pass with BAC:' + spl});
          this.setState ({
            sources: require ('../assets/images/testpass.png'),
          });
          setTimeout (() => {
            this.setState ({currentstate: null});
            this.setState ({pressed: null});
          }, 5000);
        } else {
          var spl = val.substring (1);
          this.setState ({showtext: 'Fail wit BAC:' + spl});
          this.setState ({
            sources: require ('../assets/images/testfail.png'),
          });
          setTimeout (() => {
            this.setState ({currentstate: null});
            this.setState ({pressed: null});
          }, 5000);
        }
      } else if (val == '0x57') {
        //toast memory reset done
        // this.setState ({currentstate: val.toString()});
        setTimeout (() => {
          Toast.show ('Mem Set Done successfully ', {
            position: 350,
            containerStyle: {
              backgroundColor: 'black',
              width: wp ('70%'),
              height: hp ('8%'),
            },
            textStyle: {
              color: 'white',
              fontSize: 17,
              fontWeight: 'bold',
            },
            mask: true,
            maskStyle: {},
          });
        }, 1000);
      } else if (val == '0x54') {
        //toast memory reset done
        this.setState ({currentstate: val.toString ()});
        Toast.show ('Please Wait Wifi Connecting.....', {
          position: 350,
          containerStyle: {
            backgroundColor: 'black',
            width: wp ('70%'),
            height: hp ('8%'),
          },
          duration: 2000,
          textStyle: {
            color: 'white',
            fontSize: 17,
            fontWeight: 'bold',
          },
          mask: true,
          maskStyle: {},
        });
        this.setState ({currentstate: null});
      } else if (val == '0x56') {
        //toast memory reset done
        this.setState ({currentstate: val.toString ()});
        Toast.show ('Please Wait OTA Updating.....', {
          position: 350,
          containerStyle: {
            backgroundColor: 'black',
            width: wp ('70%'),
            height: hp ('8%'),
          },
          duration: 30000,
          textStyle: {
            color: 'white',
            fontSize: 17,
            fontWeight: 'bold',
          },
          mask: true,
          maskStyle: {},
        });
        this.setState ({currentstate: null});
      } else if (val == '0x55') {
        //no updates ack number
        //toast memory reset done
        // this.setState ({currentstate: val.toString()});
        Toast.show ('Already Up to Date', {
          position: 350,
          containerStyle: {
            backgroundColor: 'black',
            width: wp ('70%'),
            height: hp ('8%'),
          },
          textStyle: {
            color: 'white',
            fontSize: 17,
            fontWeight: 'bold',
          },
          mask: true,
          maskStyle: {},
          duration: 2000,
        });
        this.setState ({currentstate: null});
      } else {
        // console.log(val.startsWith('O'));
        // console.log(val.startsWith('o'))
        if (val.startsWith ('O')) {
          var spl = val.split (',');
          this.setState ({pressoffset: spl[0]});
          this.setState ({alcooffset: spl[1]});
        } else if (val.startsWith ('B')) {
          this.setState ({bracpeak: val});
          console.log (this.state.bracpeak, 'bracpeak');
          this.setState ({currentstate: null});
        } else if (val.startsWith ('V')) {
          this.setState ({vbat: val.substring (1)});
          console.log (this.state.vbat + 'vbattttttttttttt');
          this.setState ({currentstate: null});
        } else if (val.startsWith ('U')) {
          var ver = val.split ('U');
          this.setState ({otaversion: ver[1]});
          this.setState ({currentstate: null});
          console.log (this.state.otaversion);
        } else if (val.startsWith ('0x')) {
          this.setState ({currentstate: null});
        } else {
          // console.log("PPPPPPPPPPPPPPPPPPPPPPpp")
          if (this.state.pressed == 'logread') {
            var eve = val.split ('-');
            // console.log(eve);
            if (eve.length < 2) {
              if (this.state.logsavailable) {
                this.setState ({logsavailable: false});
                setTimeout (() => {
                  this.setState ({currentstate: null});
                  Toast.show ('Logs Reading Finished', {
                    position: 350,
                    containerStyle: {
                      backgroundColor: 'black',
                      width: wp ('70%'),
                      height: hp ('8%'),
                    },
                    textStyle: {
                      color: 'white',
                      fontSize: 17,
                      fontWeight: 'bold',
                    },
                    mask: true,
                    maskStyle: {},
                  });
                  console.log ('Log Read Finished');
                }, 2000);
              } else {
                this.setState ({logsavailable: false});
                Toast.show ('No Logs Found', {
                  position: 350,
                  containerStyle: {
                    backgroundColor: 'black',
                    width: wp ('70%'),
                    height: hp ('8%'),
                  },
                  textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
                  mask: true,
                  maskStyle: {},
                });
                setTimeout (() => {
                  this.setState ({currentstate: null});
                  console.log ('Log Read Finished');
                }, 2000);
              }
            } else {
              this.setState ({logsavailable: true});
              Toast.show ('Reading Logs Please Wait...', {
                position: 350,
                containerStyle: {
                  backgroundColor: 'black',
                  width: wp ('70%'),
                  height: hp ('8%'),
                },
                textStyle: {color: 'white', fontSize: 17, fontWeight: 'bold'},
                mask: true,
                maskStyle: {},
              });
              // alert("Log Read Finished")
              this.setState ({currentstate: null});
              var pdate =
                eve[1].padStart (2, 0) +
                '/' +
                eve[2].padStart (2, 0) +
                '/' +
                eve[3].padStart (2, 0) +
                ' ' +
                eve[4].padStart (2, 0) +
                ':' +
                eve[5].padStart (2, 0) +
                ':' +
                eve[6].padStart (2, 0);
              var mytag = {
                devid: this.state.text1,
                timestamp: pdate,
                bac: eve[8] == '255' ? 'NA' : eve[8],
                result: eve[7] == '1' ? 'PASS' : eve[7] == '2' ? 'FAIL' : 'NA',
                eventtype: eve[7] == '1' || eve[7] == '2'
                  ? 'BREATHE TEST'
                  : eve[7] == '3'
                      ? 'CALIBRATION DONE'
                      : eve[7] == '4'
                          ? 'INSUFFICIENT VOLUME'
                          : eve[7] == '5'
                              ? 'HARD BLOW'
                              : eve[7] == '6'
                                  ? 'LOG READ'
                                  : eve[7] == '7' ? 'LOG RESET' : null,
                lat: this.state.lat.toString (),
                lon: this.state.lon.toString (),
                bracpeakfactor: this.state.bracpeak,
                pressoffset: this.state.pressoffset,
                alcooffset: this.state.alcooffset,
              };

              // var obj = {'name':"sai1",'regnum':"APZ2"}
              //fetch data and parse to get this.array

              const isFound = this.state.wholedata.some (element => {
                if (
                  element.eventtype == mytag.eventtype &&
                  element.timestamp == mytag.timestamp
                ) {
                  return true;
                }

                return false;
              });
              if (isFound) {
                // if internet there post data and here insert into a file a posted file
                console.log ('yes there');
              } else {
                console.log ('not there');
                NetInfo.fetch ().then (state => {
                  if (state.isConnected) {
                    this.setState({viewmsg:false})
                    this.eventpost (mytag);
                  } else {
                    this.setState({viewmsg:true})
                    const mynewstring = mytag;
                    const folderPath =
                      RNFS.DocumentDirectoryPath + '/LogFiles/Unposted';
                    RNFS.mkdir (folderPath).catch (err => {
                      console.log (err);
                    });
                    const path = folderPath + '/' + this.state.text1 + '.txt';
                    if (RNFS.exists (path)) {
                      RNFS.appendFile (
                        path,
                        ',' + JSON.stringify (mynewstring),
                        'utf8'
                      )
                        .then (() => {
                          console.log ('written');
                        })
                        .catch (error => {
                          console.log (error);
                        });
                    } else {
                      RNFS.writeFile (
                        path,
                        JSON.stringify (mynewstring),
                        'utf8'
                      )
                        .then (() => {
                          console.log ('written');
                        })
                        .catch (error => {
                          console.log (error);
                        });
                    }
                  }
                });
              }
            }
          } else {
            console.log ('EVENT TRIGGERED');
            var eve = val.split ('-');
            console.log (eve);
            var pdate =
              eve[1].padStart (2, 0) +
              '/' +
              eve[2].padStart (2, 0) +
              '/' +
              eve[3].padStart (2, 0) +
              ' ' +
              eve[4].padStart (2, 0) +
              ':' +
              eve[5].padStart (2, 0) +
              ':' +
              eve[6].padStart (2, 0);
            var imgdata = {
              devid: this.state.text1,
              time_stamp: pdate,
              content: this.state.imagebase64,
            };
            this.setState({testtime:pdate});
            var mytag = {
              devid: this.state.text1,
              timestamp: pdate,
              bac: eve[8] == '255' ? 'NA' : eve[8],
              result: eve[7] == '1' ? 'PASS' : eve[7] == '2' ? 'FAIL' : 'NA',
              eventtype: eve[7] == '1' || eve[7] == '2'
                ? 'BREATHE TEST'
                : eve[7] == '3'
                    ? 'CALIBRATION DONE'
                    : eve[7] == '4'
                        ? 'INSUFFICIENT VOLUME'
                        : eve[7] == '5'
                            ? 'HARD BLOW'
                            : eve[7] == '6'
                                ? 'LOG READ'
                                : eve[7] == '7' ? 'LOG RESET' : null,
              lat: this.state.lat.toString (),
              lon: this.state.lon.toString (),
              bracpeakfactor: this.state.bracpeak,
              pressoffset: this.state.pressoffset,
              alcooffset: this.state.alcooffset,
            };
            const isFound = this.state.wholedata.some (element => {
              if (
                element.eventtype == mytag.eventtype &&
                element.timestamp == mytag.timestamp
              ) {
                return true;
              }

              return false;
            });
            if (isFound) {
              // if internet there post data and here insert into a file a posted file
              console.log ('yes there2');
            } else {
              console.log ('not there2');
              if(this.state.isVideoMode){
              if (imgdata.content != null) {
                // this.startLoading ('Saving file');
                Toast.show ('Saving file', {
                  position: 320,
                  containerStyle: {
                    backgroundColor: 'white',
                    // marginTop:wp('40%'),
                    width: wp ('80%'),
                    height: hp ('12%'),
                  },
                  textStyle: {color: 'green', fontSize: 18, fontWeight: 'bold'},
                  mask: true,
                  maskStyle: {},
                });
                const folderPath1 =
                  RNFS.DocumentDirectoryPath +
                  '/Images/Unposted' +
                  '/' +
                  imgdata.devid;
                RNFS.mkdir (folderPath1).catch (err => {
                  console.log("line 1564")
                  console.log (err);
                });
                var fpath = imgdata.time_stamp;
                fpath = fpath.replace ('/', '_');
                fpath = fpath.replace ('/', '_');
                fpath = fpath.replace (' ', '_');
                fpath = fpath.replace (':', '_');
                fpath = fpath.replace (':', '_');
                // var fpath4 = fpath3.replaceAll(":", "_");
                console.log (fpath);

                setTimeout (() => {
                  const path1 = folderPath1 + '/' + fpath + '.mp4';
                  RNFS.copyFile (this.state.imagebase64, path1).then (res => {
                    // this.stopLoading ();
                    this.setState ({imagebase64: null});
                  });
                }, 2000);
              }
            }
              NetInfo.fetch ().then (state => {
                if (state.isConnected) {
                  this.setState({viewmsg:false})
                  console.log("AAAAAAAAAAAAAAA")
                  console.log(imgdata);
                  //post image in background
                  if(this.state.isVideoMode){
                    console.log("entered inti videomode")
                  if (this.state.imagebase64 != null) {
                    var video = {
                      uri: this.state.imagebase64,
                      type: 'video/mp4',
                      name: fpath + '.mp4',
                    };
                    const formData = new FormData ();
                    formData.append ('file', video);
                    formData.append ('devid', imgdata.devid);
                    formData.append ('timestamp', imgdata.time_stamp);
                    this.startLoading (`data is Uploading don't exit app`);
                    const PromiseValue = new Promise ((resolve, reject) => {
                      BackgroundTimer.runBackgroundTimer (() => {
                        axios ({
                          url: 'https://robopower.xyz/breathalyzer/image/videoupload',
                          method: 'POST',
                          data: formData,
                          headers: {
                            'Content-Type': 'multipart/form-data',
                            Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
                          },
                        }).then (() => {
                          this.stopLoading ();
                          console.log ('uploaded');
                          // reset video parameters
                          this.setState({imagebase64:null});
                          this.setState({isVideoMode:false});
                          this.setState({base64Image:null});

                        });
                        resolve ();
                        reject ();
                      }, 3000);
                    });
                    PromiseValue.finally (() =>
                      BackgroundTimer.stopBackgroundTimer ()
                    );
                  } else {
                    console.log ('Video Not Found');
                  }
                } else{
                  if (this.state.base64Image != null) {
                    // if (this.state.base64Image != null) {
                      console.log("8888888888888888888888")
                      // this.startLoading ('Saving file');
                      Toast.show ('Saving file', {
                        position: 320,
                        containerStyle: {
                          backgroundColor: 'white',
                          width: wp ('80%'),
                          // marginTop:wp('40%'),
                          height: hp ('12%'),
                        },
                        textStyle: {color: 'green', fontSize: 18, fontWeight: 'bold'},
                        mask: true,
                        maskStyle: {},
                      });
                      const folderPath1 =
                        RNFS.DocumentDirectoryPath +
                        '/Images/Unposted' +
                        '/' +
                        this.state.text1;
                      RNFS.mkdir (folderPath1)
                      .catch (err => {
                        console.log (err,"errorrrrr");
                      });
                      var fpath = pdate;
                      fpath = fpath.replace ('/', '_');
                      fpath = fpath.replace ('/', '_');
                      fpath = fpath.replace (' ', '_');
                      fpath = fpath.replace (':', '_');
                      fpath = fpath.replace (':', '_');
                      // var fpath4 = fpath3.replaceAll(":", "_");
                      console.log (fpath);
      
                      setTimeout (() => {
                        console.log('line 1669')
                        console.log(folderPath1 + '/' + fpath + '.txt')
                        const path1 = folderPath1 + '/' + fpath + '.txt';
                        RNFS.copyFile (this.state.base64Image, path1).then (res => {
                          console.log(res+"resp")
                          // this.stopLoading ();
                          
                        }).catch((err)=>{
                          console.log("path err"+err)
                        })
                      }, 500);
                    // }
          const payload = JSON.stringify({
            devid: this.state.text1,
            time_stamp: pdate,
            content: this.state.base64Image,
          });
        
          console.log(payload, "JSON payload");
        
          // Make API request to upload image (using axios.post)
        
          axios.post('https://robopower.xyz/breathalyzer/image', payload, { // Use axios.post for POST requests
            headers: {
              'Accept': 'application/txt', // Assuming you expect plain text response
              'Content-Type': 'application/json' // Correctly set for JSON payload
            }
          })
          .then(response => { // Handle successful response
            console.log(response.status);
            console.log(response.data);

            if (response.status == 200) {
              // console.log()
              this.setState({imagebase64:null});
                          this.setState({isVideoMode:false});
                          this.setState({base64Image:null});
              console.log(' uploaded image:', response.status);
              // return;
            }
            // console.log(response.status, "insideres", response);
            // Process successful response here (e.g., display success message)
          })
          .catch(error => { // Handle errors
            console.error('Error uploading image:', error);
            // Handle errors gracefully (e.g., display error message)
          });
        }
        else{
        console.log("image not found inidide triger event")
      }  
                }
                  this.eventpost (mytag);
                } else {
                  this.setState({viewmsg:true})
                  console.log ('unimgpost', '0');
                  const mynewstring = mytag;
                  const folderPath =
                    RNFS.DocumentDirectoryPath + '/LogFiles/Unposted';
                  RNFS.mkdir (folderPath).catch (err => {
                    console.log (err,"errrorrr");
                  });
                  const path = folderPath + '/' + this.state.text1 + '.txt';
                  if (RNFS.exists (path)) {
                    RNFS.appendFile (
                      path,
                      ',' + JSON.stringify (mynewstring),
                      'utf8'
                    )
                      .then (() => {
                        console.log ('written');
                      })
                      .catch (error => {
                        console.log (error,"{{{{");
                      });
                  } else {
                    RNFS.writeFile (path, JSON.stringify (mynewstring), 'utf8')
                      .then (() => {
                        console.log ('written');
                      })
                      .catch (error => {
                        console.log (error);
                      });
                  }
                }
              });
            }
          }
        }
      }
    }
  }
  onPoweroff () {
    this.setState ({pressed: 'poweroff'});
    const device = this.state.devices;
    this.setState ({onlogout: true});
    const senddata = base64.encode ('92');
    this.manager
      .writeCharacteristicWithResponseForDevice (
        this.state.deviceid,
        '000000ff-0000-1000-8000-00805f9b34fb',
        '0000ff01-0000-1000-8000-00805f9b34fb',
        senddata
      )
      .then (characteristic => {
        // console.log(characteristic.value, 'characteristic');
        this.setState ({characteristicsUUID: characteristic.uuid});
        this.setState ({serviceUUID: characteristic.serviceUUID});
      });
    setTimeout (() => {
      // this.props.navigation.navigate('login')
      this.setState ({islist: false});
      this.manager.stopDeviceScan ();
      this.setState ({blulist: {}});
      this.setState ({otaversion: null});
      setTimeout (() => {
        if (Platform.OS == 'android') {
          this.manager.disable ();
        } else {
          Alert.alert (
            '"App" would like to turnoff the Bluetooth.',
            'This app like to disconnect Bluetooth to connected device and share information with your .....',
            [
              {
                text: 'Okay',
                onPress: () => {
                  Linking.openURL ('App-prefs:root=Bluetooth');
                  // this.scanAndConnect();
                },
              },
              // {
              //   text: 'Turn ON',
              //   onPress: () => {
              //     Linking.openURL ('App-prefs:root=Bluetooth');
              //     // this.scanAndConnect();
              //   },
              // },
            ]
          );
        }
      }, 1000);
    }, 2000);
  }
  onTaketest () {
    console.log ('datatest');
    this.setState ({pressed: 'taketest'});
    const senddata = base64.encode ('79');
    this.manager
      .writeCharacteristicWithResponseForDevice (
        this.state.deviceid,
        '000000ff-0000-1000-8000-00805f9b34fb',
        '0000ff01-0000-1000-8000-00805f9b34fb',
        senddata
      )
      .then (characteristic => {
        // console.log(characteristic.value, 'characteristic');
        this.setState ({characteristicsUUID: characteristic.uuid});
        this.setState ({serviceUUID: characteristic.serviceUUID});
        // console.log(senddata,'taketest');
      });
  }
  setTime () {
    this.setState ({pressed: 'timeset'});
    let d = new Date ();
    var dat =
      'T' +
      ('0' + d.getHours ()).slice (-2) +
      ':' +
      ('0' + d.getMinutes ()).slice (-2) +
      ':' +
      ('0' + d.getSeconds ()).slice (-2) +
      ' ' +
      ('0' + (d.getMonth () + 1)).slice (-2) +
      '/' +
      ('0' + d.getDate ()).slice (-2) +
      '/' +
      d.getFullYear ();
    let date = dat.toString ();
    const device = this.state.devices;
    this.manager.isDeviceConnected (this.state.deviceid).then (res => {
      if (res) {
        console.log ('tset');
        // console.log('Discovering services and characteristics test');
        device.discoverAllServicesAndCharacteristics ().then (device => {
          const senddata = base64.encode (date);
          this.manager
            .writeCharacteristicWithResponseForDevice (
              this.state.deviceid,
              '000000ff-0000-1000-8000-00805f9b34fb',
              '0000ff01-0000-1000-8000-00805f9b34fb',
              senddata
            )
            .then (characteristic => {
              // console.log(characteristic.value, 'characteristic');
              this.setState ({characteristicsUUID: characteristic.uuid});
              this.setState ({serviceUUID: characteristic.serviceUUID});
              // console.log(base64.decode(senddata),'settime');
            });
        });
      }
    });
  }
  autosetTime (item) {
    // this.setState({pressed:"timeset"})
    console.log ('time set');
    let d = new Date ();
    var dat =
      'T' +
      ('0' + d.getHours ()).slice (-2) +
      ':' +
      ('0' + d.getMinutes ()).slice (-2) +
      ':' +
      ('0' + d.getSeconds ()).slice (-2) +
      ' ' +
      ('0' + (d.getMonth () + 1)).slice (-2) +
      '/' +
      ('0' + d.getDate ()).slice (-2) +
      '/' +
      d.getFullYear ();
    let date = dat.toString ();
    // const device = this.state.devices;
    // console.log(date);
    this.manager.isDeviceConnected (item.id).then (res => {
      if (res) {
        console.log ('tset');
        // console.log('Discovering services and characteristics test');
        item.discoverAllServicesAndCharacteristics ().then (device => {
          const senddata = base64.encode (date);
          this.manager
            .writeCharacteristicWithResponseForDevice (
              item.id,
              '000000ff-0000-1000-8000-00805f9b34fb',
              '0000ff01-0000-1000-8000-00805f9b34fb',
              senddata
            )
            .then (characteristic => {
              // console.log(characteristic.value, 'characteristic');
              this.setState ({characteristicsUUID: characteristic.uuid});
              this.setState ({serviceUUID: characteristic.serviceUUID});
              // console.log(base64.decode(senddata),'settime');
            });
        });
      }
    });
  }

  calibration () {
    this.setState ({pressed: 'calibrate'});
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              const senddata = base64.encode ('80');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }

  Logread () {
    this.setState ({pressed: 'logread'});
    const senddata = base64.encode ('90');
    this.manager
      .writeCharacteristicWithResponseForDevice (
        this.state.deviceid,
        '000000ff-0000-1000-8000-00805f9b34fb',
        '0000ff01-0000-1000-8000-00805f9b34fb',
        senddata
      )
      .then (characteristic => {
        this.setState ({characteristicsUUID: characteristic.uuid});
        this.setState ({serviceUUID: characteristic.serviceUUID});
      });
  }
  memoryReset () {
    this.setState ({pressed: 'memset'});
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              const senddata = base64.encode ('91');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }

  OTAupdate () {
    this.setState ({pressed: 'otaupdate'});
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              const senddata = base64.encode ('66');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }
  startLoading (text) {
    this.setState ({showloader: true});
    this.setState ({loadertext: text});
  }
  stopLoading () {
    this.setState ({showloader: false});
    // this.setState ({loadertext: 'N/A'});
  }
  b64toBlob (b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = base64.decode (b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice (offset, offset + sliceSize);

      var byteNumbers = new Array (slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt (i);
      }

      var byteArray = new Uint8Array (byteNumbers);

      byteArrays.push (byteArray);
    }

    var blob = new Blob (byteArrays, {type: contentType});
    return blob;
  }
  // showImage2 (item, index) {
  //   console.log (item);
  //   // this.startLoading ('Loading Video Please Wait');
  //   // const date = item.timestamp.replace (/:/g, '_');
  //   // const dat = date.replace (/ /g, '_');
  //   // const da = dat.replace (new RegExp ('/', 'g'), '_');
  //   // console.log (da, 'timestamp');
  //   if (item) {
  //     // this.setState ({
  //     //   videoUrl:
  //     axios
  //       .get (
  //         'https://robopower.xyz/breathalyzer/images/getuniquevideo?devid=' +
  //           item.devid +
  //           '&timestamp=' +
  //           item.timestamp
  //       )
  //       .then (res => {
  //         if (res.data != null) {
  //           console.log (res.data[0].image);
  //           this.setState ({videoUrl: res.data[0].image});
  //         }
  //       });
  //     // });
  //     this.setState ({imageDialog: true});
  //     this.setState ({mystate: 'yesdata'});
  //     this.stopLoading ();
  //   } else {
  //     this.setState ({mystate: 'nodata'});
  //   }
  // }

  async showImage (item) {
    console.log("iiiiiiiiiiiiiiiii")
    console.log(item)
    console.log ('image loading please wait');
    if (Platform.OS == 'android') {
      this.setState ({showloaderss: true});
      this.setState ({loadertextss: 'Media Loading Please Wait......'});
      // console.log(item)
    } else {
      Toast.show ('Media Loading Please Wait......', {
        position: 320,
        duration:3000,
        containerStyle: {
          backgroundColor: 'white',
          width: wp ('80%'),
          height: hp ('12%'),
        },
        textStyle: {color: 'green', fontSize: 18, fontWeight: 'bold'},
        mask: true,
        maskStyle: {},
      });
    }
    setTimeout (() => {
      console.log("vvvv link")
      if(item.videolink != ""){
              axios
              .get (item.videolink
              )
              .then (res => {
                if (res) {
                  const contentType = res.headers['content-type'];
                  if(contentType == "image/jpeg"){
                    this.setState({filetype:"image"})
                    this.setState ({videoUrl: item.videolink});
                    this.setState ({imageDialog: true});
                    this.setState ({showloaderss: false});
                  } else if(contentType == "video/mp4"){
                    this.setState({filetype:"mp4"})
                    this.setState ({videoUrl: item.videolink});
                    this.setState ({imageDialog: true});
                    this.setState ({showloaderss: false});
                  } else{
                    // Invalid Format
                    Alert.alert ('Invalid Format Please Try Again!');

                  }
                }
              })
            } else{
              this.setState ({showloaderss: false});
              Alert.alert("No Media Found")
            }
    }, 500);
  }
  
  onLoad (data) {
    console.log ('On load fired!');
    console.log (data);
    // this.setState({duration: data.duration});
  }
  onProgress (data) {
    console.log ('when video is playing progress run');
  }
  onBuffer = ({isBuffering}) => {
    this.setState ({opacity: isBuffering ? 1 : 0});
    console.log ('this.state.opacity', isBuffering);
  };
  onVideoLoadStart (text) {
    console.log (text);
  }

  showmap (item, index) {
    console.log (item, 'item');
    // const lati = item.lat
    console.log (item.lat);
    if (item) {
      this.setState ({mystate: 'yesdata'});
      this.setState ({lat1: parseFloat (item.lat)}, () => {
        console.log (this.state.lat1, 'dealersOverallTotal1');
      });
      this.setState ({lon1: parseFloat (item.lon)}, () => {
        console.log (this.state.lon1, 'dealersOverallTotal1');
      });
      this.setState ({showLoc: true});
    } else {
      this.setState ({mystate: 'nodata'});
    }
  }

  devicelist () {
    this.setState ({listshow: true});
    RNFS.readDir (RNFS.DocumentDirectoryPath + '/Devices/')
      .then (result => {
        console.log ('GOT RESULT', result);
        this.setState ({devlist: result});
      })
      // .then (async statResult => {
      //   //looping array to get individual paths/folders
      //   var promised = statResult.map (async fpath => {
      //     console.log(fpath)
      //   })
      // })
      .catch (err => {
        console.log (err.message, err.code);
      });
  }

  onLogclose () {
    console.log (this.state.deviceid);
    this.setState ({wholedata: []});
    // if (this.state.bluetoothState != 'PoweredOn' && this.state.pressed == 'devices list') {
    //   this.setState ({islist: false});
    //   this.setState ({blulist: {}});
    //   this.setState ({otaversion: null});
    //   this.manager.stopDeviceScan ();
    //   setTimeout (() => {
    //     this.manager.disable ();
    //   }, 1000);
    // } else {
    //   if (this.state.text1) {
    //     this.setState ({viewlog: false});
    //   }
    // }
    if (this.state.pressed == 'devices list') {
      this.setState ({viewlog: false});
      this.setState ({islist: false});
      this.setState ({blulist: {}});
      this.setState ({otaversion: null});
    } else {
      this.setState ({viewlog: false});
    }
  }

  devLogs (item) {
    this.setState ({text1: item.name.substr (0, 21)});
    this.setState ({listshow: false});
    this.setState ({pressed: 'devices list'});
    this.startLoading ('Loading Logs Please wait....');
    if (Platform.OS == 'ios') {
      Toast.show ('Loading Logs Please wait....', {
        position: 320,
        duration: 4000,
        containerStyle: {
          backgroundColor: 'white',
          width: wp ('80%'),
          height: hp ('12%'),
        },
        textStyle: {color: 'green', fontSize: 18, fontWeight: 'bold'},
        mask: true,
        maskStyle: {},
      });
    }
    axios
      .get (
        'https://robopower.xyz/breathalyzer/tests/getdevicetests?devid=' +
          item.name.substr (0, 21)
      )
      .then (resp => {
        if (resp.data != null) {
          const array = resp.data;
          const data = array.sort ((a, b) =>
            this.sortDate (a.timestamp, b.timestamp, 'desc')
          );
          // console.log(data)
          const data1 = data.filter (o => {
            return o.eventtype !== '';
          });
          this.setState ({wholedata: data1});
          // console.log(this.state.wholedata,'devlogs')
          this.setState ({islist: true});
          this.setState ({viewlog: true});
          this.setState ({listshow: false});
          this.manager.disable ();
          this.stopLoading ();
        } else {
          Alert.alert ('Logs Not Found');
          this.stopLoading ();
        }
      })
      .catch (error => {
        this.startLoading ('Error : ' + error.message);
        setTimeout (() => {
          this.stopLoading ();
        }, 3000);
      });
  }
  

  render () {
    return (
      <View style={{flex: 1, backgroundColor: '#e8f5e9'}}>
        <View>
          <Dialog.Container
            visible={this.state.showloaderss}
            blurStyle={{backgroundColor: 'white'}}
            contentStyle={{
              backgroundColor: 'white',
              height: Platform.OS == 'android' ? hp ('12%') : hp ('10%'),
              width: wp ('80%'),
              justifyContent: 'center',
            }}
          >
            <Dialog.Title
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                color: 'green',
                fontWeight: 'bold',
                fontSize: 17,
              }}
            >
              {this.state.loadertextss}
            </Dialog.Title>

          </Dialog.Container>
        </View>
        <View>
          {/* <Modal
            visible={this.state.listshow}
            animationType="slide"
            transparent={true}
          > */}
          <Dialog.Container
            visible={this.state.listshow}
            blurStyle={{backgroundColor: 'white'}}
            contentStyle={{
              backgroundColor: 'white',
              height: hp ('60%'),
              width: wp ('80%'),
              justifyContent: 'center',
            }}
          >
            <Dialog.Title
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                color: 'green',
                fontWeight: 'bold',
                fontSize: Platform.OS == 'android' ? 20 : 17,
              }}
            >
              Devices List
            </Dialog.Title>
            {/* <Dialog.Description> */}
            <View
              style={{
                flex: 1,
                // backgroundColor: 'white',
                // borderRadius: 20,
                // alignItems: 'center',
                // maxHeight: hp ('70%'),
                // maxWidth: wp ('92%'),
                // marginRight: wp ('4%'),
                // marginTop: hp ('20%'),
              }}
            >
              {this.state.devlist != null
                ? <View style={{height: hp ('40%')}}>
                    <FlatList
                      data={Object.values (this.state.devlist)}
                      renderItem={({item}) => (
                        <TouchableHighlight
                          onPress={() => {
                            this.devLogs (item);
                          }}
                          style={styles.item}
                        >
                          <Text style={styles.title}>
                            {item.name.substr (0, 21)}
                          </Text>
                        </TouchableHighlight>
                      )}
                      keyExtractor={item => item.id}
                    />
                  </View>
                : <View>
                    <TouchableOpacity
                      style={{
                        // backgroundColor: 'black',
                        // height: hp ('9%'),
                        // width: wp ('17%'),
                        justifyContent: 'center',
                        alignSelf: 'center',
                        // borderRadius: 30,
                      }}
                    >
                      <Icon
                        name="playlist-remove"
                        size={50}
                        color={'white'}
                        style={{textAlign: 'center'}}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        color: 'black',
                        fontWeight: 'bold',
                        fontSize: 17,
                        textAlign: 'center',
                      }}
                    >
                      No Devices Found
                    </Text>
                  </View>}
              <TouchableOpacity
                onPress={() => {
                  this.setState ({listshow: false});
                }}
                style={{
                  width: wp ('18%'),
                  marginBottom: hp ('3%'),
                  marginTop: hp ('3%'),
                  height: Platform.OS == 'ios' ? hp ('7%') : hp ('5%'),
                  backgroundColor: '#f44336',
                  justifyContent: 'center',
                  borderRadius: 20,
                  alignSelf: 'center',
                }}
              >
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: Platform.OS == 'android' ? 17 : 15,
                  }}
                >
                  Close
                </Text>
              </TouchableOpacity>
            </View>
     
          </Dialog.Container>
        </View>
        {/* loading dialog */}
        <View>
     
          <Dialog.Container
            visible={this.state.showloader}
            blurStyle={{backgroundColor: 'white'}}
            contentStyle={{
              backgroundColor: 'white',
              height: Platform.OS == 'android' ? hp ('12%') : hp ('10%'),
              width: wp ('80%'),
            }}
          >
            <Dialog.Title
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                color: 'green',
                fontWeight: 'bold',
                fontSize: 17,
              }}
            >
              {this.state.loadertext}
            </Dialog.Title>
 
          </Dialog.Container>
        </View>
        <View>
   
          <Dialog.Container
            visible={this.state.imageDialog}
            blurStyle={{backgroundColor: 'white'}}
            contentStyle={{
              backgroundColor: 'white',
              height: hp ('80%'),
              width: wp ('80%'),
              justifyContent: 'center',
            }}
          >
            <Dialog.Title
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                color: 'green',
                fontWeight: 'bold',
                fontSize: 17,
              }}
            >
              BREATHE TEST
            </Dialog.Title>
            {/* <Dialog.Description> */}
            <View
              style={{
                // flex: 1,
                // justifyContent: 'center',
                alignItems: 'center',
        
              }}
            >
              {this.state.videoUrl && this.state.videoUrl != 'N/A'
                ? <View
           
                  >
              
                    <View>
      {this.state.filetype === 'mp4' ? (
        <VideoPlayer
          controls={false}
          thumbnail={{
            uri: 'https://i.picsum.photos/id/866/1600/900.jpg',
          }}
          videoWidth={windowWidth}
          videoHeight={windowHeight}
          disableControlsAutoHide={true}
          disableFullscreen={true}
          autoplay={true}
          video={{ uri: this.state.videoUrl }}
          onBuffer={this.onBuffer}
          onLoadStart={this.onLoadStart}
          onLoad={this.onLoad}
          ref={r => (this.player = r)}
        />
      ) : 
      (
        <Image
          source={{ uri: this.state.videoUrl }}
          style={{ width: 300, height: 300 }}
          resizeMode="contain"
        />
      )}
    </View>
          
                  </View>
                : <View>
                    <Icons
                      name="perm-media"
                      size={60}
                      color={'green'}
                      style={{
                        marginTop: hp ('10%'),
                        marginBottom: hp ('10%'),
                        justifyContent: 'center',
                        textAlign: 'center',
                      }}
                    />
                  </View>}
              <TouchableOpacity
                onPress={() => {
                  this.setState ({imageDialog: false});
                }}
                style={{
                  width: wp ('18%'),
                  marginBottom: hp ('2%'),
                  marginTop: hp ('2%'),
                  height: Platform.OS == 'ios' ? hp ('7%') : hp ('5%'),
                  backgroundColor: '#f44336',
                  justifyContent: 'center',
                  borderRadius: 20,
                  alignSelf: 'center',
                }}
              >
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: Platform.OS == 'android' ? 17 : 15,
                  }}
                >
                  Close
                </Text>
              </TouchableOpacity>

              
            </View>

            
            {/* </Dialog.Description> */}
            {/* <Dialog.Button
              label="Close"
              onPress={() => {
                this.setState ({imageDialog: false});
              }}
              style={{backgroundColor:'red',borderRadius:20,color:'white',fontWeight:'bold'}}
            /> */}
          </Dialog.Container>
        </View>
        <View>
          {/* <Modal
            visible={this.state.showLoc}
            animationType="slide"
            transparent={true}
          > */}
          <Dialog.Container
            visible={this.state.showLoc}
            blurStyle={{backgroundColor: 'white'}}
            contentStyle={{
              backgroundColor: 'white',
              height: hp ('70%'),
              width: wp ('80%'),
              justifyContent: 'center',
            }}
          >
            <Dialog.Title
              style={{
                justifyContent: 'center',
                alignSelf: 'center',
                color: 'green',
                fontWeight: 'bold',
                fontSize: 17,
              }}
            >
              EVENT LOCATION
            </Dialog.Title>
            {/* <Dialog.Description> */}
            <View
              style={{
                // flex: 1,
                // backgroundColor: 'white',
                // borderRadius: 20,
                alignItems: 'center',
                // maxHeight: hp ('20%'),
                // maxWidth: wp ('92%'),
                // marginLeft: wp ('-4%'),
              }}
            >
              {/* {this.state.mystate == 'yesdata'
                // ? <MapboxGL.MapView style={styles.map}>
                //     <MapboxGL.Camera
                //       zoomLevel={15}
                //       centerCoordinate={[this.state.lon1, this.state.lat1]}
                //     />
                //     <MapboxGL.PointAnnotation
                //       coordinate={[this.state.lon, this.state.lat]}
                //     >
                //       <MapboxGL.Callout>
                //         <View>
                //           <Text
                //             style={{
                //               backgroundColor: '#ffffff',
                //               color: 'purple',
                //               fontWeight: 'bold',
                //             }}
                //           >
                //             {this.state.devid}
                //           </Text>
                //         </View>
                //       </MapboxGL.Callout>
                //     </MapboxGL.PointAnnotation>
                //   </MapboxGL.MapView>
          
                : this.state.mystate == 'loading'
                    ? <View style={{flex: 1}}>
                        <Text
                          style={{
                            fontSize: 30,
                            color: 'green',
                            justifyContent: 'center',
                            marginTop: hp ('25%'),
                          }}
                        >
                          Loading...
                        </Text>
                      </View>
                    : <View style={{flex: 1, justifyContent: 'center'}}>
                        <Icon
                          name="map-marker-remove-variant"
                          size={30}
                          color={'red'}
                          style={{textAlign: 'center'}}
                        />
                      </View>} */}
              <TouchableOpacity
                onPress={() => {
                  this.setState ({showLoc: false});
                }}
                style={{
                  width: wp ('18%'),
                  marginBottom: hp ('3%'),
                  marginTop: hp ('3%'),
                  height: Platform.OS == 'ios' ? hp ('7%') : hp ('5%'),
                  backgroundColor: '#f44336',
                  justifyContent: 'center',
                  borderRadius: 20,
                  alignSelf: 'center',
                }}
              >
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontSize: Platform.OS == 'android' ? 17 : 15,
                  }}
                >
                  Close
                </Text>
              </TouchableOpacity>
            </View>
            {/* </Dialog.Description> */}
            {/* <Dialog.Button
              label="Close"
              onPress={() => {
                this.setState ({showLoc: false});
              }}
              style={{backgroundColor:'red',justifyContent:'center',alignSelf:'center',
              borderRadius:Platform.OS=='android'?20:50,color:'white',fontWeight:'bold'}}
            /> */}
          </Dialog.Container>
        </View>
        <View
          style={{
            justifyContent: 'center',
            height: hp ('10%'),
            backgroundColor: '#006064',
          }}
        >
          <View style={{flexDirection: 'row'}}>

            <Image
              style={{width: wp ('44%'), height: hp ('4.5%'), marginLeft: 20}}
              // source={require ('../assets/images/school1.png')}
              source={require ('../assets/images/alcobrake.jpg')}

            />
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 18,
                marginLeft: wp ('40%'),
              }}
            >
              {this.state.otaversion != null
                ? 'V' + this.state.otaversion
                : null}
            </Text>
          </View>
        </View>
        {this.state.islist
          ? <View style={{flex: 1, backgroundColor: '#e8f5e9'}}>
              {this.state.viewlog == false
                ? <View style={{flex: 1, backgroundColor: '#e8f5e9'}}>



          <View style={styles.controls}>
          <Text style={styles.text}>Photo Mode</Text>
          <Switch
            value={this.state.isVideoMode}
            onValueChange={this.handleSwitchChange}
            thumbColor="#f4f3f4"
            trackColor={{ false: '#81b0ff', true: '#81b0ff' }}
            style={styles.switch}
          />
          <Text style={styles.text}>Video Mode</Text>
        </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: hp ('2%'),
                      }}
                    >

                      <Text
                        style={{
                          fontSize: 17,
                          color: 'black',
                          fontWeight: 'bold',
                          marginLeft: wp ('3%'),
                        }}
                      >
                        CONNECTED TO:
                        {' '}
                        {this.state.bluetoothState == 'PoweredOn'
                          ? this.state.text1
                          : 'NOT CONNECTED'}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: hp ('2%'),
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          height: hp ('15%'),
                          width: wp ('15%'),
                          marginLeft: wp ('62%'),
                        }}
                        onPress={() => {
                          this.viewLogs ();
                        }}
                      >
                        <Icon
                          name={'book-information-variant'}
                          size={38}
                          style={{
                            color: '#ef5350',
                          }}
                        />
                      </TouchableOpacity>
                      <View
                        style={{
                          height: hp ('3%'),
                          width: wp ('13%'),
                          borderColor: '#000',
                          borderWidth: 2,
                          borderRadius: 5,
                          borderColor: '#000000',
                          overflow: 'hidden',
                          marginTop: hp ('1.5%'),
                          marginRight: wp ('5%'),
                        }}
                      >
                        <View
                          style={[
                            {
                              backgroundColor: this.state.vbat > 66 &&
                                this.state.vbat <= 100
                                ? '#8BED4F'
                                : this.state.vbat > 33 && this.state.vbat <= 66
                                    ? 'orange'
                                    : this.state.vbat <= 33 ? 'red' : null,
                              width: this.state.vbat + '%',
         
                              bottom: 0,
                              position: 'absolute',
                            },
                            {height: '100%'},
                          ]}
                        />
                        <Text style={{marginLeft: wp ('1%'), color: 'black'}}>
                          {this.state.vbat}%
                        </Text>
                      </View>
                    </View>
                    { this.state.viewmsg == true ?
                        <View
                          style={{
                            marginTop: hp ('-10%'),
                            justifyContent: 'center',
                            marginBottom: hp ('7%'),
                          }}
                        >
              
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: 'bold',
                              color: 'green',
                              textAlign: 'center',
                            }}
                          >
                            Please TurnOn Your Internet to view Logs ....
                          </Text>
                          {/* </AutoScrolling> */}
                        </View>:null
                      }
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}
                    >
                      {this.state.currentstate == null
                        ? <View
                            style={{
                              alignItems: 'center',
                              justifyContent: 'center',
                              padding: 10,
                            }}
                          >
                            <View
                              style={{
                                position: 'absolute',
                                zIndex: 1,
                                justifyContent: 'center',
                              }}
                            >
                              <Spinner
                                // style={styles.spinner}
                                isVisible={this.state.isVisible}
                                size={120}
                                type="Circle"
                                color="#006064"
                              />
                            </View>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: Platform.OS == 'ios'
                                  ? hp ('-10%')
                                  : hp ('-15'),
                              }}
                            >

                              <TouchableOpacity
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#ffb74d',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  marginRight: wp ('20%'),
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  display: this.state.display,
                                }}
                                onPress={() => this.onTaketest ()}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  TAKE-TEST
                                </Text>
                              </TouchableOpacity>
                              {/* <TouchableOpacity
                          onPress={() => this.setTime()}
                          style={{
                            justifyContent: 'center',
                            backgroundColor: '#9ccc65',
                            borderTopRightRadius: 60,
                            borderBottomLeftRadius: 60,
                            width: wp("50%"),
                            height: hp('8%'),
                            marginLeft: wp('20%'),
                            marginTop: hp('2.5%')
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 20,
                              color: 'white',
                              textAlign: 'center',
                              fontWeight: 'bold',
                            }}
                          >
                            TIME-SET
                          </Text>
                        </TouchableOpacity> */}
                              <TouchableOpacity
                                onPress={() => this.calibration ()}
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#81c784',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  marginLeft: wp ('20%'),
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  marginTop: hp ('2.5%'),
                                  display: this.state.display,
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  CALIBRATION
                                </Text>
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() => this.Logread ()}
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#ffab91',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  marginRight: wp ('20%'),
                                  marginTop: hp ('2.5%'),
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  LOG-READ
                                </Text>
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() => this.memoryReset ()}
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#ce93d8',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  marginLeft: wp ('20%'),
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  marginTop: hp ('2.5%'),
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  MEM-RESET
                                </Text>
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() => this.OTAupdate ()}
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#b39ddb',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  marginRight: wp ('20%'),
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  marginTop: hp ('2.5%'),
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  OTA-UPDATE
                                </Text>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{
                                  justifyContent: 'center',
                                  backgroundColor: '#80cbc4',
                                  borderTopRightRadius: 60,
                                  borderBottomLeftRadius: 60,
                                  marginLeft: wp ('20%'),
                                  width: wp ('50%'),
                                  height: hp ('8%'),
                                  marginTop: hp ('2.5%'),
                                }}
                                onPress={() => this.onPoweroff ()}
                              >
                                <Text
                                  style={{
                                    fontSize: 20,
                                    color: 'white',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  POWER-OFF
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        : <View
                            style={{
                              flex: 1,
                              alignItems: 'center',
                              justifyContent: 'center',
                              flexDirection: 'column',
                            }}
                          >
                            <View
                              style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                marginTop: Platform.OS == 'android'
                                  ? hp ('-1%')
                                  : hp ('2%'),
                              }}
                            >
                              <Text style={styles.titleText}>
                                {this.state.showtext}
                              </Text>
                              {(this.state.currentstate == '0x93' &&
                                this.state.pressed != 'calibrate') ||
                                (this.state.currentstate == '0x81' &&
                                  this.state.pressed != 'calibrate') ||
                                (this.state.currentstate == '0x84' &&
                                  this.state.pressed != 'calibrate')
                                ? <View
                                    style={{
                                      flex: 1,
                                      alignItems: 'center',
                                      flexDirection: 'column',
                                      marginTop: Platform.OS == 'ios'
                                        ? hp ('5%')
                                        : null,
                                    }}
                                  >
                                    <Text>
                                      <Spinner
                                        isVisible={true}
                                        size={
                                          Platform.OS == 'android' ? 120 : 100
                                        }
                                        type="Wave"
                                        color="green"
                                      />
                                    </Text>
                                    <View>
                                      {this.state.imagebase64 == null
                                        ? <View>

                                            <RNCamera
                                              ref={ref => (this.camera = ref)}
                                              style={{
                                                height: hp ('35%'),
                                                width: wp ('50%'),
                                                marginTop: hp ('3%'),
                                              }}
                                              type={this.state.cameratype}
                                              captureAudio={false}
                                              androidCameraPermissionOptions={{
                                                title: 'Permission to use camera',
                                                message: 'We need your permission to use your camera',
                                                buttonPositive: 'Ok',
                                                buttonNegative: 'Cancel',
                                              }}
                                            />
                                          </View>
                                        : <Image
                                            style={{
                                              height: hp ('35%'),
                                              width: wp ('50%'),
                                              marginTop: hp ('3%'),
                                            }}
                                            source={{
                                              uri: `data:image/jpg;base64,${this.state.imagebase64}`,
                                            }}
                                          />}
                                    </View>
                                  </View>
                                : <View
                                    style={{
                                      flex: 1,
                                      justifyContent: 'center',
                                      alignItems: 'center',
                                      marginTop: Platform.OS == 'android'
                                        ? hp ('-20%')
                                        : null,
                                    }}
                                  >
                                    <Image
                                      style={{
                                        width: wp ('70%'),
                                        height: hp ('60%'),
                                        marginTop: Platform.OS == 'ios'
                                          ? hp ('-10%')
                                          : null,
                                        resizeMode: 'contain',
                                      }}
                                      source={this.state.sources}
                                    />
                                    {/* </View>} */}
                                  </View>}
                            </View>
                          </View>}
                    </View>

                  </View>
                : <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        marginTop: hp ('2%'),
                        marginBottom: hp ('-2%'),
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          justifyContent: 'center',
                          backgroundColor: 'green',
                          borderRadius: 20,
                          height: Platform.OS == 'ios'
                            ? hp ('5%')
                            : hp ('3.5%'),
                          width: Platform.OS == 'ios' ? wp ('70%') : wp ('60%'),
                        }}
                      >
                        <Text
                          style={{
                            color: 'white',
                            fontSize: 17,
                            fontWeight: 'bold',
                            textAlign: 'center',
                          }}
                        >
                          {this.state.text1}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
                          this.onLogclose ();
                        }}
                        style={{
                          width: wp ('18%'),
                          height: Platform.OS == 'ios' ? hp ('7%') : hp ('5%'),
                          backgroundColor: '#f44336',
                          justifyContent: 'center',
                          borderRadius: 20,
                        }}
                      >
                        <Text
                          style={{
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                            fontSize: Platform.OS == 'android' ? 17 : 15,
                          }}
                        >
                          Close
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View>
                      {this.state.wholedata != []
                        ? <View style={{marginBottom: hp ('10%')}}>
                            <FlatList
                              style={styles.notificationList}
                              enableEmptySections={true}
                              data={this.state.wholedata}
                              keyExtractor={item => item.id}
                              extraData={this.state.wholedata}
                              renderItem={({item, index}) => {
                                return (
                                  <View
                                    style={{
                                      flex: 1,
                                      borderRadius: 10,
                                      height: Platform.OS == 'ios'
                                        ? hp ('25%')
                                        : hp ('17%'),
                                      justifyContent: 'center',
                                      flexDirection: 'row',
                                      backgroundColor: '#58ad9c',
                                      marginBottom: hp ('2%'),
                                    }}
                                  >
                                    <ScrollView horizontal={true}>
                                      <View style={styles.datatable}>
                                        <View style={{flexDirection: 'row'}}>
                                          <Text
                                            style={{
                                              color: 'black',
                                              fontSize: 15,
                                              fontWeight: 'bold',
                                              marginLeft: wp ('3%'),
                                            }}
                                          >
                                            EVENT
                                          </Text>
                                          <Text
                                            style={{
                                              color: 'black',
                                              marginLeft: wp ('23%'),
                                              fontWeight: 'bold',
                                            }}
                                          >
                                            TIME
                                          </Text>
                                          <Text
                                            style={{
                                              color: 'black',
                                              marginLeft: wp ('22%'),
                                              fontWeight: 'bold',
                                            }}
                                          >
                                            BAC
                                          </Text>
                                          <Text
                                            style={{
                                              color: 'black',
                                              marginLeft: wp ('22%'),
                                              fontWeight: 'bold',
                                            }}
                                          >
                                            RESULT
                                          </Text>
                                          <Text
                                            style={{
                                              color: 'black',
                                              marginLeft: wp ('26%'),
                                              fontWeight: 'bold',
                                              marginRight: wp ('4%'),
                                            }}
                                          >
                                            MEDIA
                                            {/* IMAGE */}
                                          </Text>
                                          <Text
                                            style={{
                                              color: 'black',
                                              marginLeft: wp ('22%'),
                                              fontWeight: 'bold',
                                              marginRight: wp ('2%'),
                                            }}
                                          >
                                            LOCATION
                                          </Text>
                                        </View>
                                        <View
                                          style={{
                                            flexDirection: 'row',
                                            marginBottom: 17,
                                          }}
                                        >
                                          <Text
                                            style={{
                                              color: 'black',
                                              fontSize: 15,
                                              marginLeft: wp ('3%'),
                                              marginTop: 5,
                                              flexShrink: 1,
                                              width: wp ('30%'),
                                            }}
                                            numberOfLines={7}
                                          >
                                            {item.eventtype}
                                          </Text>

                                          <Text
                                            style={{
                                              color: 'black',
                                              fontSize: 15,
                                              marginLeft: wp ('3%'),
                                              marginTop: 5,
                                              flexShrink: 1,
                                              width: wp ('30%'),
                                            }}
                                            numberOfLines={7}
                                          >
                                            {item.timestamp}
                                          </Text>

                                          <Text
                                            style={{
                                              color: 'black',
                                              fontSize: 15,
                                              marginLeft: Platform.OS == 'ios'
                                                ? wp ('6%')
                                                : wp ('6%'),
                                              marginTop: 5,
                                              flexShrink: 1,
                                              width: wp ('30%'),
                                            }}
                                            numberOfLines={7}
                                          >
                                            {item.bac}
                                          </Text>

                                          <Text
                                            style={{
                                              color: 'black',
                                              fontSize: 15,
                                              marginLeft: Platform.OS == 'ios'
                                                ? null
                                                : wp ('-1%'),
                                              marginTop: 5,
                                              flexShrink: 1,
                                              width: wp ('30%'),
                                            }}
                                            numberOfLines={7}
                                          >
                                            {item.result}
                                          </Text>
                                          {item.eventtype == 'BREATHE TEST'
                                            ? <TouchableOpacity
                                                style={{
                                                  marginLeft: Platform.OS ==
                                                    'ios'
                                                    ? wp ('9%')
                                                    : wp ('7%'),
                                                  marginRight: wp ('4%'),
                                                }}
                                                onPress={() => {
                                                  this.showImage (item, index);
                                                }}
                                              >
                                           
                                                <Icons
                                                  name="perm-media"
                                                  size={40}
                                                  color={'green'}
                                                />
                                              </TouchableOpacity>
                                            : <Icons
                                                name="perm-media"
                                                size={40}
                                                color={'green'}
                                              />}
                                          <TouchableOpacity
                                            style={{marginLeft: wp ('26%')}}
                                            onPress={() => {
                                              this.showmap (item, index);
                                            }}
                                          >
                                            <Icon
                                              name="map-marker-radius"
                                              size={40}
                                              color={'green'}
                                            />
                                          </TouchableOpacity>
                                        </View>
                                      </View>
                                    </ScrollView>
                                  </View>
                                );
                              }}
                            />
                          </View>
                        : <View>
                            <Spinner
                              isVisible={this.state.isVisible}
                              size={120}
                              type="Circle"
                              color="#006064"
                            />
                          </View>}
                    </View>
                  </View>}
            </View>
          : <View>
              <View>
              {/* <View style={styles.controls}>
              
          <Text style={styles.text}>Photo Mode</Text>
          <Switch
            value={this.state.isVideoMode}
            onValueChange={this.handleSwitchChange}
            thumbColor="#f4f3f4"
            trackColor={{ false: '#81b0ff', true: '#81b0ff' }}
            style={styles.switch}
          />
          <Text style={styles.text}>Video Mode</Text>
        </View> */}
                <TouchableOpacity
                  onPress={() => {
                    this.devicelist ();
                  }}
                  style={{
                    marginTop: hp ('2%'),
                    marginLeft: wp ('73%'),
                    backgroundColor: '#aed581',
                    height: hp ('7%'),
                    width: wp ('30%'),
                    justifyContent: 'center',
                    borderTopLeftRadius: 30,
                  }}
                >
                  <Text
                    style={{
                      color: 'black',
                      textAlign: 'center',
                      fontWeight: 'bold',
                    }}
                  >
                    Devices List
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{marginTop: hp ('5%')}}>
                
                {/* <TouchableOpacity disabled={this.state.isdis} onPress={() => { this.postUnpostedlogs() }}> */}
                {/* <TouchableOpacity disabled={this.state.isdis} onPress={() => { this.viewLogs() }}> */}
                <TouchableOpacity
                  disabled={this.state.isdis}
                  onPress={() => {
                    this.stopScanning ();
                  }}
                  style={{
                    backgroundColor: this.state.scantext == 'TURN ON BLUETOOTH'
                      ? 'steelblue'
                      : this.state.scantext == 'START SCAN'
                          ? '#689f38'
                          : '#e53935',
                    justifyContent: 'center',
                    borderRadius: 70,
                    width: Platform.OS == 'android' ? wp ('35%') : wp ('38%'),
                    alignSelf: 'center',
                    height: Platform.OS == 'android' ? hp ('17%') : hp ('22%'),
                    marginTop: hp ('-7%'),
                  }}
                >
                  <Text
                    style={{
                      textAlign: 'center',
                      color: 'white',
                      fontWeight: 'bold',
                      fontSize: 17,
                    }}
                  >
                    {this.state.scantext}
                  </Text>
                </TouchableOpacity>
                
              </View>
              <Text style={{color: 'black'}}>{this.state.original}</Text>
              <View style={{height: hp ('50%')}}>
                <FlatList
                  data={Object.values (this.state.blulist)}
                  renderItem={({item}) => (
                    <TouchableHighlight
                      // disabled={this.state.isdis}
                      onPress={() => {
                        this.onConnect (item);
                      }}
                      style={styles.item}
                    >
                      <Text style={styles.title}>{item.name}</Text>
                    </TouchableHighlight>
                  )}
                  keyExtractor={item => item.id}
                />
              </View>
            </View>}
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  map: {
    height: hp ('47%'),
    width: wp ('70%'),
    // marginTop: hp ('2%'),
  },
  notificationList: {
    marginTop: 20,
    padding: 10,
  },

  columnRowTxt: {
    width: '20%',
    textAlign: 'center',
    color: 'black',
  },
  tableRow: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
  head: {height: 40, backgroundColor: '#f1f8ff'},
  text: {
    margin: 6,
    textAlign: 'center',
    justifyContent: 'center',
    color: 'black',
  },
  item: {
    backgroundColor: '#757575',
    padding: 20,
    marginVertical: 8,
    // marginHorizontal: 16,
    width: wp ('70%'),
    borderRadius: 20,
    alignSelf: 'center',
  },
  listing: {
    backgroundColor: 'teal',
    padding: 20,
    marginVertical: 2,
    marginHorizontal: 4,
    marginLeft: 10,
    color: 'red',
  },
  title: {
    fontSize: 17,
    color: 'white',
    textAlign: 'center',
  },

  head: {height: 40, backgroundColor: '#f1f8ff'},
  text: {
    margin: 6,
    textAlign: 'center',
    justifyContent: 'center',
  },
  activityIndicator: {
    position: 'absolute',
    top: 50,
    left: 55,
    right: 70,
    height: 50,
    color: 'green',
  },
  titleText: {
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color: 'green',
    marginTop: Platform.OS == 'android' ? 22 : null,
  },
  datatable: {
    justifyContent: 'center',
    alignSelf: 'center',
    height: Platform.OS == 'ios' ? hp ('17%') : hp ('12%'),
    backgroundColor: '#E5E7E9',
    borderRadius: 10,
    marginLeft: wp ('2%'),
    marginRight: wp ('2%'),
  },
  controls: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderRadius: 8,
    backgroundColor: '#e6ffe6',
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
    justifyContent:'center'
  },
  switch: {
    marginRight: 10,
  },
  text: {
    fontSize: 16,
    fontWeight: '500',
    color: '#333333',
  },
});

export default Dashboard;
