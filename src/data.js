import React, { Component } from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  View,
  AsyncStorage,
  Modal,
  ActivityIndicator,
  Image,
  ImageBackground,
  Alert,
  Button,
  Linking,
  LogBox,
  PermissionsAndroid,
} from 'react-native';
import { Table, Row } from 'react-native-table-component';
import { BleManager } from 'react-native-ble-plx';
import { Buffer } from 'buffer';
import base64 from 'react-native-base64';
// import { Camera } from 'expo-camera';
import { Card } from 'react-native-elements';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';
import RNFS from 'react-native-fs';
// import * as ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-image-crop-picker';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { set } from 'react-native-reanimated';
import Geolocation from '@react-native-community/geolocation';
import Toast from 'react-native-tiny-toast';
import { Dialog } from 'react-native-elements';
import MapboxGL from '@rnmapbox/maps';

MapboxGL.setWellKnownTileServer('Mapbox');
MapboxGL.setAccessToken(
  'pk.eyJ1IjoiYWxjb3Jha2UiLCJhIjoiY2w4d3hqemV3MDE0bjN2bGgzYjlvODFrcCJ9.-o4l76OqxfQ1fZIJKZLx0g'
);



var Spinner = require('react-native-spinkit');

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.manager = new BleManager();
    this.state = {
      fullLogs: [],
      wholedata: [],
      original: null,
      logtext: null,
      widthArr: [200, 100, 150, 150, 200],
      bracpeak: "NA",
      waittime: false,
      waitingtime: 5,
      pressoffset: "NA",
      alcooffset: "NA",
      pressed: null,
      camera: null,
      imagebase64: null,
      cameraPermission: null,
      cameratype: RNCamera.Constants.Type.front,
      imageArray: [],
      deviceid: '',
      serviceUUID: '',
      characteristicsUUID: '',
      text1: null,
      makedata: [],
      onlogout: false,
      showToast: false,
      imagepost: false,
      logpost: false,
      viewlog: false,
      notificationReceiving: false,
      bluetoothState: '',
      logsavailable: false,
      currentstate: null,
      sources: null,
      sources1: null,
      showtext: null,
      devices: null,
      result: null,
      image: null,
      images: null,
      mydevice: null,
      isdis: false,
      scantext: 'START SCAN',
      video: '',
      isVideo: true,
      islist: false,
      blulist: {},
      videoSource: '',
      isFaceDetected: false,
      filepath: {
        data: '',
        uri: '',
      },
      fileData: '',
      fileUri: '',
      arr: [],
      count: 1,
      lat: '0.000',
      lon: '0.000',
      lat1: '0.000',
      lon1: '0.000',
      imageDate: null,
      isVisible: false,
      logRead: null,
      Content: [],
      ImgDataarr: [],
      otaversion: null,
      tableHead: [{ "timestamp": 'TIME'.padEnd(15, " "), "bac": 'BAC'.padEnd(10, " "), "result": 'RESULT'.padEnd(20, " "), "eventtype": 'EVENT'.padEnd(20, " "), "lat": 'LAT'.padEnd(30, " "), "lon": 'LON' }],
      imageDialog: false,
      showLoc: false,
      imgbase64: null
    };
  }
  componentWillUnmount() {
    this.setState({ onlogout: true })
    this.manager.disable();
  }

  componentWillMount() {
    this.requestLocationPermission()
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        // Toast.show('Uploading logs...', {
        //   position: 350,
        //   containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('8%') },
        //   textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
        //   mask: true,
        //   maskStyle: {},
        // })
        // please update unposted data here
        console.log("please update unposted data here");
        this.postUnpostedlogs();
      } else {
        this.setState({ logpost: true })
        this.setState({ imagepost: true })
      }
    })
    this.manager.onStateChange(state => {
      this.setState({ scantext: state == "PoweredOff" ? "TURN ON BLUETOOTH" : state == "PoweredOn" ? "START SCAN" : this.state.scantext })
      this.setState({ bluetoothState: state });
    }, true)
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
    LogBox.ignoreAllLogs();
  }

  requestLocationPermission = async () => {
    if (Platform.OS == 'ios' && parseInt(Platform.Version, 10) > 12) {
      Geolocation.requestAuthorization();
    } else {
      try {
        const granted = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
          PermissionsAndroid.PERMISSIONS.CAMERA,
        ]).then(result => {
          if (result['android.permission.ACCESS_FINE_LOCATION'] === granted) {
            Geolocation.getCurrentPosition(
              position => {
                this.setState({
                  lat: position.coords.latitude,
                  lon: position.coords.longitude,
                });
              },
              error => {
                console.log(error.code, error.message);
              },
            );
            console.log('You can use Geolocation');
            return true;
          } else {
            console.log('User refuse');
            BackHandler.exitApp();
            console.log('You cannot use Geolocation');
            return false;
          }

        })

      } catch (err) {
        return false;
      }
    }
  };


  takePicture = async () => {
    var d = new Date();
    var date =
      ('0' + (d.getMonth() + 1)).slice(-2) +
      '/' +
      ('0' + d.getDate()).slice(-2) +
      '/' +
      d.getFullYear() +
      ' ' +
      ('0' + d.getHours()).slice(-2) +
      ':' +
      ('0' + d.getMinutes()).slice(-2) +
      ':' +
      ('0' + d.getSeconds()).slice(-2);

    var fname =
      ('0' + (d.getMonth() + 1)).slice(-2) +
      '_' +
      ('0' + d.getDate()).slice(-2) +
      '_' +
      d.getFullYear() +
      '_' +
      ('0' + d.getHours()).slice(-2) +
      '_' +
      ('0' + d.getMinutes()).slice(-2) +
      '_' +
      ('0' + d.getSeconds()).slice(-2);
    this.setState({ imageDate: date });
    console.log(this.state.text1 + '_' + fname)
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      try {
        const data = await this.camera.takePictureAsync(options);
        this.setState({ imagebase64: data.base64 });
        // var imgdata = {
        //   devid: this.state.text1,
        //   time_stamp: this.state.imageDate,
        //   content: this.state.imagebase64,
        // };
        // NetInfo.fetch().then(state => {
        //   if (state.isConnected) {
        //     console.log('imgpost', "0")
        //     const folderPath =
        //       RNFS.DocumentDirectoryPath + '/Images/Posted';
        //     RNFS.mkdir(folderPath).catch(err => {
        //       console.log(err);
        //     });
        //     console.log('imgpost', "2")
        //     const mynewstring = imgdata;
        //     const path = folderPath + '/' + this.state.text1 + '.txt';
        //     if (RNFS.exists(path)) {
        //       RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
        //         .then(() => {
        //           this.imgpost(imgdata);
        //         })
        //         .catch(error => {
        //           console.log(error);
        //         });
        //     } else {
        //       RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
        //         .then(() => {
        //           this.imgpost(imgdata);
        //         })
        //         .catch(error => {
        //           console.log(error);
        //         });
        //     }
        //   } else {
        //     console.log('unimgpost', "0")
        //     const folderPath =
        //       RNFS.DocumentDirectoryPath + '/Images/Unposted';
        //     RNFS.mkdir(folderPath).catch(err => {
        //       console.log(err);
        //     });
        //     var mynewstring = imgdata
        //     const path = folderPath + '/' + this.state.text1 + '.txt';
        //     if (RNFS.exists(path)) {
        //       RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
        //         .then(() => {
        //           console.log("written")
        //           this.setState({ imagebase64: null })
        //         })
        //         .catch(error => {
        //           console.log(error);
        //         });
        //     } else {
        //       RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
        //         .then(() => {
        //           console.log("written")
        //           this.setState({ imagebase64: null })
        //         })
        //         .catch(error => {
        //           console.log(error);
        //         });
        //     }
        //   }
        // })

      } catch (err) {
        Toast.show('Image Captured Failed', {
          position: 350,
          containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
          textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
          mask: true,
          maskStyle: {},
        })
        console.log(
          'Error',
          'Failed to take picture: ' + (err.message || err)
        );
        return;
      }
    } else {
      console.log("Camera Not Open")
    }
  };

  imgpost(val) {
    axios({
      url: 'https://robopower.xyz/breathalyzer/image',
      method: 'POST',
      data: val,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then(resp => {
        console.log('imagesuccess')
        setTimeout(() => {
          this.setState({ imagebase64: null });
        }, 1000)
      })
      .catch(error => console.error(error));
  }
  onConnect(item) {
    this.manager.stopDeviceScan();
    this.setState({ mydevice: item });
    this.setState({ text1: item.name });
    this.setState({ deviceid: item.id });
    this.setState({ devices: item });
    item.connect({ requestMTU: 512 })
      .then((item) => {
        this.autosetTime(item);
        return item.discoverAllServicesAndCharacteristics()
      })
      //monitoring reciever
      .then(() => {

        this.manager.monitorCharacteristicForDevice(item.id,
          '000000ff-0000-1000-8000-00805f9b34fb',
          '0000ff01-0000-1000-8000-00805f9b34fb',
          (error, characteristic) => {
            if (error) {
              console.log(error.message);
              return;
            }
            var buffer = Buffer.from(
              characteristic.value,
              'base64'
            )
            var dec = buffer.toString('hex');
            this.setState({ islist: true })
            var str = '';
            for (var i = 0; i < dec.length; i += 2) {
              var v = parseInt(dec.substr(i, 2), 16);
              if (v) str += String.fromCharCode(v);
            }
            dec = str;
            this.setcurrentState(dec);
          })
      }).then(() => {
        this.manager.onDeviceDisconnected(item.id, (error, device) => {
          if (error) {
            console.log(error);
          }
          console.log('Device is disconnected');
          this.setState({ text1: 'Device Disconnected' });
          // setTimeout(() => {
          this.setState({ islist: false })
          this.manager.stopDeviceScan();
          this.setState({ blulist: {} })
          // }, 1000)
        })
      })
  }
  scanAndConnect() {
    // 000000ee-0000-1000-8000-00805f9b34fb
    this.setState({ text1: 'Scanning...' });
    this.manager.startDeviceScan(
      null,
      { allowDuplicates: false },
      async (error, device) => {
        if (null) {
          console.log('null');
          // this.manager.stopDeviceScan();
        }
        if (error) {
          console.log('Error in scan=> ' + error);
          // this.manager.destroy();
          // setTimeout(() => {
          this.setState({ isdis: true })
          // alert("unknown error")
          this.setState({ waittime: true })
          var counting = 5;
          var sInt = setInterval(() => {
            this.setState({ waitingtime: counting });
            Toast.show('Please Wait...' + counting, {
              position: 350,
              containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
              textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
              mask: true,
              maskStyle: {},
            })
            counting--;
            if (counting < 1) {
              this.setState({ isdis: false })
              this.setState({ waittime: false });
              clearInterval(sInt);
              counting = 5;
            }
          }, 1000)
          this.setState({ text1: error.message });
          this.manager.stopDeviceScan();
          // }, 1000)
          return;
        }
        this.setState({ scantext: 'STOP SCAN' })
        if (device.name != null && device.name.startsWith("ABB_")) {
          this.setState({
            blulist: {
              ...this.state.blulist, [device.name]: device,
            }
          })
        }
        setTimeout(() => {
          this.manager.stopDeviceScan();
          this.setState({ scantext: 'START SCAN' })
        }, 10000)
      }
    );
  }
  postUnpostedlogs() {
    //merge posted and unposted here
    RNFS.readDir(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')
      .then((result) => {
        //Passing Folders(devids) as array
        return Promise.all(result)
        // return Promise.all([RNFS.stat(result), result]);
      })
      .then(async (statResult) => {
        //looping array to get individual paths/folders
        var promised = statResult.map(async (fpath) => {
          //reading dirs to get file paths
          await RNFS.readFile(fpath.path, 'utf8')
            .then(async (content) => {
              Toast.show(content.length > 0 ? 'Uploading logs...' : 'Files Uptodate', {
                position: 350,
                containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                mask: true,
                maskStyle: {},
              })
              var devid = fpath.name.split(".")[0];
              var fulldata = JSON.parse('[' + content.substring(1).toString() + ']');
              if (fulldata.length > 0) {
                var promises = fulldata.map(async (reg) => {
                  this.eventpost(reg)
                })
                const allLogins = await Promise.all(promises);
                if (allLogins) {
                  const folderPath =
                    RNFS.DocumentDirectoryPath + '/LogFiles/Posted';
                  RNFS.mkdir(folderPath).catch(err => {
                    console.log(err);
                  });
                  const path = folderPath + '/' + fulldata[0].devid + '.txt';
                  if (RNFS.exists(path)) {
                    RNFS.appendFile(path, content, 'utf8')
                      .then(() => {
                        // this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path, content, 'utf8')
                      .then(() => {
                        // this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                  //write Unlink
                  console.log(devid + "read completed");
                  RNFS.unlink(fpath.path)
                }
              }
            })
        })
        const allLogins2 = await Promise.all(promised);
        if (allLogins2) {
          this.setState({ logpost: true });
          console.log("all Log folders read success");
          // console.log(allLogins2);
          this.postUnpostedimages();
        }
      })
  }
  postUnpostedimages() {
    //merge posted and unposted here
    RNFS.readDir(RNFS.DocumentDirectoryPath + '/Images/Unposted')
      .then((result) => {
        //Passing Folders(devids) as array
        return Promise.all(result)
        // return Promise.all([RNFS.stat(result), result]);
      })
      .then(async (statResult) => {
        //looping array to get individual paths/folders
        var promises2 = await statResult.map(async (fpath) => {
          //reading dirs to get file paths
          await RNFS.readFile(fpath.path, 'utf8')
            .then(async (statResult2) => {
              Toast.show(statResult2.length > 0 ? 'Uploading Images...' : 'Files Uptodate', {
                position: 350,
                containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                mask: true,
                maskStyle: {},
              })
              //looping filepaths
              var devid = fpath.name.split(".")[0];
              var fulldata = JSON.parse('[' + statResult2.substring(1).toString() + ']');
              if (fulldata.length > 0) {
                var promises = await fulldata.map(async (resp) => {
                  var imgtime = resp.time_stamp;
                  var imgdata = {
                    devid: devid,
                    time_stamp: imgtime,
                    content: resp.content,
                  };
                  this.imgpost(imgdata);
                })
                const allLogins = await Promise.all(promises);
                if (allLogins) {
                  const folderPath =
                    RNFS.DocumentDirectoryPath + '/Images/Posted';
                  RNFS.mkdir(folderPath).catch(err => {
                    console.log(err);
                  });
                  const path = folderPath + '/' + fulldata[0].devid + '.txt';
                  if (RNFS.exists(path)) {
                    RNFS.appendFile(path, statResult2, 'utf8')
                      .then(() => {
                        // this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path, statResult2, 'utf8')
                      .then(() => {
                        // this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                  //write Unlink
                  console.log(devid + "read completed");
                  RNFS.unlink(fpath.path)
                }
              }
            })
        })
        const allLogins3 = await Promise.all(promises2);
        if (allLogins3) {
          console.log("Images Posted Success");
          this.setState({ imagepost: true });
        }
      })
  }

  viewLogs() {
    //merge posted and unposted here
    // var fdata = [];
    //Unposted list
    if (RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')) {
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/LogFiles').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/LogFiles/Posted').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted').catch(err => {
        console.log(err);
      });
      RNFS.readDir(RNFS.DocumentDirectoryPath + '/LogFiles/Unposted')
        .then(async (result) => {
          // console.log('GOT RESULT', result);
          return await Promise.all(result);
        })
        .then(async (res) => {
          // console.log()
          var mypath = await res.filter((res) => {
            if (res.name.includes(this.state.text1)) {
              // console.log(res)
              return res
            }
          })
          // })
          // console.log(mypath)
          if (mypath.length > 0) {
            // console.log(mypath.length)
            RNFS.readFile(mypath[0].path, 'utf8').then(async (content) => {
              // console.log("unposted data")
              var unposteddata = content.substring(1);
              // console.log(unposteddata);
              console.log(RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted'))
              //check for posted list with unposted list
              const folderPath = RNFS.DocumentDirectoryPath + '/LogFiles/Posted'
              RNFS.mkdir(folderPath).catch(err => {
                console.log(err);
              });
              const path = folderPath + '/' + this.state.text1 + '.txt'
              if (RNFS.exists(path)) {
                console.log('exists', path)
                RNFS.appendFile(path, ',', unposteddata, 'utf8')
                await RNFS.readDir(RNFS.DocumentDirectoryPath + '/LogFiles/Posted')
                  .then((result) => {
                    console.log(result)
                    return Promise.all(result);
                  })
              } else {
                // RNFS.writeFile(path)
                console.log('written')
              }

              // .then(async (res) => {
              //   var mypath = await res.filter((res) => {
              //     if (res.name.includes(this.state.text1)) {
              //       return res
              //     }
              //   })
              //   // })
              //   if (mypath.length > 0) {
              //     RNFS.readFile(mypath[0].path, 'utf8').then(async (content2) => {
              //       console.log("posted data")
              //       var posteddata = content2.substring(1);
              //       // console.log(JSON.parse('['+posteddata+']'))
              //       this.setState({ wholedata: JSON.parse('[' + posteddata + ',' + unposteddata + ']') });
              //       // console.log(JSON.parse('[' + posteddata + ',' + unposteddata + ']'))
              //       //  posteddata +','+ unposteddata
              //     }).then(() => {
              //       this.setState({ viewlog: true })
              //       console.log("completed whole")
              //     })
              //   } else {
              //     Toast.show('Logs Empty', {
              //       position: 350,
              //       containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
              //       textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
              //       mask: true,
              //       maskStyle: {},
              //     })
              //     console.log("No logs in posted file")
              //   }
              // })
            })
          } else {
            //check for Only postedlist
            // if (RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted')) {
            RNFS.readDir(RNFS.DocumentDirectoryPath + '/LogFiles/Posted')
              .then((result) => {
                return Promise.all(result);
              })
              .then(async (res) => {
                var mypath = await res.filter((res) => {
                  if (res.name.includes(this.state.text1)) {
                    return res
                  }
                })
                // })
                if (mypath.length > 0) {
                  RNFS.readFile(mypath[0].path, 'utf8').then(async (content3) => {
                    // console.log("only posted list");
                    var onlyposted = content3.substring(1);
                    // console.log(onlyposted)
                    this.setState({ wholedata: JSON.parse('[' + onlyposted + ']') });
                    if (JSON.parse('[' + onlyposted + ']').length == 0) {
                      Toast.show('Logs Empty', {
                        position: 350,
                        containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                        textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                        mask: true,
                        maskStyle: {},
                      })
                    }
                  }).then(() => {
                    // console.log("completed only posted")
                    this.setState({ viewlog: true })
                  })
                } else {
                  Toast.show('Logs Empty', {
                    position: 350,
                    containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                    textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                    mask: true,
                    maskStyle: {},
                  })
                  console.log("No logs in posted file")
                }
              })
            // } else {
            //   console.log("Posted folder Not exists")
            // }
          }
        })
        // var whole = await promises.all()
        .catch((err) => {
          console.log("fff")
          console.log(err.message, err.code);
        });
    } else {
      console.log('no folder exists')
    }
  }


  ImageLogs() {
    //merge posted and unposted here
    // var fdata = [];
    //Unposted list
    if (RNFS.exists(RNFS.DocumentDirectoryPath + '/Images/Unposted')) {
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/Images').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/Images/Posted').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/Images/Unposted').catch(err => {
        console.log(err);
      });
      RNFS.mkdir(RNFS.DocumentDirectoryPath + '/Images/Unposted').catch(err => {
        console.log(err);
      });
      RNFS.readDir(RNFS.DocumentDirectoryPath + '/Images/Unposted')
        .then(async (result) => {
          // console.log('imagedata')
          // console.log('GOT RESULT Image', result);
          return await Promise.all(result);
        })
        .then(async (res) => {
          // console.log()
          var mypath = await res.filter((res) => {
            if (res.name.includes(this.state.text1)) {
              return res
            }
          })
          // })
          // console.log(mypath.length, "image")
          if (mypath.length > 0) {
            RNFS.readFile(mypath[0].path, 'utf8').then(async (content) => {
              // console.log("unposted Image data")
              var unposteddataimage = content.substring(1);
              // console.log(unposteddataimage);
              // console.log(RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted'))
              //check for posted list with unposted list
              RNFS.mkdir(RNFS.DocumentDirectoryPath + '/Images/Posted').catch(err => {
                console.log(err);
              });
              await RNFS.readDir(RNFS.DocumentDirectoryPath + '/Images/Posted')
                .then((result) => {
                  return Promise.all(result);
                })
                .then(async (res) => {
                  var mypath = await res.filter((res) => {
                    if (res.name.includes(this.state.text1)) {
                      return res
                    }
                  })
                  // })
                  if (mypath.length > 0) {
                    RNFS.readFile(mypath[0].path, 'utf8').then(async (content2) => {
                      console.log("posted imagedata")
                      var posteddataimage = content2.substring(1);
                      // console.log(JSON.parse('['+posteddata+']'))
                      this.setState({ ImgDataarr: JSON.parse('[' + posteddataimage + ',' + unposteddataimage + ']') });
                      // console.log(JSON.parse('[' + posteddataimage + ',' + unposteddataimage + ']'))
                      //  posteddata +','+ unposteddata
                      // console.log(this.state.ImgDataarr.length,"Posted")
                    }).then(() => {
                      console.log("completed Images")
                    })
                  } else {
                    Toast.show('Images Empty', {
                      position: 350,
                      containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                      textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                      mask: true,
                      maskStyle: {},
                    })
                    console.log("No images in posted file")
                  }
                })
            })
          } else {
            //check for Only postedlist
            // if (RNFS.exists(RNFS.DocumentDirectoryPath + '/LogFiles/Posted')) {
            RNFS.readDir(RNFS.DocumentDirectoryPath + '/Images/Posted')
              .then((result) => {
                return Promise.all(result);
              })
              .then(async (res) => {
                var mypath = await res.filter((res) => {
                  if (res.name.includes(this.state.text1)) {
                    return res
                  }
                })
                // })
                if (mypath.length > 0) {
                  RNFS.readFile(mypath[0].path, 'utf8').then(async (content3) => {
                    console.log("only posted list");
                    var onlyposted = content3.substring(1);
                    // console.log(onlyposted)
                    this.setState({ ImgDataarr: JSON.parse('[' + onlyposted + ']') });
                    // console.log(this.state.ImgDataarr.length,"Posted")
                    // if (JSON.parse('[' + onlyposted + ']').length == 0) {
                    //   Toast.show('Images Empty', {
                    //     position: 350,
                    //     containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                    //     textStyle: { color: 'white', fontSize: 20, fontWeight: 'bold' },
                    //     mask: true,
                    //     maskStyle: {},
                    //   })
                    // }
                  }).then(() => {
                    console.log("completed only posted")
                  })
                } else {
                  Toast.show('Logs Empty', {
                    position: 350,
                    containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('10%') },
                    textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                    mask: true,
                    maskStyle: {},
                  })
                  console.log("No logs in posted file")
                }
              })
            // } else {
            //   console.log("Posted folder Not exists")
            // }
          }
        })
        // var whole = await promises.all()
        .catch((err) => {
          console.log("fff")
          console.log(err.message, err.code);
        });
    } else {
      console.log('no folder exists')
    }
  }

  stopScanning() {
    if (this.state.scantext == 'START SCAN') {
      if (this.state.bluetoothState == 'PoweredOn') {
        this.setState({ blulist: {} })
        console.log("powON");
        setTimeout(() => {
          this.scanAndConnect();
        }, 1000)

      } else if (this.state.bluetoothState == 'PoweredOff') {
        console.log("poff")
        this.manager.enable();
        // this.scanAndConnect();
      } else {
        console.log(this.state.bluetoothState)
      }


    } else if (this.state.scantext == "TURN ON BLUETOOTH") {
      this.manager.enable();
    } else {
      this.manager.stopDeviceScan();
      this.setState({ scantext: 'START SCAN' });
    }
  }
  eventpost(val) {
    axios({
      url: 'https://robopower.xyz/breathalyzer/tests',
      method: 'POST',
      data: JSON.stringify(val),
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then(resp => {
        // this.setState({currentstate:null})

      })
      .catch(error => console.error(error));
  }

  setcurrentState(val) {
    if (val) {
      if (val == '0x81') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Please Blow' });
        this.setState({
          sources: require('../assets/images/BREATHALYZERS.png'),
        });
      }
      else if (val == '0x84') {
        this.setState({ currentstate: val.toString() });
        if (this.state.pressed == "calibrate") {
          this.setState({ showtext: 'Calibration' });
          this.setState({
            sources: require('../assets/images/calibrate.png'),
          });
        } else if (this.state.pressed == "taketest") {
          this.setState({ showtext: 'Analysing' });
          setTimeout(() => {
            console.log('take image');
            this.takePicture();
          }, 500);
        }
      } else if (val == '0x85') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Calibrate the Device' });
        this.setState({
          sources: require('../assets/images/calibrate.png'),
        });
        setTimeout(() => {
          this.setState({ currentstate: null })
          this.setState({ pressed: null })
        }, 2000)
      } else if (val == '0x83') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Hard Blow' });
        this.setState({
          sources: require('../assets/images/hardblow1.png'),
        });
        setTimeout(() => {
          this.setState({ currentstate: null })
          this.setState({ pressed: null })
        }, 2000)

      } else if (val == '0x86') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Blow Timeout' });
        this.setState({ sources: require('../assets/images/timeout.png') });
        setTimeout(() => {
          this.setState({ currentstate: null })
          this.setState({ pressed: null })
        }, 2000)
      } else if (val == '0x87') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Calibration Done' });
        this.setState({
          sources: require('../assets/images/calibrationDone.png'),
        });
        setTimeout(() => {
          this.setState({ currentstate: null });
          this.setState({ pressed: null })
        }, 2000);
      } else if (val == '0x82') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Insufficient Volume' });
        this.setState({
          sources: require('../assets/images/In-sufficientvolume1.png'),
        });
        setTimeout(() => {
          this.setState({ currentstate: null })
          this.setState({ pressed: null })
        }, 2000)
      } else if (val == '0x88') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'Calibration Failed' });
        this.setState({
          sources: require('../assets/images/calibrationFail.png'),
        });
        setTimeout(() => {
          this.setState({ currentstate: null });
          this.setState({ pressed: null })
        }, 2000);
      } else if (val == '0x5') {
        this.setState({ currentstate: val.toString() });
        this.setState({
          sources1: require('../assets/images/battery_5.png'),
        });
        this.setState({ currentstate: null });
      } else if (val == '0x1E') {
        this.setState({ currentstate: val.toString() });
        this.setState({
          sources1: require('../assets/images/battery_30.png'),
        });
        this.setState({ currentstate: null });
      } else if (val == '0x46') {
        this.setState({ currentstate: val.toString() });
        this.setState({
          sources1: require('../assets/images/battery_70.png'),
        });
        this.setState({ currentstate: null });
      } else if (val == '0x64') {
        this.setState({ currentstate: val.toString() });
        this.setState({
          sources1: require('../assets/images/battery_100.png'),
        });
        this.setState({ currentstate: null });
      }
      else if (val == '0x65') {
        this.setState({ currentstate: val.toString() });
        this.setState({ showtext: 'BATTERY_OFF' });
        // this.setState({ logtext: 'BATTERY_OFF' });
        this.setState({ sources: '' })
        setTimeout(() => {
          this.setState({ islist: false })
          this.manager.stopDeviceScan();
          this.setState({ blulist: {} })
          this.setState({ otaversion: null })
        }, 2000)
      }
      else if (val.startsWith('P') || val.startsWith('F')) {
        this.setState({ currentstate: val.toString() });
        if (val.startsWith('P')) {
          var spl = val.substring(2) + '.00';
          this.setState({ showtext: 'Pass with BAC:' + spl });
          this.setState({
            sources: require('../assets/images/testpass.png'),
          });
          setTimeout(() => {
            this.setState({ currentstate: null })
            this.setState({ pressed: null })
          }, 2000)
        } else {
          var spl = val.substring(1);
          this.setState({ showtext: 'Fail wit BAC:' + spl });
          this.setState({
            sources: require('../assets/images/testfail.png'),
          });
          setTimeout(() => {
            this.setState({ currentstate: null })
            this.setState({ pressed: null })
          }, 2000)
        }
      }
      else {
        // console.log(val.startsWith('O'));
        // console.log(val.startsWith('o'))
        if (val.startsWith('O')) {
          var spl = val.split(",")
          this.setState({ pressoffset: spl[0] })
          this.setState({ alcooffset: spl[1] })

        } else if (val.startsWith('B')) {
          this.setState({ bracpeak: val });
          this.setState({ currentstate: null });
        } else if (val.startsWith('V')) {
          this.setState({ batteryvoltage: val });
          // setTimeout(()=>{
          //   this.setState({ currentstate: null });
          // },3000)
        }
        else if (val.startsWith('U')) {
          var ver = val.split('U')
          this.setState({ otaversion: ver[1] });
          this.setState({ currentstate: null });
        }
        else {
          // console.log("PPPPPPPPPPPPPPPPPPPPPPpp")
          if (this.state.pressed == "logread") {
            var eve = val.split("-");
            // console.log(eve);
            if (eve.length < 2) {
              if (this.state.logsavailable) {
                this.setState({ logsavailable: false })
                // this.setState({ logtext: "Reading Logs Finished" });
                setTimeout(() => {
                  this.setState({ currentstate: null });
                  Toast.show('Logs Reading Finished', {
                    position: 350,
                    containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('8%') },
                    textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                    mask: true,
                    maskStyle: {},
                  })
                  console.log("Log Read Finished")
                }, 3000)
              } else {
                this.setState({ logsavailable: false })
                // this.setState({ logtext: "Logs Empty" });
                Toast.show('No Logs Found', {
                  position: 350,
                  containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('8%') },
                  textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                  mask: true,
                  maskStyle: {},
                })
                setTimeout(() => {
                  this.setState({ currentstate: null });
                  console.log("Log Read Finished")
                }, 2000)
              }
            } else {
              this.setState({ logsavailable: true })
              // this.setState({ logtext: "Reading Logs Please Wait..." })
              Toast.show('Reading Logs Please Wait...', {
                position: 350,
                containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('8%') },
                textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                mask: true,
                maskStyle: {},
              })
              // alert("Log Read Finished")
              this.setState({ currentstate: null });
              var pdate = eve[1].padStart(2, 0) + '/' + eve[2].padStart(2, 0) + '/' + eve[3].padStart(2, 0) + ' ' + eve[4].padStart(2, 0) + ':' + eve[5].padStart(2, 0) + ':' + eve[6].padStart(2, 0);
              var imgdata = {
                devid: this.state.text1,
                time_stamp: pdate,
                content: this.state.imagebase64,
              };
              var mytag = {
                devid: this.state.text1,
                timestamp: pdate,
                bac: eve[8] == "255" ? "NA" : eve[8],
                result: eve[7] == "1" ? "PASS" : eve[7] == "2" ? "FAIL" : "NA",
                eventtype: (eve[7] == "1" || eve[7] == "2") ? "BREATHE TEST" : eve[7] == "3" ? "CALIBRATION DONE" : eve[7] == "4" ? "INSUFFICIENT VOLUME" : eve[7] == "5" ? "HARD BLOW" : eve[7] == "6" ? "LOG READ" : eve[7] == "7" ? "LOG RESET" : null,
                lat: this.state.lat.toString(),
                lon: this.state.lon.toString(),
                bracpeakfactor: this.state.bracpeak,
                pressoffset: this.state.pressoffset,
                alcooffset: this.state.alcooffset
              };
              // var obj = {'name':"sai1",'regnum':"APZ2"}
              //fetch data and parse to get this.array
              const isFound = this.state.wholedata.some(element => {
                if (element.eventtype == mytag.eventtype && element.timestamp == mytag.timestamp) {
                  return true;
                }

                return false;
              });
              if (isFound) {
                // if internet there post data and here insert into a file a posted file
                console.log("yes there")
              } else {
                console.log("not there")
                NetInfo.fetch().then(state => {
                  if (state.isConnected) {
                    const mynewstring = mytag;
                    const folderPath =
                      RNFS.DocumentDirectoryPath + '/LogFiles/Posted';
                    RNFS.mkdir(folderPath).catch(err => {
                      console.log(err);
                    });
                    const path = folderPath + '/' + this.state.text1 + '.txt';
                    if (RNFS.exists(path)) {
                      RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
                        .then(() => {
                          this.eventpost(mytag);
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    } else {
                      RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
                        .then(() => {
                          this.eventpost(mytag);
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }
                    const folderPath1 =
                      RNFS.DocumentDirectoryPath + '/Images/Posted';
                    RNFS.mkdir(folderPath1).catch(err => {
                      console.log(err);
                    });
                    console.log('imgpost', "2")
                    const mynewstring1 = imgdata;
                    const path1 = folderPath1 + '/' + this.state.text1 + '.txt';
                    if (RNFS.exists(path1)) {
                      RNFS.appendFile(path1, "," + JSON.stringify(mynewstring1), 'utf8')
                        .then(() => {
                          this.imgpost(imgdata);
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    } else {
                      RNFS.writeFile(path1, JSON.stringify(mynewstring1), 'utf8')
                        .then(() => {
                          this.imgpost(imgdata);
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }

                  } else {
                    console.log('unimgpost', "0")
                    const folderPath1 =
                      RNFS.DocumentDirectoryPath + '/Images/Unposted';
                    RNFS.mkdir(folderPath1).catch(err => {
                      console.log(err);
                    });
                    var mynewstring1 = imgdata
                    const path1 = folderPath1 + '/' + this.state.text1 + '.txt';
                    if (RNFS.exists(path1)) {
                      RNFS.appendFile(path1, "," + JSON.stringify(mynewstring1), 'utf8')
                        .then(() => {
                          console.log("written")
                          this.setState({ imagebase64: null })
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    } else {
                      RNFS.writeFile(path1, JSON.stringify(mynewstring1), 'utf8')
                        .then(() => {
                          console.log("written")
                          this.setState({ imagebase64: null })
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }
                    console.log("No internet")
                    const mynewstring = mytag;
                    const folderPath =
                      RNFS.DocumentDirectoryPath + '/LogFiles/Unposted';
                    RNFS.mkdir(folderPath).catch(err => {
                      console.log(err);
                    });
                    const path = folderPath + '/' + this.state.text1 + '.txt';
                    if (RNFS.exists(path)) {
                      RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
                        .then(() => {
                          console.log("written")
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    } else {
                      RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
                        .then(() => {
                          console.log("written")
                        })
                        .catch(error => {
                          console.log(error);
                        });
                    }
                  }
                })
              }
            }
          } else {
            console.log("EVENT TRIGGERED");
            var eve = val.split("-");
            var pdate = eve[1].padStart(2, 0) + '/' + eve[2].padStart(2, 0) + '/' + eve[3].padStart(2, 0) + ' ' + eve[4].padStart(2, 0) + ':' + eve[5].padStart(2, 0) + ':' + eve[6].padStart(2, 0)
            var imgdata = {
              devid: this.state.text1,
              time_stamp: pdate,
              content: this.state.imagebase64,
            };
            var mytag = {
              devid: this.state.text1,
              timestamp: pdate,
              bac: eve[8] == "255" ? "NA" : eve[8],
              result: eve[7] == "1" ? "PASS" : eve[7] == "2" ? "FAIL" : "NA",
              eventtype: (eve[7] == "1" || eve[7] == "2") ? "BREATHE TEST" : eve[7] == "3" ? "CALIBRATION DONE" : eve[7] == "4" ? "INSUFFICIENT VOLUME" : eve[7] == "5" ? "HARD BLOW" : eve[7] == "6" ? "LOG READ" : eve[7] == "7" ? "LOG RESET" : null,
              lat: this.state.lat.toString(),
              lon: this.state.lon.toString(),
              bracpeakfactor: this.state.bracpeak,
              pressoffset: this.state.pressoffset,
              alcooffset: this.state.alcooffset
            };
            const isFound = this.state.wholedata.some(element => {
              if (element.eventtype == mytag.eventtype && element.timestamp == mytag.timestamp) {
                return true;
              }

              return false;
            });
            if (isFound) {
              // if internet there post data and here insert into a file a posted file
              console.log("yes there")
            } else {
              console.log("not there")
              NetInfo.fetch().then(state => {
                if (state.isConnected) {
                  const folderPath1 =
                    RNFS.DocumentDirectoryPath + '/Images/Posted';
                  RNFS.mkdir(folderPath1).catch(err => {
                    console.log(err);
                  });
                  console.log('imgpost', "2")
                  const mynewstring1 = imgdata;
                  const path1 = folderPath1 + '/' + this.state.text1 + '.txt';
                  if (RNFS.exists(path1)) {
                    RNFS.appendFile(path1, "," + JSON.stringify(mynewstring1), 'utf8')
                      .then(() => {
                        this.imgpost(imgdata);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path1, JSON.stringify(mynewstring1), 'utf8')
                      .then(() => {
                        this.imgpost(imgdata);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                  const mynewstring = mytag;
                  const folderPath =
                    RNFS.DocumentDirectoryPath + '/LogFiles/Posted';
                  RNFS.mkdir(folderPath).catch(err => {
                    console.log(err);
                  });
                  const path = folderPath + '/' + this.state.text1 + '.txt';
                  if (RNFS.exists(path)) {
                    RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
                      .then(() => {
                        this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
                      .then(() => {
                        this.eventpost(mytag);
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }

                } else {
                  console.log('unimgpost', "0")
                  const folderPath1 =
                    RNFS.DocumentDirectoryPath + '/Images/Unposted';
                  RNFS.mkdir(folderPath1).catch(err => {
                    console.log(err);
                  });
                  var mynewstring1 = imgdata
                  const path1 = folderPath1 + '/' + this.state.text1 + '.txt';
                  if (RNFS.exists(path1)) {
                    RNFS.appendFile(path1, "," + JSON.stringify(mynewstring1), 'utf8')
                      .then(() => {
                        console.log("written")
                        this.setState({ imagebase64: null })
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path1, JSON.stringify(mynewstring1), 'utf8')
                      .then(() => {
                        console.log("written")
                        this.setState({ imagebase64: null })
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                  console.log("No internet")
                  const mynewstring = mytag;
                  const folderPath =
                    RNFS.DocumentDirectoryPath + '/LogFiles/Unposted';
                  RNFS.mkdir(folderPath).catch(err => {
                    console.log(err);
                  });
                  const path = folderPath + '/' + this.state.text1 + '.txt';
                  if (RNFS.exists(path)) {
                    RNFS.appendFile(path, "," + JSON.stringify(mynewstring), 'utf8')
                      .then(() => {
                        console.log("written")
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  } else {
                    RNFS.writeFile(path, JSON.stringify(mynewstring), 'utf8')
                      .then(() => {
                        console.log("written")
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                }
              })
            }
          }
        }
      }
    }
  }
  onPoweroff() {
    this.setState({ pressed: "poweroff" })
    const device = this.state.devices;
    this.setState({ onlogout: true });
    const senddata = base64.encode('92');
    this.manager
      .writeCharacteristicWithResponseForDevice(
        this.state.deviceid,
        '000000ff-0000-1000-8000-00805f9b34fb',
        '0000ff01-0000-1000-8000-00805f9b34fb',
        senddata
      )
      .then(characteristic => {
        // console.log(characteristic.value, 'characteristic');
        this.setState({ characteristicsUUID: characteristic.uuid });
        this.setState({ serviceUUID: characteristic.serviceUUID });
      })
    setTimeout(() => {
      // this.props.navigation.navigate('login')
      this.setState({ islist: false })
      this.manager.stopDeviceScan();
      this.setState({ blulist: {} })
      this.setState({ otaversion: null })
      setTimeout(() => {
        this.manager.disable()
      }, 1000)

    }, 2000)
  }

  handleFaceDetected = faceArray => {
    console.log(faceArray)
    this.setState({ isFaceDetected: true });
};
onTaketest() {
  this.setState({ pressed: "taketest" });
  this.setState({ currentstate: 'vedio' })

  // const device = this.state.devices;
  // const senddata = base64.encode('79');
  // this.manager
  //   .writeCharacteristicWithResponseForDevice(
  //     this.state.deviceid,
  //     '000000ff-0000-1000-8000-00805f9b34fb',
  //     '0000ff01-0000-1000-8000-00805f9b34fb',
  //     senddata
  //   )
  //   .then(characteristic => {
  //     // console.log(characteristic.value, 'characteristic');
  //     this.setState({ characteristicsUUID: characteristic.uuid });
  //     this.setState({ serviceUUID: characteristic.serviceUUID });
  //     // console.log(senddata,'taketest');
  //   })

}
setTime() {
  this.setState({ pressed: "timeset" });
  let d = new Date();
  var dat =
    'T' +
    ('0' + d.getHours()).slice(-2) +
    ':' +
    ('0' + d.getMinutes()).slice(-2) +
    ':' +
    ('0' + d.getSeconds()).slice(-2) +
    ' ' +
    ('0' + (d.getMonth() + 1)).slice(-2) +
    '/' +
    ('0' + d.getDate()).slice(-2) +
    '/' +
    d.getFullYear();
  let date = dat.toString();
  const device = this.state.devices;
  this.manager.isDeviceConnected(this.state.deviceid).then(res => {
    if (res) {
      console.log("tset")
      // console.log('Discovering services and characteristics test');
      device.discoverAllServicesAndCharacteristics().then(device => {
        const senddata = base64.encode(date);
        this.manager
          .writeCharacteristicWithResponseForDevice(
            this.state.deviceid,
            '000000ff-0000-1000-8000-00805f9b34fb',
            '0000ff01-0000-1000-8000-00805f9b34fb',
            senddata
          )
          .then(characteristic => {
            // console.log(characteristic.value, 'characteristic');
            this.setState({ characteristicsUUID: characteristic.uuid });
            this.setState({ serviceUUID: characteristic.serviceUUID });
            // console.log(base64.decode(senddata),'settime');
          });
      });
    }
  });
}
autosetTime(item) {
  // this.setState({pressed:"timeset"})
  console.log("time set")
  let d = new Date();
  // var date =
  // 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopppppppbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbjsjsjsjsjs'
  var dat =
    'T' +
    ('0' + d.getHours()).slice(-2) +
    ':' +
    ('0' + d.getMinutes()).slice(-2) +
    ':' +
    ('0' + d.getSeconds()).slice(-2) +
    ' ' +
    ('0' + (d.getMonth() + 1)).slice(-2) +
    '/' +
    ('0' + d.getDate()).slice(-2) +
    '/' +
    d.getFullYear();
  let date = dat.toString();
  // const device = this.state.devices;
  // console.log(date);
  this.manager.isDeviceConnected(item.id).then(res => {
    if (res) {
      console.log("tset")
      // console.log('Discovering services and characteristics test');
      item.discoverAllServicesAndCharacteristics().then(device => {
        const senddata = base64.encode(date);
        this.manager
          .writeCharacteristicWithResponseForDevice(
            item.id,
            '000000ff-0000-1000-8000-00805f9b34fb',
            '0000ff01-0000-1000-8000-00805f9b34fb',
            senddata
          )
          .then(characteristic => {
            // console.log(characteristic.value, 'characteristic');
            this.setState({ characteristicsUUID: characteristic.uuid });
            this.setState({ serviceUUID: characteristic.serviceUUID });
            // console.log(base64.decode(senddata),'settime');
          });
      });
    }
  });
}

calibration() {
  this.setState({ pressed: "calibrate" })
  // console.log(this.state.deviceid);
  const device = this.state.devices;
  this.manager
    .isDeviceConnected(this.state.deviceid)
    .then(res => {
      // console.log(this.state.deviceid,"deviceid")
      // console.log(res,'res')
      if (res) {
        console.log('Discovering services and characteristics test');
        device
          .discoverAllServicesAndCharacteristics()
          .then(device => {
            // console.log("2");
            // console.log(device,'device')
            const senddata = base64.encode('80');
            this.manager
              .writeCharacteristicWithResponseForDevice(
                this.state.deviceid,
                '000000ff-0000-1000-8000-00805f9b34fb',
                '0000ff01-0000-1000-8000-00805f9b34fb',
                senddata
              )
              .then(characteristic => {
                // console.log(characteristic.value, 'characteristic');
                this.setState({ characteristicsUUID: characteristic.uuid });
                this.setState({ serviceUUID: characteristic.serviceUUID });
                // console.log(senddata,'calibration');
              })
          })
          .catch(err => {
            console.log('2');
            console.log(err);
          });
      }
    })
    .catch(err => {
      console.log('1');
      console.log(err);
    });
}

Logread() {
  this.setState({ pressed: "logread" })
  const senddata = base64.encode('90');
  this.manager
    .writeCharacteristicWithResponseForDevice(
      this.state.deviceid,
      '000000ff-0000-1000-8000-00805f9b34fb',
      '0000ff01-0000-1000-8000-00805f9b34fb',
      senddata
    )
    .then(characteristic => {
      // console.log(characteristic.value, 'characteristic');
      this.setState({ characteristicsUUID: characteristic.uuid });
      this.setState({ serviceUUID: characteristic.serviceUUID });
      // console.log(senddata,'Logread');
    })
}
memoryReset() {
  this.setState({ pressed: "memset" })
  // console.log(this.state.deviceid);
  const device = this.state.devices;
  this.manager
    .isDeviceConnected(this.state.deviceid)
    .then(res => {
      // console.log(this.state.deviceid,"deviceid")
      // console.log(res,'res')
      if (res) {
        console.log('Discovering services and characteristics test');
        device
          .discoverAllServicesAndCharacteristics()
          .then(device => {
            // console.log("2");
            // console.log(device,'device')
            const senddata = base64.encode('91');
            this.manager
              .writeCharacteristicWithResponseForDevice(
                this.state.deviceid,
                '000000ff-0000-1000-8000-00805f9b34fb',
                '0000ff01-0000-1000-8000-00805f9b34fb',
                senddata
              )
              .then(characteristic => {
                // console.log(characteristic.value, 'characteristic');
                this.setState({ characteristicsUUID: characteristic.uuid });
                this.setState({ serviceUUID: characteristic.serviceUUID });
                // console.log(senddata,'memset');
                setTimeout(() => {
                  Toast.show('Mem Set Done successfully ', {
                    position: 350,
                    containerStyle: { backgroundColor: 'black', width: wp('70%'), height: hp('8%') },
                    textStyle: { color: 'white', fontSize: 17, fontWeight: 'bold' },
                    mask: true,
                    maskStyle: {},
                  })
                }, 3000)
              });
          })
          .catch(err => {
            console.log('2');
            console.log(err);
          });
      }
    })
    .catch(err => {
      console.log('1');
      console.log(err);
    });
}

OTAupdate() {
  this.setState({ pressed: "otaupdate" })
  // console.log(this.state.deviceid);
  // this.downloaddata();
  const device = this.state.devices;
  this.manager
    .isDeviceConnected(this.state.deviceid)
    .then(res => {
      // console.log(this.state.deviceid,"deviceid")
      // console.log(res,'res')
      if (res) {
        console.log('Discovering services and characteristics test');
        device
          .discoverAllServicesAndCharacteristics()
          .then(device => {
            // console.log("2");
            // console.log(device,'device')
            const senddata = base64.encode('66');
            this.manager
              .writeCharacteristicWithResponseForDevice(
                this.state.deviceid,
                '000000ff-0000-1000-8000-00805f9b34fb',
                '0000ff01-0000-1000-8000-00805f9b34fb',
                senddata
              )
              .then(characteristic => {
                // console.log(characteristic.value, 'characteristic');
                this.setState({ characteristicsUUID: characteristic.uuid });
                this.setState({ serviceUUID: characteristic.serviceUUID });
                // console.log(base64.decode(senddata),"otaupdate");
              });
          })
          .catch(err => {
            console.log('2');
            console.log(err);
          });
      }
    })
    .catch(err => {
      console.log('1');
      console.log(err);
    });
}

showImage(item) {
  this.setState({ imageDialog: true })
  this.setState({ mystate: "yesdata" })
  if (item) {
    this.state.ImgDataarr.map((item1) => {
      if (item.timestamp == item1.time_stamp) {
        this.setState({ imgbase64: item1.content })
      }
    })
  } else {
    this.setState({ mystate: "nodata" })
  }
}

showmap(item) {
  console.log(item.lat, item.lon)
  this.setState({ showLoc: true })
  if (item) {
    this.setState({ mystate: "yesdata" })
    this.setState({ lat1: item.lat })
    this.setState({ lon1: item.lon })
  } else {
    this.setState({ mystate: "nodata" })
  }
}

render() {
  return (
    <View style={{ flex: 1, backgroundColor: '#e8f5e9' }}>
      <View>
        <Modal
          visible={this.state.imageDialog}
          animationType="slide"
          transparent={true}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              borderRadius: 20,
              alignItems: 'center',
              maxHeight: hp('60%'),
              maxWidth: wp('92%'),
              marginLeft: wp('4%'),
              marginTop: hp('20%')
            }}>
            {this.state.mystate == "yesdata" ?
              <Image
                style={{
                  height: hp('47%'),
                  width: wp('80%'),
                  marginTop: hp('2%')
                }}
                // source={require('../assets/images/a200device.jpg')}
                source={{
                  uri: `data:image/jpg;base64,${this.state.imgbase64}`,
                }}
              />
              : this.state.mystate == "loading" ?
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 30, color: "green", justifyContent: 'center', marginTop: hp('25%') }}>Loading...</Text>
                </View>
                : <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 30, color: "green", justifyContent: 'center', marginTop: hp('25%') }}>Image Not Found</Text>
                </View>
            }
            <TouchableOpacity onPress={() => { this.setState({ imageDialog: false }) }} style={{
              width: wp('25%'), marginBottom: hp('3%'), marginTop: hp('3%'),
              height: hp('5%'), backgroundColor: '#f44336', justifyContent: 'center', borderRadius: 20,
              alignSelf: 'center'
            }}>
              <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold' }}>Close</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
      <View>
        <Modal
          visible={this.state.showLoc}
          animationType="slide"
          transparent={true}>
          <View style={{
            flex: 1,
            backgroundColor: 'white',
            borderRadius: 20,
            alignItems: 'center',
            maxHeight: hp('60%'),
            maxWidth: wp('92%'),
            marginLeft: wp('4%'),
            marginTop: hp('20%')
          }}>
            {this.state.mystate == "yesdata" ?
              <MapboxGL.MapView style={styles.map} >
                <MapboxGL.Camera
                  zoomLevel={8}
                  centerCoordinate={[this.state.lon1, this.state.lat1]}
                />
                <MapboxGL.PointAnnotation
                  coordinate={[this.state.lon, this.state.lat]}>
                  <MapboxGL.Callout>
                    <View>
                      <Text style={{ backgroundColor: '#ffffff', color: 'purple', fontWeight: 'bold' }}>{this.state.devid}</Text>
                    </View>
                  </MapboxGL.Callout>
                </MapboxGL.PointAnnotation>
              </MapboxGL.MapView>
              : this.state.mystate == "loading" ?
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 30, color: "green", justifyContent: 'center', marginTop: hp('25%') }}>Loading...</Text>
                </View>
                : <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 30, color: "green", justifyContent: 'center', marginTop: hp('25%') }}>Location Not Found</Text>
                </View>
            }
            <TouchableOpacity onPress={() => { this.setState({ showLoc: false }) }} style={{
              width: wp('25%'), marginBottom: hp('3%'), marginTop: hp('3%'),
              height: hp('5%'), backgroundColor: '#f44336', justifyContent: 'center', borderRadius: 20,
              alignSelf: 'center'
            }}>
              <Text style={{ color: 'white', textAlign: 'center', fontWeight: 'bold' }}>Close</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
      <View
        style={{
          justifyContent: 'center',
          height: hp('10%'),
          backgroundColor: '#006064',
        }}
      >
        <View style={{ flexDirection: 'row' }}>

          <Image
            style={{ width: wp('38%'), height: hp('3.5%'), marginLeft: 20 }}
            source={require('../assets/images/alcobrake.jpg')}
          />
          <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18, marginLeft: wp('40%') }}>{this.state.otaversion != null ? 'V' + this.state.otaversion : null}</Text>
        </View>
      </View>
      {this.state.islist ?
        <View style={{ flex: 1, backgroundColor: '#e8f5e9' }}>
          {!this.state.viewlog ?
            <View style={{ flex: 1, backgroundColor: '#e8f5e9' }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: hp('2%'),
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    color: 'black',
                    fontWeight: 'bold',
                    marginLeft: wp('3%'),
                  }}
                >
                  CONNECTED TO:
                  {' '}
                  {this.state.bluetoothState == 'PoweredOn'
                    ? this.state.text1
                    : 'NOT CONNECTED'}
                </Text>
                {/* <Icon
                    name={
                      this.state.bluetoothState == 'PoweredOn'
                        ? 'bluetooth'
                        : 'bluetooth-off'
                    }
                    size={35}
                    style={{
                      color: this.state.bluetoothState == 'PoweredOn'
                        ? 'steelblue'
                        : '#9fa8da',
                      textAlign: 'center',
                      marginRight: wp('4%'),
                    }}
                  /> */}
              </View>
              <View
                style={{ flexDirection: 'row', justifyContent: 'space-between' }}
              >
                <TouchableOpacity
                  onPress={() => { this.viewLogs(), this.ImageLogs() }}
                >
                  <Icon
                    name={"book-information-variant"}
                    size={38}
                    style={{
                      color: '#ef5350',
                      marginLeft: wp('60%')
                    }}
                  />
                </TouchableOpacity>
                <Image
                  style={{
                    width: wp('25%'),
                    height: hp('3.5%'),
                    marginLeft: wp('72%'),
                    // marginRight: wp('2.5%')
                  }}
                  source={this.state.sources1}
                // source={require('../assets/images/battery_30.png')}
                />
              </View>
              <View
                style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
              >
                {this.state.currentstate == null ?
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      padding: 10,
                    }}
                  >
                    <View style={{ position: 'absolute', zIndex: 1, justifyContent: 'center' }}>
                      <Spinner
                        // style={styles.spinner}
                        isVisible={this.state.isVisible}
                        size={120}
                        type="Circle"
                        color="#006064"
                      />
                    </View>

                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: hp('-10%'),
                      }}
                    >
                      <TouchableOpacity
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#ffb74d',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          marginRight: wp('20%'),
                          width: wp("50%"),
                          height: hp('8%')
                        }}
                        onPress={() => this.onTaketest()}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          TAKE-TEST
                        </Text>
                      </TouchableOpacity>
                      {/* <TouchableOpacity
                          onPress={() => this.setTime()}
                          style={{
                            justifyContent: 'center',
                            backgroundColor: '#9ccc65',
                            borderTopRightRadius: 60,
                            borderBottomLeftRadius: 60,
                            width: wp("50%"),
                            height: hp('8%'),
                            marginLeft: wp('20%'),
                            marginTop: hp('2.5%')
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 20,
                              color: 'white',
                              textAlign: 'center',
                              fontWeight: 'bold',
                            }}
                          >
                            TIME-SET
                          </Text>
                        </TouchableOpacity> */}
                      <TouchableOpacity
                        onPress={() => this.calibration()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#81c784',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          marginLeft: wp('20%'),
                          width: wp("50%"),
                          height: hp('8%'),
                          marginTop: hp('2.5%')
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          CALIBRATION
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.Logread()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#ffab91',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          width: wp("50%"),
                          height: hp('8%'),
                          marginRight: wp('20%'),
                          marginTop: hp('2.5%')
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          LOG-READ
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.memoryReset()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#ce93d8',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          marginLeft: wp('20%'),
                          width: wp("50%"),
                          height: hp('8%'),
                          marginTop: hp('2.5%')
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          MEM-RESET
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.OTAupdate()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#b39ddb',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          marginRight: wp('20%'),
                          width: wp("50%"),
                          height: hp('8%'),
                          marginTop: hp('2.5%')
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          OTA-UPDATE
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#80cbc4',
                          borderTopRightRadius: 60,
                          borderBottomLeftRadius: 60,
                          marginLeft: wp('20%'),
                          width: wp("50%"),
                          height: hp('8%'),
                          marginTop: hp('2%')
                        }}
                        onPress={() => this.onPoweroff()}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                            fontWeight: 'bold',
                          }}
                        >
                          POWER-OFF
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  :
                  <View>
                    {this.state.pressed == "taketest" && this.state.currentstate == 'vedio' ?
                      <View style={{ marginBottom: hp('20%') }}>
                        {/* <RNCamera
                          faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.fast}
                          onFacesDetected={this.handleFaceDetected}
                        /> */}
                        <RNCamera
                                    ref={ref => (this.camera = ref)}
                                    style={{
                                      height: hp('35%'),
                                      width: wp('50%'),
                                      marginTop: hp('3%')
                                    }}
                                    type={this.state.cameratype}
                                    captureAudio={false}
                                    androidCameraPermissionOptions={{
                                      title: 'Permission to use camera',
                                      message: 'We need your permission to use your camera',
                                      buttonPositive: 'Ok',
                                      buttonNegative: 'Cancel',
                                    }}
                                  />
                        {this.state.isFaceDetected && (<Text>I saw a face</Text>)}
                      </View> :
                      // <View>
                      //   {this.state.pressed == "logread" ?
                      //     <View style={{ marginBottom: hp('20%') }}>
                      //       <Text style={{ color: 'green', fontWeight: 'bold', fontSize: 30 }}>{this.state.logtext}</Text>
                      //     </View>
                      //     :
                      <View
                        style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'column',
                        }}
                      >
                        <View
                          style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: hp('-1%')
                          }}
                        >
                          <Text style={styles.titleText}>
                            {this.state.showtext}
                          </Text>
                          {this.state.currentstate == '0x84' && this.state.pressed == "taketest"
                            ? <View
                              style={{
                                flex: 1,
                                alignItems: 'center',
                                flexDirection: 'column',
                                marginTop: hp('5%')
                              }}
                            >
                              <Text>
                                <Spinner
                                  isVisible={true}
                                  size={120}
                                  type="Wave"
                                  color="green"
                                />
                              </Text>
                              <View>
                                {this.state.imagebase64 == null
                                  ? <RNCamera
                                    ref={ref => (this.camera = ref)}
                                    style={{
                                      height: hp('35%'),
                                      width: wp('50%'),
                                      marginTop: hp('3%')
                                    }}
                                    type={this.state.cameratype}
                                    captureAudio={false}
                                    androidCameraPermissionOptions={{
                                      title: 'Permission to use camera',
                                      message: 'We need your permission to use your camera',
                                      buttonPositive: 'Ok',
                                      buttonNegative: 'Cancel',
                                    }}
                                  />
                                  : <Image
                                    style={{
                                      height: hp('35%'),
                                      width: wp('50%'), marginTop: hp('3%')
                                    }}
                                    source={{
                                      uri: `data:image/jpg;base64,${this.state.imagebase64}`,
                                    }}
                                  />}
                              </View>
                            </View>
                            : <View
                              style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: hp('-20%'),
                              }}
                            >
                              <Image
                                style={{
                                  width: wp('70%'),
                                  height: hp('60%'),
                                  resizeMode: 'contain',
                                }}
                                source={this.state.sources}
                              />
                            </View>}
                        </View>
                      </View>}
                  </View>
                }
              </View>
            </View> :
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: hp('2%'), marginBottom: hp('-2%') }}>
                <Text style={{
                  color: 'white', fontSize: 17, fontWeight: 'bold', backgroundColor: 'green', borderRadius: 20,
                  height: hp('4%'), width: wp('60%'), textAlign: 'center', paddingTop: hp('0.3%')
                }}>{this.state.text1}</Text>
                <TouchableOpacity onPress={() => { this.setState({ viewlog: false }) }} style={{
                  width: wp('15%'),
                  height: hp('5%'), backgroundColor: '#f44336', justifyContent: 'center', borderRadius: 20
                }}>
                  <Text style={{ color: 'white', textAlign: 'center' }}>Close</Text>
                </TouchableOpacity>
              </View>
              <View>
                {
                  this.state.wholedata.length > 0 ?
                    <View style={{ marginBottom: hp('10%') }}>
                      <FlatList
                        style={styles.notificationList}
                        enableEmptySections={true}
                        data={this.state.wholedata}
                        keyExtractor={item => {
                          item.id;
                        }}
                        extraData={this.state.wholedata}
                        renderItem={({ item, index }) => {
                          return (
                            <View
                              style={{
                                flex: 1,
                                borderRadius: 10,
                                height: hp('15%'),
                                justifyContent: 'center',
                                flexDirection: 'row',
                                backgroundColor: '#58ad9c',
                                marginBottom: hp('2%'),
                              }}
                            >
                              <ScrollView horizontal={true}>
                                <View style={styles.datatable}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text
                                      style={{
                                        color: 'black',
                                        fontSize: 15,
                                        fontWeight: 'bold',
                                        marginLeft: wp('3%'),
                                      }}
                                    >
                                      EVENT
                                    </Text>
                                    <Text
                                      style={{
                                        color: 'black',
                                        marginLeft: wp('23%'),
                                        fontWeight: 'bold',
                                      }}
                                    >
                                      TIME
                                    </Text>
                                    <Text
                                      style={{
                                        color: 'black',
                                        marginLeft: wp('22%'),
                                        fontWeight: 'bold',
                                      }}
                                    >
                                      BAC
                                    </Text>
                                    <Text
                                      style={{
                                        color: 'black',
                                        marginLeft: wp('22%'),
                                        fontWeight: 'bold',
                                      }}
                                    >
                                      RESULT
                                    </Text>
                                    <Text
                                      style={{
                                        color: 'black',
                                        marginLeft: wp('22%'),
                                        fontWeight: 'bold',
                                      }}
                                    >
                                      IMAGE
                                    </Text>
                                    <Text
                                      style={{
                                        color: 'black',
                                        marginLeft: wp('22%'),
                                        fontWeight: 'bold',
                                        marginRight: wp('2%')
                                      }}
                                    >
                                      LOCATION
                                    </Text>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      marginBottom: 17,
                                    }}
                                  >
                                    <Text
                                      style={{
                                        color: 'black',
                                        fontSize: 15,
                                        marginLeft: wp('3%'),
                                        marginTop: 5,
                                        flexShrink: 1,
                                        width: wp('30%'),
                                      }}
                                      numberOfLines={2}
                                    >
                                      {item.eventtype}
                                    </Text>

                                    <Text
                                      style={{
                                        color: 'black',
                                        fontSize: 15,
                                        marginLeft: wp('3%'),
                                        marginTop: 5,
                                        flexShrink: 1,
                                        width: wp('30%'),
                                      }}
                                      numberOfLines={7}
                                    >
                                      {item.timestamp}
                                    </Text>

                                    <Text
                                      style={{
                                        color: 'black',
                                        fontSize: 15,
                                        marginLeft: wp('5.5%'),
                                        marginTop: 5,
                                        flexShrink: 1,
                                        width: wp('30%'),
                                      }}
                                      numberOfLines={7}
                                    >
                                      {item.bac}
                                    </Text>

                                    <Text
                                      style={{
                                        color: 'black',
                                        fontSize: 15,
                                        marginLeft: wp('1%'),
                                        marginTop: 5,
                                        flexShrink: 1,
                                        width: wp('30%'),
                                      }}
                                      numberOfLines={7}
                                    >
                                      {item.result}
                                    </Text>
                                    <TouchableOpacity style={{ marginLeft: wp('3%') }} onPress={() => { this.showImage(item, index) }}>
                                      <Icon name='file-image' size={40} color={'green'} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ marginLeft: wp('25%') }} onPress={() => { this.showmap(item) }}>
                                      <Icon name='map-marker-radius' size={40} color={'green'} />
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </ScrollView>
                            </View>
                          )
                        }}
                      />
                    </View>
                    : <View>
                      <Spinner
                        isVisible={this.state.isVisible}
                        size={120}
                        type="Circle"
                        color="#006064"
                      />
                    </View>}
              </View>
            </View>
          }
        </View>
        :
        <View>
          <View>
            {/* <TouchableOpacity disabled={this.state.isdis} onPress={() => { this.postUnpostedlogs() }}> */}
            {/* <TouchableOpacity disabled={this.state.isdis} onPress={() => { this.viewLogs() }}> */}
            <TouchableOpacity disabled={this.state.isdis} onPress={() => { this.stopScanning() }} style={{
              backgroundColor: this.state.scantext == 'TURN ON BLUETOOTH' ? 'steelblue' : this.state.scantext == 'START SCAN' ? '#689f38' : '#e53935', justifyContent: 'center', borderRadius: 70, width: wp('35%'),
              alignSelf: 'center', height: hp('17%'), marginTop: hp('10%')
            }}>
              <Text
                style={{ textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 17 }}>
                {this.state.scantext}</Text>
            </TouchableOpacity>
          </View>
          <Text>{this.state.original}</Text>
          <FlatList
            data={Object.values(this.state.blulist)}
            renderItem={({ item }) => <TouchableHighlight onPress={() => { this.onConnect(item) }} style={styles.item}>
              <Text style={styles.title}>{item.name}</Text>
            </TouchableHighlight>}
            keyExtractor={item => item.id}
          />
        </View>}
    </View>
  );
}
}

const styles = StyleSheet.create({
  map: {
    height: hp('47%'),
    width: wp('80%'),
    marginTop: hp('2%')
  },
  notificationList: {
    marginTop: 20,
    padding: 10,
  },

  columnRowTxt: {
    width: "20%",
    textAlign: "center",
    color: 'black',
  }, tableRow: {
    flexDirection: "row",
    height: 40,
    alignItems: "center",
  },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: {
    margin: 6,
    textAlign: 'center',
    justifyContent: 'center',
    color: 'black'
  },
  item: {
    backgroundColor: '#757575',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    width: wp('70%'),
    borderRadius: 20,
    alignSelf: 'center'
  },
  listing: {
    backgroundColor: 'teal',
    padding: 20,
    marginVertical: 2,
    marginHorizontal: 4,
    marginLeft: 10,
    color: 'red'
  },
  title: {
    fontSize: 17,
    color: 'white',
    textAlign: 'center'
  },
  // container: {
  //   flex: 0.9,
  //   backgroundColor: '#F5FCFF',
  // },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: {
    margin: 6,
    textAlign: 'center',
    justifyContent: 'center'
  },
  titleText: {
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color: 'green',
    marginTop: 20,
  },
  datatable: {
    justifyContent: 'center',
    alignSelf: 'center',
    height: hp('12%'),
    backgroundColor: '#E5E7E9',
    borderRadius: 10,
    marginLeft: wp('2%'),
    marginRight: wp('2%')
  },

});

export default Dashboard;