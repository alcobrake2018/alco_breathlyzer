/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableHighlight, 
  View,
  AsyncStorage,
  Modal,
  ActivityIndicator,
  Image,
  ImageBackground,
  Alert,
  Button,
} from 'react-native';
import {BleManager} from 'react-native-ble-plx';
import {Buffer} from 'buffer';
import base64 from 'react-native-base64';
// import { Camera } from 'expo-camera';
import {Card} from 'react-native-elements';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';
import RNFS from 'react-native-fs';
// import * as ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-image-crop-picker';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialIcons';

// var Spinner = require ('react-native-spinkit');

class Dashboard extends Component {
  constructor (props) {
    super (props);
    this.manager = new BleManager ();
    this.state = {
      camera: null,
      imagebase64: null,
      cameraPermission: null,
      cameratype: RNCamera.Constants.Type.front,
      imageArray: [],
      deviceid: '',
      serviceUUID: '',
      characteristicsUUID: '',
      text1: 'No connections',
      makedata: [],
      showToast: false,
      notificationReceiving: false,
      bluetoothState: '',
      currentstate: null,
      sources: null,
      sources1:null,
      showtext: null,
      devices: null,
      result: null,
      image: null,
      images: null,
      video: '',
      isVideo: true,
      videoSource: '',
      isFaceDetected: false,
      filepath: {
        data: '',
        uri: '',
      },
      fileData: '',
      fileUri: '',
      arr: [],
      count: 1,
      lat: props.route.params.lat,
      lon: props.route.params.lon,
      imageDate: null,
      isVisible: false,
    };
  }
  componentWillUnmount () {
    this.manager.disable ();
    this.manager.stopDeviceScan ();
    this.manager = null;
    console.log ('unmounttttt');
  }
  componentDidMount () {
    this.manager = new BleManager ();
    console.log ('&&&&&&&&&&&&&&&&&&&&&&&&&');
    this.checkConn ();
  }
  checkConn () {
    console.log (this.manager);
    if (this.manager != null) {
      this.manager.onStateChange (state => {
        // console.log(state)
        this.setState ({bluetoothState: state});
        if (state == 'PoweredOn') {
          this.scanAndConnect ();
        } else if (state == 'PoweredOff') {
          this.manager.enable ();
          this.setState ({currentstate: state});
          this.scanAndConnect ();
        } else {
          console.log (state);
        }
      }, true);
    }
  }
  takePicture = async () => {
    var d = new Date ();
    var date =
      ('0' + (d.getMonth () + 1)).slice (-2) +
      '/' +
      ('0' + d.getDate ()).slice (-2) +
      '/' +
      d.getFullYear () +
      ' ' +
      ('0' + d.getHours ()).slice (-2) +
      ':' +
      ('0' + d.getMinutes ()).slice (-2) +
      ':' +
      ('0' + d.getSeconds ()).slice (-2);
    this.setState ({imageDate: date});
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      try {
        const data = await this.camera.takePictureAsync (options);
        // console.log('Success', JSON.stringify(data.base64));
        console.log (data.uri, this.state.imageDate);
        this.setState ({imagebase64: JSON.stringify (data.base64)});
        console.log (this.state.imagebase64);
      } catch (err) {
        Alert.alert (
          'Error',
          'Failed to take picture: ' + (err.message || err)
        );
        return;
      }
    }
  };

  scanAndConnect () {
    // 000000ee-0000-1000-8000-00805f9b34fb
    this.setState ({text1: 'Scanning...'});
    this.manager.startDeviceScan (
      null,
      {allowDuplicates: false},
      (error, device) => {
        // console.log(device)
        if (null) {
          console.log ('null');
          this.setState ({text1: 'No Devices found'});
          // this.scanAndConnect ();
        }
        if (error) {
          console.log ('Error in scan=> ' + error);
          this.setState ({text1: error.message});
          this.manager.stopDeviceScan ();
          return;
        }
        if (device.name == 'ALCOBRAKE') {
          this.setState ({serviceUUID: device.serviceUUIDs[0]});
          this.setState ({text1: device.name});
          this.setState ({deviceid: device.id});
          this.setState ({devices: device});
          this.manager.stopDeviceScan ();
          console.log (device.id, '=====> MACID');
          device
            .connect ({requestMTU: 512})
            .then (device => {
              this.manager.onDeviceDisconnected (device.id, (error, device) => {
                if (error) {
                  console.log (error);
                }
                console.log ('Device is disconnected');
                this.setState ({text1: 'Device Disconnected'});
              });
            })
            // .then((device) => {
            //   console.log("Discovering services and characteristics");
            //   return device.discoverAllServicesAndCharacteristics()
            // })
            // .then((device) => {
            //   const senddata = base64.encode("79");
            //   device.writeCharacteristicWithResponseForService('000000ff-0000-1000-8000-00805f9b34fb', '0000ff01-0000-1000-8000-00805f9b34fb', senddata)
            //     .then((characteristic) => {
            //       // console.log("characteristic");
            //       // console.log(characteristic);
            //       // console.log("characteristic");
            //     })
            .then (device => {
              device.monitorCharacteristicForDevice (
                device.id,
                '000000ff-0000-1000-8000-00805f9b34fb',
                '0000ff01-0000-1000-8000-00805f9b34fb',
                (error, characteristic) => {
                  if (error) {
                    // this.error(error.message)
                    console.log (error.message);
                    return;
                  }
                  console.log ('&&&&&&&&&&&&&&&&&&');
                  console.log (characteristic.value);
                  if (characteristic.value.length < 10) {
                    var buffer = Buffer.from (characteristic.value, 'base64');
                    var dec = buffer.toString ('hex');
                    console.log (dec);
                    // var str = '';
                    // for (var i = 0; i < dec.length; i += 2) {
                    //   var v = parseInt(dec.substr(i, 2), 16);
                    //   if (v) str += String.fromCharCode(v);
                    // }
                    // dec = str
                    // console.log("^^^^^^^^^^^^^^^^^")
                    // console.log(dec)
                    this.setcurrentState (dec);
                  } else {
                    var buffer = Buffer.from (
                      characteristic.value.substring (0, 4) + '=',
                      'base64'
                    );
                    var dec = buffer.toString ('hex');
                    console.log (dec);
                    var str = '';
                    for (var i = 0; i < dec.length; i += 2) {
                      var v = parseInt (dec.substr (i, 2), 16);
                      if (v) str += String.fromCharCode (v);
                    }
                    dec = str;
                    console.log ('@@@@@@@@@@@@@@@@');
                    console.log (dec);
                    this.setcurrentState (dec);
                  }
                }
              );
            })
            .catch (err => {
              console.log (err.message);
            });
          // })
          // .catch((error) => {
          //   console.log(error.message)
          // })
        }
      }
    );
  }

  eventpost (val) {
    axios ({
      url: 'http://robopower.xyz/breathalyzer/tests',
      method: 'POST',
      data: JSON.stringify (val),
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then (resp => console.log (resp.data.response))
      .catch (error => console.error (error));
  }

  readData () {
    NetInfo.fetch ().then (state => {
      console.log (state.isConnected);
      if (state.isConnected) {
        // var pth = RNFS.DocumentDirectoryPath + '/Alcobrake' + '/events';
        var pth = RNFS.DocumentDirectoryPath + '/LogFiles';
        RNFS.readDir (pth) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
          .then (result => {
            console.log ('GOT RESULT', result);
            // stat the first file
            return Promise.all ([RNFS.stat (result[0].path), result[0].path]);
          })
          .then (statResult => {
            console.log (statResult[0], 'file');
            if (statResult[0].isFile ()) {
              console.log (statResult[1]);
              // if we have a file, read it
              RNFS.readFile (statResult[1], 'utf8')
                .then (contents => {
                  // log the file contents
                  console.log ('success');
                })
                .catch (err => {
                  console.log (err.message, err.code);
                });
            }
            return 'no file';
          });

        // RNFS.exists(pth).then(exists => {
        //   if (exists) {
        //     RNFS.readDir(pth)
        //       .then(result => {
        //         var mypaths = [];
        //         result.map(res => {
        //           var mypth = res.path;
        //           if (mypth.substr(mypth.length - 3) == 'txt') {
        //             mypaths.push(res.path);
        //           }
        //         });
        //         // RNFS.readFile(`${RNFS.DocumentDirectoryPath}/Logfiles/test.txt`,
        //         // "utf8").then(result => {
        //         //   console.log('file copied:', result);
        //         // })
        //         //   .catch(err => {
        //         //     console.log(err);
        //         //   });
        //         console.log(mypaths)
        //         return Promise.all(mypaths);
        //       })
        //   }
        // })
      } else {
        console.log (state.isConnected, 'readData');
      }
    });
  }

  launchCamera = (cropping, mediaType = 'photo') => {
    // let options = {
    //   storageOptions: {
    //     skipBackup: true,
    //     path: 'images',
    //   },
    // };
    let options = {
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 30,
      thumbnail: true,
      allowsEditing: true,
    };
    ImagePicker.launchCamera (options, response => {
      console.log ('Response = ', response);

      if (response.didCancel) {
        console.log ('User cancelled image picker');
      } else if (response.error) {
        console.log ('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log ('User tapped custom button: ', response.customButton);
        alert (response.customButton);
      } else {
        const source = {uri: response.uri};
        console.log ('response', JSON.stringify (response.uri).base64);
        this.setState ({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
        });
      }
    });
  };

  Takepicture (cropping, mediaType = 'photo') {
    //   const options = {
    //     mediaType: 'video',
    //     videoQuality: 'medium',
    //     durationLimit: 30,
    //     thumbnail: true,
    //     allowsEditing: true,
    //  };

    //    ImagePicker.launchCamera(options, (response) => {
    //     console.log(response)
    //     if (response.didCancel) {
    //       // console.warn('User cancelled video picker');
    //       return true;
    //     } else if (response.error) {
    //        // console.warn('ImagePicker Error: ', response.error);
    //     } else if (response.customButton) {
    //         console.warn('User tapped custom button: ', response.customButton);
    //     } else {
    //        this.setState({video: response.uri});
    //        console.log(response.uri)
    //     }
    //  });
    ImagePicker.openCamera ({
      cropping: cropping,
      quality: 0.9,
      maxWidth: 1200,
      maxHeight: 1200,
      includeExif: true,
      mediaType,
      useFrontCamera: true,
    })
      .then (image => {
        console.log ('received image', image);
        this.setState ({
          image: {
            uri: image.path,
            width: image.width,
            height: image.height,
            mime: image.mime,
          },
          images: null,
        });
        console.log (this.state.image.uri);
        const senddata = base64.encode (this.state.image.uri);
        console.log (typeof senddata);
      })
      .catch (e => console.log (e));
  }

  setcurrentState (val) {
    if (val) {
      this.setState ({currentstate: val.toString ()});
      NetInfo.fetch ().then (state => {
        // console.log(state.isConnected)
        if (state.isConnected) {
          // console.log(val)
          console.log (this.state.currentstate);
          // console.log(state)
          if (this.state.currentstate == '0x81') {
            // setTimeout(()=>{
            this.setState ({showtext: 'Please Blow'});
            this.setState ({
              sources: require ('../assets/images/BREATHALYZERS.png'),
            });
            // },2000)
          } else if (this.state.currentstate == '0x83') {
            this.setState ({showtext: 'Hard Blow'});
            this.setState ({
              sources: require ('../assets/images/hardblow1.png'),
            });
          } else if (val == '0x84') {
            this.setState ({showtext: 'Analysing'});
            this.setState ({
              sources: (
                <Spinner
                  isVisible={true}
                  size={150}
                  type="Wave"
                  color="green"
                />
              ),
            });
            // this.Takepicture(false);
            // this.takePicture.bind(this);
            // this.launchCamera()
          } else if (val == '0x85') {
            this.setState ({showtext: 'Calibrate the Device'});
            this.setState ({
              sources: require ('../assets/images/calibrate.png'),
            });
          } else if (val == '0x86') {
            this.setState ({showtext: 'Blow Timeout'});
            this.setState ({sources: require ('../assets/images/timeout.png')});
          } else if (val == '0x87') {
            this.setState ({showtext: 'Calibration Done'});
            this.setState ({
              sources: require ('../assets/images/calibrationDone.png'),
            });
            setTimeout (() => {
              this.setState ({currentstate: null});
            }, 1000);
          } else if (this.state.currentstate == '0x82') {
            this.setState ({showtext: 'Insufficient Volume'});
            this.setState ({
              sources: require ('../assets/images/In-sufficientvolume1.png'),
            });
          } else if (val == '0x88') {
            this.setState ({showtext: 'Calibration Failed'});
            this.setState ({
              sources: require ('../assets/images/calibrationFail.png'),
            });
            setTimeout (() => {
              this.setState ({currentstate: null});
            }, 1000);
          } else if (this.state.currentstate == '0x5') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_5'});
              this.setState ({
                sources1: require ('../assets/images/battery_5.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x1E') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_30'});
              this.setState ({
                sources1: require ('../assets/images/battery_30.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x46') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_70'});
              this.setState ({
                sources1: require ('../assets/images/battery_70.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x64') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_100'});
              this.setState ({
                sources1: require ('../assets/images/battery_100.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x65') {
            this.setState ({text1: 'BATTERY_OFF'});
            this.setState ({currentstate: null});
          } else if (val.startsWith ('P') || val.startsWith ('F')) {
            // console.log(val)
            if (val.startsWith ('P')) {
              var spl = val.substring (2) + '.00';
              // console.log(typeof(spl[1]))
              this.setState ({showtext: 'Pass with BAC:' + spl});
              this.setState ({
                sources: require ('../assets/images/testpass.png'),
              });
              var d = new Date ();
              var date =
                ('0' + (d.getMonth () + 1)).slice (-2) +
                '/' +
                ('0' + d.getDate ()).slice (-2) +
                '/' +
                d.getFullYear () +
                ' ' +
                ('0' + d.getHours ()).slice (-2) +
                ':' +
                ('0' + d.getMinutes ()).slice (-2) +
                ':' +
                ('0' + d.getSeconds ()).slice (-2);

              var mytag = {
                devid: this.state.deviceid.replace (/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              this.eventpost (mytag);
              // this.readData ();
            } else {
              var spl = val.substring (1);
              // console.log(typeof(spl[1]))
              this.setState ({showtext: 'Fail wit BAC:' + spl});
              this.setState ({
                sources: require ('../assets/images/testfail.png'),
              });
              var d = new Date ();
              var date =
                ('0' + (d.getMonth () + 1)).slice (-2) +
                '/' +
                ('0' + d.getDate ()).slice (-2) +
                '/' +
                d.getFullYear () +
                ' ' +
                ('0' + d.getHours ()).slice (-2) +
                ':' +
                ('0' + d.getMinutes ()).slice (-2) +
                ':' +
                ('0' + d.getSeconds ()).slice (-2);
              var mytag = {
                devid: this.state.deviceid.replace (/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              this.eventpost (mytag);
              // this.readData ();
            }
          } else {
            // console.log(val)
            setTimeout (() => {
              this.setState ({showtext: 'Nothing to Display'});
            }, 2000);
            this.setState ({currentstate: null});
          }
        } else {
          if (this.state.currentstate == '0x81') {
            // setTimeout(()=>{
            this.setState ({showtext: 'Please Blow'});
            this.setState ({
              sources: require ('../assets/images/BREATHALYZERS.png'),
            });
            // },2000)
          } else if (this.state.currentstate == '0x83') {
            this.setState ({showtext: 'Hard Blow'});
            this.setState ({
              sources: require ('../assets/images/hardblow1.png'),
            });
          } else if (val == '0x84') {
            this.setState ({showtext: 'Analysing'});
            this.setState ({
              sources: (
                <Spinner
                  isVisible={true}
                  size={150}
                  type="Wave"
                  color="green"
                />
              ),
            });
            // this.Takepicture(false);
            // this.takePicture.bind(this)
            // this.launchCamera()
          } else if (val == '0x85') {
            this.setState ({showtext: 'Calibrate the Device'});
            this.setState ({
              sources: require ('../assets/images/calibrate.png'),
            });
          } else if (val == '0x86') {
            this.setState ({showtext: 'Blow Timeout'});
            this.setState ({sources: require ('../assets/images/timeout.png')});
          } else if (val == '0x87') {
            this.setState ({showtext: 'Calibration Done'});
            this.setState ({
              sources: require ('../assets/images/calibrationDone.png'),
            });
            setTimeout (() => {
              this.setState ({currentstate: null});
            }, 1000);
          } else if (this.state.currentstate == '0x82') {
            this.setState ({showtext: 'Insufficient Volume'});
            this.setState ({
              sources: require ('../assets/images/In-sufficientvolume1.png'),
            });
          } else if (val == '0x88') {
            this.setState ({showtext: 'Calibration Failed'});
            this.setState ({
              sources: require ('../assets/images/calibrationFail.png'),
            });
            setTimeout (() => {
              this.setState ({currentstate: null});
            }, 1000);
          } else if (this.state.currentstate == '0x5') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_5'});
              this.setState ({
                sources1: require ('../assets/images/battery_5.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x1E') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_30'});
              this.setState ({
                sources1: require ('../assets/images/battery_30.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x46') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_70'});
              this.setState ({
                sources1: require ('../assets/images/battery_70.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x64') {
            setTimeout (() => {
              this.setState ({showtext: 'battery_100'});
              this.setState ({
                sources1: require ('../assets/images/battery_100.png'),
              });
              setTimeout (() => {
                this.setState ({currentstate: null});
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x65') {
            this.setState ({text1: 'BATTERY_OFF'});
            this.setState ({currentstate: null});
          } else if (val.startsWith ('P') || val.startsWith ('F')) {
            if (val.startsWith ('P')) {
              var spl = val.substring (1);
              // console.log(typeof(spl[1]))
              this.setState ({showtext: 'Pass with BAC:' + spl});
              this.setState ({
                sources: require ('../assets/images/testpass.png'),
              });
              var d = new Date ();
              var date =
                ('0' + (d.getMonth () + 1)).slice (-2) +
                '/' +
                ('0' + d.getDate ()).slice (-2) +
                '/' +
                d.getFullYear () +
                ' ' +
                ('0' + d.getHours ()).slice (-2) +
                ':' +
                ('0' + d.getMinutes ()).slice (-2) +
                ':' +
                ('0' + d.getSeconds ()).slice (-2);
              var mytag = {
                devid: this.state.deviceid.replace (/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              var folderPath =
                RNFS.DocumentDirectoryPath + '/Alcobrake/' + 'events/';
              RNFS.mkdir (folderPath).catch (err => {
                console.log (err);
              });
              const path =
                folderPath +
                this.state.deviceid.replace (/:/g, '_') +
                '_' +
                date +
                '.txt';
              RNFS.write (path, JSON.stringify (mytag))
                .then (() => {
                  console.log ('WRITTEN');
                })
                .catch (error => {
                  console.log (error);
                });
            } else {
              var spl = val.substring (1);
              // console.log(typeof(spl[1]))
              this.setState ({showtext: 'Fail wit BAC:' + spl});
              this.setState ({
                sources: require ('../assets/images/testfail.png'),
              });
              var d = new Date ();
              var date =
                ('0' + (d.getMonth () + 1)).slice (-2) +
                '/' +
                ('0' + d.getDate ()).slice (-2) +
                '/' +
                d.getFullYear () +
                ' ' +
                ('0' + d.getHours ()).slice (-2) +
                ':' +
                ('0' + d.getMinutes ()).slice (-2) +
                ':' +
                ('0' + d.getSeconds ()).slice (-2);
              var mytag = {
                devid: this.state.deviceid.replace (/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              var folderPath =
                RNFS.DocumentDirectoryPath + '/Alcobrake/' + 'events/';
              RNFS.mkdir (folderPath).catch (err => {
                console.log (err);
              });
              const path =
                folderPath +
                this.state.deviceid.replace (/:/g, '_') +
                '_' +
                date +
                '.txt';
              RNFS.write (path, JSON.stringify (mytag))
                .then (() => {
                  console.log ('WRITTEN');
                })
                .catch (error => {
                  console.log (error);
                });
            }
          } else {
            this.setState ({showtext: 'Nothing to Display'});
            this.setState ({currentstate: null});
          }
        }
      });
    }
  }

  onTaketest () {
    console.log (this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode ('79');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  console.log (characteristic.value, 'characteristic');
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                  console.log (senddata);
                })
                .then (device => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice (
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log (error.message);
                          return;
                        }
                        // console.log(characteristic.uuid, characteristic.value)
                        var buffer = Buffer.from (
                          characteristic.value,
                          'base64'
                        );
                        var dec = buffer.toString ('hex');
                        var str = '';
                        for (var i = 0; i < dec.length; i += 2) {
                          var v = parseInt (dec.substr (i, 2), 16);
                          if (v) str += String.fromCharCode (v);
                        }
                        dec = str;
                        console.log ('******');
                        console.log (dec);
                        // let newarray = this.state.arr.concat(dec)
                        // this.setState({ arr: newarray })
                        // const mynewstring = this.state.arr.toString();
                        // console.log(typeof(mynewstring))
                        // console.log(this.state.arr)
                        // const folderPath = RNFS.DocumentDirectoryPath + "/LogFiles";
                        // RNFS.mkdir(folderPath).catch(err => {
                        //   console.log(err)
                        // })
                        // const path = folderPath+'/test.txt'
                        // RNFS.write(path,mynewstring).then(()=>{
                        //   console.log('WRITTEN')
                        // }).catch ((error) => { //if the function throws an error, log it out.
                        //   console.log(error);
                        // })

                        this.setcurrentState (dec);
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch (err => {
                      console.log ('4');
                      console.log (err);
                    });
                })
                .catch (err => {
                  console.log ('3');
                  console.log (err.message);
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        } else {
          this.manager.onDeviceDisconnected (device.id, (error, device) => {
            if (error) {
              console.log (error);
            }
            console.log ('Device is disconnected');
            this.setState ({text1: 'Device Disconnected'});
          });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }
  setTime () {
    let d = new Date ();
    // var date =
    // 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopppppppbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbjsjsjsjsjs'
    var dat =
      'T' +
      ('0' + d.getHours ()).slice (-2) +
      ':' +
      ('0' + d.getMinutes ()).slice (-2) +
      ':' +
      ('0' + d.getSeconds ()).slice (-2) +
      ' ' +
      ('0' + (d.getMonth () + 1)).slice (-2) +
      '/' +
      ('0' + d.getDate ()).slice (-2) +
      '/' +
      d.getFullYear ();
    let date = dat.toString ();
    const device = this.state.devices;
    console.log (this.state.deviceid);
    this.manager.isDeviceConnected (this.state.deviceid).then (res => {
      if (res) {
        console.log ('Discovering services and characteristics test');
        device.discoverAllServicesAndCharacteristics ().then (device => {
          const senddata = base64.encode (date);
          this.manager
            .writeCharacteristicWithResponseForDevice (
              this.state.deviceid,
              '000000ff-0000-1000-8000-00805f9b34fb',
              '0000ff01-0000-1000-8000-00805f9b34fb',
              senddata
            )
            .then (characteristic => {
              console.log (characteristic.value, 'characteristic');
              this.setState ({characteristicsUUID: characteristic.uuid});
              this.setState ({serviceUUID: characteristic.serviceUUID});
              console.log (base64.decode (senddata));
            });
        });
      }
    });
  }

  calibration () {
    console.log (this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode ('80');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  console.log (characteristic.value, 'characteristic');
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                  console.log (senddata);
                })
                .then (device => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice (
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log (error.message);
                          return;
                        }
                        // console.log(characteristic.uuid, characteristic.value)
                        var buffer = Buffer.from (
                          characteristic.value,
                          'base64'
                        );
                        var dec = buffer.toString ('hex');
                        var str = '';
                        for (var i = 0; i < dec.length; i += 2) {
                          var v = parseInt (dec.substr (i, 2), 16);
                          if (v) str += String.fromCharCode (v);
                        }
                        dec = str;
                        console.log ('******');
                        console.log (dec);
                        this.setcurrentState (dec);
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch (err => {
                      console.log ('4');
                      console.log (err);
                    });
                })
                .catch (err => {
                  console.log ('3');
                  console.log (err.message);
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        } else {
          this.manager.onDeviceDisconnected (device.id, (error, device) => {
            if (error) {
              console.log (error);
            }
            console.log ('Device is disconnected');
            this.setState ({text1: 'Device Disconnected'});
          });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }

  Logread () {
    // this.setState ({isVisible: true});
    console.log (this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            // .then (device => {
            //   // console.log("2");
            //   // console.log(device,'device')
            //   const senddata = base64.encode ('90');
            //   this.manager
            //     .writeCharacteristicWithResponseForDevice (
            //       this.state.deviceid,
            //       '000000ff-0000-1000-8000-00805f9b34fb',
            //       '0000ff01-0000-1000-8000-00805f9b34fb',
            //       senddata
            //     )
            //     .then (characteristic => {
            //       console.log (characteristic.value, 'characteristic');
            //       this.setState ({characteristicsUUID: characteristic.uuid});
            //       this.setState ({serviceUUID: characteristic.serviceUUID});
            //       console.log (senddata);
            //     })
            .then (() => {
              // console.log("2");
              // console.log(device,'device')
              this.manager
                .monitorCharacteristicForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  (error, characteristic) => {
                    if (error) {
                      console.log (error.message);
                      return;
                    }
                    console.log (characteristic.value);
                    // var buffer = Buffer.from(characteristic.value, 'base64');
                    // var dec = buffer.toString('hex');
                    // var str = '';
                    // for (var i = 0; i < dec.length; i += 2) {
                    //   var v = parseInt(dec.substr(i, 2), 16);
                    //   if (v) str += String.fromCharCode(v);
                    // }
                    // dec = str
                    var dec = base64.decode (characteristic.value);
                    console.log ('******');
                    console.log (dec, this.state.count++);
                    // this.setState ({count: 1});
                    // let newarray = this.state.arr.concat (dec);
                    // this.setState ({arr: newarray});
                    // const mynewstring = this.state.arr.toString ();
                    // console.log (typeof mynewstring);
                    // console.log (this.state.arr);
                    // const folderPath =
                    //   RNFS.DocumentDirectoryPath + '/LogFiles';
                    // RNFS.mkdir (folderPath).catch (err => {
                    //   console.log (err);
                    // });
                    // const path = folderPath + '/test.txt';
                    // RNFS.write (path, mynewstring)
                    //   .then (() => {
                    //     console.log ('WRITTEN');
                    //   })
                    //   .catch (error => {
                    //     console.log (error);
                    //   });
                    // this.setState ({currentstate: dec});
                    // console.log(this.state.arr, 'arr')
                  }
                )
                .catch (err => {
                  console.log ('4');
                  console.log (err);
                });
            })
            .catch (err => {
              console.log ('3');
              console.log (err.message);
            });
          // })
          // .catch (err => {
          //   console.log ('2');
          //   console.log (err);
          // });
        } else {
          this.manager.onDeviceDisconnected (device.id, (error, device) => {
            if (error) {
              console.log (error);
            }
            console.log ('Device is disconnected');
            this.setState ({text1: 'Device Disconnected'});
          });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }
  memoryReset () {
    console.log (this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected (this.state.deviceid)
      .then (res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log ('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics ()
            .then (device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode ('91');
              this.manager
                .writeCharacteristicWithResponseForDevice (
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then (characteristic => {
                  console.log (characteristic.value, 'characteristic');
                  this.setState ({characteristicsUUID: characteristic.uuid});
                  this.setState ({serviceUUID: characteristic.serviceUUID});
                  console.log (senddata);
                })
                .then (device => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice (
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log (error.message);
                          return;
                        }
                        console.log (characteristic.uuid, characteristic.value);
                        var buffer = Buffer.from (
                          characteristic.value,
                          'base64'
                        );
                        var dec = buffer.toString ('hex');
                        // var str = '';
                        // for (var i = 0; i < dec.length; i += 2) {
                        //   var v = parseInt(dec.substr(i, 2), 16);
                        //   if (v) str += String.fromCharCode(v);
                        // }
                        // dec = str
                        console.log ('******');
                        console.log (dec);
                        // let newarray = this.state.arr.concat(dec)
                        // this.setState({ arr: newarray })
                        // const mynewstring = this.state.arr.toString();
                        // console.log(typeof(mynewstring))
                        // console.log(this.state.arr)
                        // const folderPath = RNFS.DocumentDirectoryPath + "/LogFiles";
                        // RNFS.mkdir(folderPath).catch(err => {
                        //   console.log(err)
                        // })
                        // const path = folderPath+'/test.txt'
                        // RNFS.write(path,mynewstring).then(()=>{
                        //   console.log('WRITTEN')
                        // }).catch ((error) => { //if the function throws an error, log it out.
                        //   console.log(error);
                        // })

                        this.setcurrentState (dec);
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch (err => {
                      console.log ('4');
                      console.log (err);
                    });
                })
                .catch (err => {
                  console.log ('3');
                  console.log (err.message);
                });
            })
            .catch (err => {
              console.log ('2');
              console.log (err);
            });
        } else {
          this.manager.onDeviceDisconnected (device.id, (error, device) => {
            if (error) {
              console.log (error);
            }
            console.log ('Device is disconnected');
            this.setState ({text1: 'Device Disconnected'});
          });
        }
      })
      .catch (err => {
        console.log ('1');
        console.log (err);
      });
  }

  render () {
    return (
      <View style={{flex: 1, backgroundColor: '#e0e0e0'}}>
        <View
          style={{
            justifyContent: 'center',
            height: 70,
            backgroundColor: 'green',
          }}
        >
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 17,
              fontWeight: 'bold',
            }}
          >
            Bluetooth is : {this.state.bluetoothState}
          </Text>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 17,
              fontWeight: 'bold',
            }}
          >
            Connected to : {this.state.text1}
          </Text>
        </View>
        {this.state.text1 == 'ALCOBRAKE' &&
          this.state.bluetoothState == 'PoweredOn' &&
          this.state.deviceid
          ? <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            >
              {this.state.currentstate == null
                ? <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      padding: 10,
                    }}
                  >
                    {/* <Text style={styles.titleText}>Please TAKE TEST</Text> */}
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: -110,
                      }}
                    >
                      <Text>{this.state.sources1}</Text>
                      <TouchableHighlight
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#00897b',
                          width: 200,
                          height: 50,
                          borderRadius: 50,
                        }}
                        onPress={() => this.onTaketest ()}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                          }}
                        >
                          TAKE TEST
                        </Text>
                        {/* <Image style={{ width: 350, height: 350, resizeMode: 'contain' }} source={require('../assets/images/BREATHALYZER.png')} /> */}
                      </TouchableHighlight>
                      <TouchableOpacity
                        onPress={() => this.setTime ()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#0097a7',
                          width: 200,
                          height: 50,
                          marginTop: 20,
                          borderRadius: 50,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                          }}
                        >
                          TimeSet
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.calibration ()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#26a69a',
                          width: 200,
                          height: 50,
                          marginTop: 20,
                          borderRadius: 50,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                          }}
                        >
                          Calibration
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.Logread ()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#009688',
                          width: 200,
                          height: 50,
                          marginTop: 20,
                          borderRadius: 50,
                        }}
                      >
                        <Spinner
                          style={styles.spinner}
                          isVisible={this.state.isVisible}
                          size={150}
                          type="FadingCircleAlt"
                          color="#ff5722"
                        />
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                          }}
                        >
                          LogRead
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => this.memoryReset ()}
                        style={{
                          justifyContent: 'center',
                          backgroundColor: '#006064',
                          width: 200,
                          height: 50,
                          marginTop: 20,
                          borderRadius: 50,
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                            textAlign: 'center',
                          }}
                        >
                          Memory Reset
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                : <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    {/* {this.state.currentstate == '0x81'? */}
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 10,
                      }}
                    >
                      <Text style={styles.titleText}>
                        {this.state.showtext}
                      </Text>
                      {/* <Text style={styles.titleText}>{this.state.currentstate}</Text> */}
                      {this.state.currentstate == '0x84'
                        ? <View>
                            <View
                              style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: -150,
                              }}
                            >
                              <Text>{this.state.sources}</Text>
                            </View>
                            {/* <View>
                              <RNCamera
                                ref={ref => (this.camera = ref)}
                                style={{
                                  flex: 0.5,
                                  height: 5,
                                  width: 300,
                                }}
                                type={this.state.cameratype}
                                faceDetectionClassifications={
                                  RNCamera.Constants.FaceDetection
                                    .Classifications.none
                                }
                                faceDetectionMode={
                                  RNCamera.Constants.FaceDetection.Mode.accurate
                                }
                                captureAudio={false}
                                androidCameraPermissionOptions={{
                                  title: 'Permission to use camera',
                                  message: 'We need your permission to use your camera',
                                  buttonPositive: 'Ok',
                                  buttonNegative: 'Cancel',
                                }}
                              /> */}
                            {/* <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                              <Icon name='camera' size={50} />
                            </TouchableOpacity> */}
                            {/* </View> */}
                          </View>
                        : <View
                            style={{
                              flex: 1,
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginTop: -110,
                            }}
                          >
                            {/* <TouchableHighlight
                              onPress={() => this.onTaketest ()}
                            > */}
                              <Image
                                style={{
                                  width: 350,
                                  height: 350,
                                  resizeMode: 'contain',
                                }}
                                source={this.state.sources}
                              />
                            {/* </TouchableHighlight> */}
                          </View>}
                    </View>
                  </View>}
            </View>
          : null}
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 0.9,
    backgroundColor: '#F5FCFF',
  },
  fixedRatio: {
    flex: 1,
  },
  capture: {
    flex: 0,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    marginTop: 150,
  },
  topBar: {
    height: 56,
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    elevation: 6,
    backgroundColor: '#2D6824',
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'center',
    color: '#FFFFFF',
  },
  enableInfoWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tab: {
    alignItems: 'center',
    flex: 0.5,
    height: 56,
    justifyContent: 'center',
    borderBottomWidth: 6,
    borderColor: 'transparent',
  },
  connectionInfoWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  connectionInfo: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 18,
    marginVertical: 10,
    color: '#238923',
  },
  listContainer: {
    borderColor: '#ccc',
    borderTopWidth: 0.5,
  },
  listItem: {
    flex: 1,
    height: 48,
    paddingHorizontal: 16,
    borderColor: '#ccc',
    borderBottomWidth: 0.5,
    justifyContent: 'center',
  },
  fixedFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#ddd',
  },
  button: {
    height: 36,
    margin: 5,
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#7B1FA2',
    fontWeight: 'bold',
    fontSize: 14,
  },
  buttonRaised: {
    backgroundColor: '#7B1FA2',
    borderRadius: 2,
    elevation: 2,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    position: 'absolute',
    width: 250,
    marginBottom: 5,
    borderRadius: 30,
  },
  loginButton: {
    backgroundColor: 'green',
    marginBottom: 20,
  },
  loginText: {
    color: 'white',
  },
  bgimage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignSelf: 'stretch',
  },
  titleText: {
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color: 'green',
    marginTop: 20,
  },
  titleCard: {
    fontSize: 50,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color: 'orange',
  },
});

export default Dashboard;
