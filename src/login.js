import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  TouchableOpacity,
  AsyncStorage,
  ImageBackground,
  Platform,
  PermissionsAndroid,
  BackHandler,
  LogBox
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
import LocationServicesDialogBox
  from 'react-native-android-location-services-dialog-box';
import Geolocation from '@react-native-community/geolocation';
import { BleManager } from 'react-native-ble-plx';
import { PERMISSIONS, request } from 'react-native-permissions';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { BleManager } from 'react-native-ble-plx';


async function saveData(readData) {
  try {
    await AsyncStorage.setItem('userdata', JSON.stringify(readData));
    return false;
  } catch (error) {
    alert(error);
  }
}

export default class Login extends Component {
  constructor(props) {
    super(props);
    // this.manager = new BleManager();
    this.state = {
      emailId: 'alcobrakebt@gmail.com',
      password: 'Alcobrake101',
      // location: false,
      lat: 0.000,
      lon: 0.000,
      press: true,
    };
  }

  componentDidMount() {
    this.requestLocationPermission();
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
    LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
    LogBox.ignoreAllLogs();
  }

  requestLocationPermission = async () => {
    if (Platform.OS == 'ios' && parseInt(Platform.Version, 10) > 12) {
      Geolocation.requestAuthorization();
      // request(PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL).then(result => {
      //     console.log(result)
      // })
    } else {
      try {
        const granted = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
          PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
          PermissionsAndroid.PERMISSIONS.CAMERA,
          // PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ]).then(result => {
          console.log(result)
          if (result['android.permission.ACCESS_FINE_LOCATION'] === granted) {
            Geolocation.getCurrentPosition(
              position => {
                console.log(position.coords);
                // this.setState({ location: position });
                this.setState({
                  lat: position.coords.latitude,
                  lon: position.coords.longitude,
                });
              },
              error => {
                console.log(error.code, error.message);
                // this.setState({ location: false });
              },
            );
            console.log('You can use Geolocation');
            return true;
          } else {
            console.log('User refuse');
            BackHandler.exitApp();
            console.log('You cannot use Geolocation');
            return false;
          }

        })

      } catch (err) {
        return false;
      }
    }
  };

  LoginView() {
    console.log("oooooooooooo")
    var data = {
      emailid: this.state.emailId,
      password: this.state.password,
    };
    axios
      .post(
        'https://robopower.xyz/breathalyzer/login/onlogin',
        JSON.stringify(data)
      )
      .then(
        response => {
          console.log(response.data);
          if (response.data != 'user not found') {
            // Alert.alert("login success")
            console.log('success');
            saveData(response.data);
            this.props.navigation.navigate('Dashboard', {
              lat: this.state.lat,
              lon: this.state.lon,
            });
            
          } else {
            alert(response.data);
          }
        }
        ,
        error => {
          alert(error);
        }
      );
  }

  handleEmailId = text => {
    this.setState({ emailId: text });
  };
  handlePassword = text => {
    this.setState({ password: text });
  };

  render() {
    return (
      // <ImageBackground
      //   source={require ('../assets/icos/a200device.jpg')}
      //   style={styles.image}
      // >
      <View style={styles.container}>
        <Image
          source={require('../assets/images/alcobrake.jpg')}
          style={{ height: hp('13%'), width: wp('60%'), resizeMode: 'contain', marginTop: hp('20%') }}
        />
        {/* <View style={styles.container1}> */}
        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={require('../assets/images/mail.png')}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Email"
            placeholderTextColor={'black'}
            keyboardType="email-address"
            value={this.state.emailId}
            underlineColorAndroid="transparent"
            onChangeText={this.handleEmailId}
          />
        </View>

        <View style={styles.inputContainer1}>
          <Image
            style={styles.inputIcon}
            source={require('../assets/icos/password55.png')}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Password"
            placeholderTextColor={'black'}
            keyboardType="default"
            secureTextEntry={this.state.press}
            underlineColorAndroid="transparent"
            value={this.state.password}
            onChangeText={this.handlePassword}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({ press: !this.state.press });
            }}
          >
            <Icon
              name={
                this.state.press == false ? 'visibility' : 'visibility-off'
              }
              size={25}
              color="steelblue"
              style={{ paddingRight: wp('5.5%') }}
            />
          </TouchableOpacity>
        </View>
        <TouchableHighlight
          style={styles.buttonContainer}
          onPress={() => 
            this.LoginView()
            // this.props.navigation.navigate('Dashboard', {
            //   lat: this.state.lat,
            //   lon: this.state.lon,})
          }
        >
          <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>
      </View>
      // </View>
      // </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#e8f5e9'
  },
  // container1: {
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    width: wp('65%'),
    height: hp('6%'),
    marginBottom: hp('3%'),
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp('13%')
  },
  inputContainer1: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    width: wp('65%'),
    height: hp('6%'),
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: hp('5%'),
    marginLeft: wp('1.5%'),
    borderBottomColor: '#FFFFFF',
    flex: 1,
    color: 'black',
  },
  inputIcon: {
    width: wp('7%'),
    height: hp('3.5%'),
    marginLeft: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: hp('5.5%'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: wp('30%'),
    borderRadius: 30,
    backgroundColor: 'green',
    marginTop: hp('5%')
  },
  loginText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  }
});
