import React from 'react';
import { View, Text, Image, Platform } from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Buffer } from 'buffer';
import NetInfo from '@react-native-community/netinfo';


export default class battery extends React.Component {
    constructor(props) {
        super(props);
        this.manager = new BleManager();
        this.state = {
            currentState: null,
            sources1: null,
            bluetoothState: null,
            location: false,
            lat: 0.000,
            lon: 0.000,
            deviceid: '',
            serviceUUID: '',
            characteristicsUUID: '',
            text1: 'No connections',
            devices:null
        }
    }

    componentDidMount() {
        this.requestLocationPermission()
        this.manager.onStateChange(state => {
            this.setState({ bluetoothState: state });
            if (state == 'PoweredOn') {
                this.scanAndConnect();
            } else if (state == 'PoweredOff') {
                if (Platform.OS == 'android') {
                    this.manager.enable();
                    this.setState({ currentstate: state });
                    this.scanAndConnect();
                } else {
                    Alert.alert(
                        '"App" would like to use Bluetooth.',
                        'This app uses Bluetooth to connect to and share information with your .....',
                        [
                            {
                                text: "Don't allow",
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                            },
                            {
                                text: 'Turn ON',
                                onPress: () => {
                                    Linking.openURL('App-prefs:root=Bluetooth');
                                    this.scanAndConnect();
                                },
                            },
                        ]
                    );
                }
            } else {
                console.log('sss' + state);
            }
        }, true);
        // this.readData();
    }

    requestLocationPermission = async () => {
        if (Platform.OS == 'ios' && parseInt(Platform.Version, 10) > 12) {
            Geolocation.requestAuthorization();
            // request(PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL).then(result => {
            //     console.log(result)
            // })
        } else {
            try {
                const granted = await PermissionsAndroid.requestMultiple([
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT,
                    PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                ]).then(result => {
                    if (result['android.permission.ACCESS_FINE_LOCATION'] === granted) {
                        Geolocation.getCurrentPosition(
                            position => {
                                console.log(position.coords);
                                this.setState({ location: position });
                                this.setState({
                                    lat: position.coords.latitude,
                                    lon: position.coords.longitude,
                                });
                            },
                            error => {
                                console.log(error.code, error.message);
                                this.setState({ location: false });
                            },
                        );
                        console.log('You can use Geolocation');
                        return true;
                    } else {
                        console.log('User refuse');
                        BackHandler.exitApp();
                        console.log('You cannot use Geolocation');
                        return false;
                    }

                })

            } catch (err) {
                return false;
            }
        }
    };


    scanAndConnect() {
        // 000000ee-0000-1000-8000-00805f9b34fb
        this.setState({ text1: 'Scanning...' });
        this.manager.startDeviceScan(
            null,
            { allowDuplicates: false },
            (error, device) => {
                if (null) {
                    console.log('null');
                    this.manager.stopDeviceScan();
                }
                if (error) {
                    console.log('Error in scan=> ' + error);
                    this.setState({ text1: error.message });
                    this.manager.stopDeviceScan();
                    return;
                }
                if (device.name == 'ALCOBRAKE300') {
                    // console.log(device);
                    this.setState({ serviceUUID: device.serviceUUIDs[0] });
                    this.setState({ text1: device.name });
                    this.setState({ deviceid: device.id });
                    this.setState({ devices: device });
                    this.manager.stopDeviceScan();
                    console.log(device.id, '=====> MACID');
                    device.connect()
                        .then((device) => {
                            // this.info("Discovering services and characteristics")
                            console.log("Connected...Discovering services and characteristics");
                            return device.discoverAllServicesAndCharacteristics()
                        }).then((device) => {
                            console.log('data')
                            this.manager
                                .monitorCharacteristicForDevice(
                                    this.state.deviceid,
                                    '000000ff-0000-1000-8000-00805f9b34fb',
                                    '0000ff01-0000-1000-8000-00805f9b34fb',
                                    (error, characteristic) => {
                                        console.log('data1')
                                        if (error) {
                                            console.log(error.message);
                                            return;
                                        }
                                        // console.log(characteristic.uuid, characteristic.value)
                                        var buffer = Buffer.from(
                                            characteristic.value,
                                            'base64'
                                        );
                                        var dec = buffer.toString('hex');
                                        var str = '';
                                        for (var i = 0; i < dec.length; i += 2) {
                                            var v = parseInt(dec.substr(i, 2), 16);
                                            if (v) str += String.fromCharCode(v);
                                        }
                                        dec = str;
                                        console.log('******');
                                        console.log(dec);
                                        this.setState({ currentState: dec.toString() })
                                        this.setcurrentState(dec)
                                    }
                                )
                        })
                }
            }
        );
    }

    setcurrentState(val) {
        this.setState({ currentState: val.toString() })
        if (val) {
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    if (this.state.currentstate == '0x1E') {
                        this.setState({
                            sources1: require('../assets/images/battery_30.png'),
                        });
                        console.log(this.state.currentState, 'battery_30')
                    } else if (this.state.currentstate == '0x5') {
                        // this.setState ({showtext: 'battery_5'});
                        this.setState({
                            sources1: require('../assets/images/battery_5.png'),
                        });

                        console.log(this.state.currentState, 'battery_5')
                    }
                    else if (this.state.currentstate == '0x46') {
                        this.setState({
                            sources1: require('../assets/images/battery_70.png'),
                        });
                        console.log(this.state.currentState, 'battery_70')
                    } else if (this.state.currentstate == '0x64') {
                        this.setState({
                            sources1: require('../assets/images/battery_100.png'),
                        });
                        console.log(this.state.currentState, 'battery_30')
                    } else {
                        console.log(this.state.currentState)
                    }
                }
            })
        }
    }


    render() {
        return (
            <View>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: hp('2%'),
                    }}
                >

                    <Text
                        style={{
                            fontSize: 18,
                            color: 'black',
                            fontWeight: 'bold',
                            marginLeft: wp('3%'),
                        }}
                    >
                        CONNECTED TO:
                        {' '}
                        {this.state.bluetoothState == 'PoweredOn'
                            ? this.state.text1
                            : 'NOT CONNECTED'}
                    </Text>
                    <Icon
                        name={
                            this.state.bluetoothState == 'PoweredOn'
                                ? 'bluetooth'
                                : 'bluetooth-off'
                        }
                        size={35}
                        style={{
                            color: this.state.bluetoothState == 'PoweredOn'
                                ? 'steelblue'
                                : '#9fa8da',
                            textAlign: 'center',
                            marginRight: wp('4%'),
                        }}
                    />
                </View>
                <Text style={{ color: 'black', justifyContent: 'center', textAlign: 'center' }}>{this.state.currentState}</Text>
                <Image
                    style={{
                        width: wp('25%'),
                        height: hp('3.5%'),
                        marginTop: hp('2%'),
                        marginLeft: wp('73%')
                    }}
                // source={this.state.sources1}

                />
            </View>
        )
    }
}