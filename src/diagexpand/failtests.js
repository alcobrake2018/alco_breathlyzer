import React, { Component, useReducer } from 'react';
import { StyleSheet, View, Text, Dimensions,TouchableOpacity, Alert,Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Card } from 'react-native-elements';
import BlinkView from 'react-native-blink-view';
//  import * as Progress from 'react-native-progress';
//import ProgressBar from 'react-native-progress/Bar';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class FailTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBlinking:true,
    devices : [
        {
           devid: '20001991',
           reg: 'TG25J987',
           drivername:'Raj',
           count:'5',
        },
        {
            devid: '20119191',
            reg: 'AP25J1230',
            drivername:'Rama',
            count:'5',
         },
         {
            devid: '201123441',
            reg: 'UP25J1230',
            drivername:'Rajkot',
            count:'3',
         },
         {
            devid: '209871',
            reg: 'AP25J1230',
            drivername:'Satti',
            count:'6',
         },
       ],
      }
  }


  render() {
    // const dateToFormat = moment.utc(moment(this.state.devices.actual_arrived_time,"DD-MM-YYYY HH:mm:ss").diff(moment(this.state.devices.estimated_arrival_time,"DD-M-YYYY HH:mm:ss"))).format("HH:mm:ss");
    return (
          <ScrollView>
      <View style={styles.container}>
      {this.state.devices.map((user, i) => (
          <Card key={i} containerStyle={styles.card}>
              <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection:'column',alignItems:'flex-start'}}>
              <Text style={{fontWeight:'bold'}}>
                  DeviceId:{user.devid}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Registration:{user.reg}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Driver name :{user.drivername}
              </Text>
              {/* <Text style={{fontWeight:'bold'}}>
                Delay Time:
              <Moment titleFormat="DD-MMM-YYYY HH:MM:SS" diff="07-05-2020 10:00:00" unit="minutes" decimal>07-05-2020 11:00:00</Moment>
              </Text> */}

              </View>
              <View style={{flexDirection:'column', justifyContent: 'flex-end'}}>
              <Text style={{fontWeight:'bold'}}>
                  FailTests
              </Text>
              <AnimatedCircularProgress
          size={60}
          width={10}
          fill={100}
          tintColor="green"
          onAnimationComplete={() => console.log('onAnimationComplete')}
          backgroundColor="#3B3C66" >
{
    (fill) => (
<BlinkView blinking={this.state.isBlinking?true:false} delay={500}>
      <Text>
        {user.count}
      </Text>
      </BlinkView>
    )
  }
</AnimatedCircularProgress>   
              
</View>
  </View>
              </Card>
  ))}
            
      </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1,alignItems:'center',padding:5  },
  card:{
      
    // alignItems:'center',
    // justifyContent:'center',
      backgroundColor:'#ffff',
      width:screenWidth-10
  }
});