import React, { Component, useReducer } from 'react';
import { StyleSheet, View, Text, Dimensions,TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Card } from 'react-native-elements';
//  import * as Progress from 'react-native-progress';
//import ProgressBar from 'react-native-progress/Bar';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class Battery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    this.devices = [
        {
           devid: '20001991',
           reg: 'TG25J987',
           battery:'9'
        },
        {
            devid: '20119191',
            reg: 'AP25J1230',
            battery:'8'
         },
         {
            devid: '201123441',
            reg: 'UP25J1230',
            battery:'8.5'
         },
         {
            devid: '209871',
            reg: 'AP25J1230',
            battery:'10'
         },
       ]
  }



  render() {
    return (
          <ScrollView>
      <View style={styles.container}>
      {this.devices.map((user, i) => (
          <Card key={i} containerStyle={styles.card}>
              <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection:'column',alignItems:'flex-start'}}>
              <Text style={{fontWeight:'bold'}}>
                  DeviceId:{user.devid}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Registratio:{user.reg}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Voltage:{user.battery}V
              </Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
        <AnimatedCircularProgress
          size={80}
          width={10}
          fill={Math.round((user.battery/13)*100)}
          tintColor="red"
          onAnimationComplete={() => console.log('onAnimationComplete')}
          backgroundColor="#3d5875" >
{
    (fill) => (
      <Text>
        {Math.round((user.battery/13)*100)+'%' }
      </Text>
    )
  }
</AnimatedCircularProgress>
</View>
  </View>
              </Card>
  ))}
            
      </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1,alignItems:'center',padding:5  },
  card:{
      
    // alignItems:'center',
    // justifyContent:'center',
      backgroundColor:'#ffff',
      width:screenWidth-10
  }
});