import React, { Component, useReducer } from 'react';
import { StyleSheet, View, Text,TextInput, Dimensions,TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class Otp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp:null
    }
    this.devices = []
    
  }
  enterotp= (text) => {
    this.setState({ otp: text })
 }
 setotp(otp) {
  console.log(typeof(otp))
  fetch('http://192.168.4.1/emi', {
  method: 'POST',
  body: otp
}).then(response =>{
  console.log(response)
})
 }


  render() {
    return (
          <ScrollView>
      <View style={styles.container}>
      <TextInput style = {styles.inputbox}
               underlineColorAndroid = "transparent"
               placeholder = "Please enter 0 or 1"
               placeholderTextColor = "red"
               autoCapitalize = "none"
               onChangeText = {this.enterotp}/>
                <TouchableOpacity
               style = {styles.sbtn}
               onPress = {
                  () => this.setotp(this.state.otp)
               }>
               <Text style= {styles.stext}> SET </Text>
            </TouchableOpacity>
      </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1,alignItems:'center',padding:5  },
  inputbox:{
    marginTop:10,
    borderRadius:20,  
      backgroundColor:'pink',
      width:screenWidth-10
  },
  sbtn:{
    marginTop:10,
    borderRadius:20,  
      backgroundColor:'green',
      // alignContent:'center',
      alignItems:'center',
      width:50
      
  },
  stext:{
    color:"white",     
  }
});