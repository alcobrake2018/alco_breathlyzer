import React, { Component, useReducer } from 'react';
import { StyleSheet, View, Text, Dimensions,TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { Card } from 'react-native-elements';
//  import * as Progress from 'react-native-progress';
//import ProgressBar from 'react-native-progress/Bar';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class LowFuel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    this.devices = [
        {
           devid: '20001991',
           reg: 'TG25J987',
           fuel:'90'
        },
        {
            devid: '20119191',
            reg: 'AP25J1230',
            fuel:'95'
         },
         {
            devid: '201123441',
            reg: 'UP25J1230',
            fuel:'10.5'
         },
         {
            devid: '209871',
            reg: 'AP25J1230',
            fuel:'46'
         },
       ]
  }



  render() {
    return (
          <ScrollView>
      <View style={styles.container}>
      {this.devices.map((user, i) => (
          <Card key={i} containerStyle={styles.card}>
              <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection:'column',alignItems:'flex-start'}}>
              <Text style={{fontWeight:'bold'}}>
                  DeviceId:{user.devid}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Registratio:{user.reg}
              </Text>
              <Text style={{fontWeight:'bold'}}>
                  Fuel(ltrs):{user.fuel}L
              </Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
        <AnimatedCircularProgress
          size={80}
          width={10}
          fill={Math.round((user.fuel/330)*100)}
          tintColor="#bd9b16"
          onAnimationComplete={() => console.log('onAnimationComplete')}
          backgroundColor="#3B3C66" >
{
    (fill) => (
      <Text>
        {Math.round((user.fuel/330)*100)+'%' }
      </Text>
    )
  }
</AnimatedCircularProgress>
</View>
  </View>
              </Card>
  ))}
            
      </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1,alignItems:'center',padding:5  },
  card:{
      
    // alignItems:'center',
    // justifyContent:'center',
      backgroundColor:'#ffff',
      width:screenWidth-10
  }
});