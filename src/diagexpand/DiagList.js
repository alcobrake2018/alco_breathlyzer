//This is an example code for Navigation Drawer with Custom Side bar//
//This Example is for React Navigation 3.+//
import React, { Component,Fragment  } from 'react';
//import react in our code.
import { Platform,StyleSheet,Text,View,Dimensions } from 'react-native';
import {Card, ListItem, Button, Icon} from 'react-native-elements';
import { CardViewWithIcon,CardViewWithImage } from "react-native-simple-card-view";
import { ScrollView } from 'react-native-gesture-handler';
import BlinkView from 'react-native-blink-view'

// import all basic components

export default class Diagnosis extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      isBlinking:true,
        battery  : 0,
        testfailed:0,
        fueltheft:0,
        bypass:0,
        overSpeed:0,
        delayedservices:0,
        kmstravelled:0,
        refused:0

        
        
      }
    )
  }

  //Services Component
  render() {
    return (
         <ScrollView>
        <View style={ {flex:0.50, alignItems: "center", flexDirection: "row",flexWrap: 'wrap', marginBottom :5} }>
            <View style={ {flex:1,}}>
            <BlinkView blinking={this.state.isBlinking?true:false} delay={100}>
        <CardViewWithImage
            source={require('../../../assets/icos/low-battery.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'battery low'
                  }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.battery.toString() }
             onPress={() => this.props.navigate.navigate('Batery')}
          />
          </BlinkView>
          </View>
          <View style={ {flex:1,}}>
          <CardViewWithImage
            source={require('../../../assets/icos/druk.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'Test Failed' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.testfailed.toString() }
            onPress={ () => this.setState({
                     testfailed       : this.state.testfailed + 1
            }) }
          />
          </View>
        </View>
        
        <View style={ {flex:0.50, alignItems: "center",flexDirection: "row",flexWrap: 'wrap',} }>
            <View style={ {flex:1,}}>
        <CardViewWithImage
            source={require('../../../assets/icos/gas-station.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'Fuel theft' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.fueltheft.toString() }
            onPress={ () => this.setState({
                     fueltheft       : this.state.fueltheft + 1
            }) }
          />
          </View>
          <View style={ {flex:1,}}>
          <CardViewWithImage
            source={require('../../../assets/icos/clock.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'bypass' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.bypass.toString() }
            onPress={ () => this.setState({
                bypass       : this.state.bypass + 1
            }) }
          />
          </View>
        </View>
        <View style={ {flex:0.50, alignItems: "center",flexDirection: "row",flexWrap: 'wrap',} }>
            <View style={ {flex:1,}}>
        <CardViewWithImage
            source={require('../../../assets/icos/fast.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'OverSpeed' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.overSpeed.toString() }
            onPress={ () => this.setState({
                     overSpeed       : this.state.overSpeed + 1
            }) }
          />
          </View>
          <View style={ {flex:1,}}>
          <CardViewWithImage
            source={require('../../../assets/icos/clock.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'Delayed Services' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.delayedservices.toString() }
            onPress={ () => this.setState({
                     delayedservices       : this.state.delayedservices + 1
            }) }
          />
          </View>
        </View>
        <View style={ {flex:0.50, alignItems: "center",flexDirection: "row",flexWrap: 'wrap',} }>
            <View style={ {flex:1,}}>
        <CardViewWithImage
            source={require('../../../assets/icos/road.png')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={'Kms Travelled' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                onPress={() => console.log("CardViewWithImage Clicked!")}
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.kmstravelled.toString() }
            onPress={ () => this.setState({
                     kmstravelled       : this.state.kmstravelled + 1
            }) }
          />
          </View>
          <View style={ {flex:1,}}>
          <CardViewWithImage
            source={require('../../../assets/icos/refused.jpg')}
            width={(Dimensions.get("window").width / 2) - 15}
                title={ 'Refused' }
                imageWidth={ 50 }
                imageHeight={ 50 }
                onPress={() => console.log("CardViewWithImage Clicked!")}
                roundedImage={ true }
                roundedImageValue={ 50 }
                imageMargin={ {top: 5} }
            content={ this.state.refused.toString() }
            onPress={ () => this.setState({
                     refused       : this.state.refused + 1
            }) }
          />
          </View>
        </View>
        </ScrollView>
    );
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingTop: 25,
  },
});