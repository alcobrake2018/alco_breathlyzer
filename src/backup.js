import React, { Component } from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  View,
  AsyncStorage,
  Modal,
  ActivityIndicator,
  Image,
  ImageBackground,
  Alert,
  Button,
  Linking,
} from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import { Buffer } from 'buffer';
import base64 from 'react-native-base64';
// import { Camera } from 'expo-camera';
import { Card } from 'react-native-elements';
import NetInfo from '@react-native-community/netinfo';
import axios from 'axios';
import RNFS from 'react-native-fs';
// import * as ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-image-crop-picker';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var Spinner = require('react-native-spinkit');

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.manager = new BleManager();
    this.state = {
      camera: null,
      imagebase64: null,
      cameraPermission: null,
      cameratype: RNCamera.Constants.Type.front,
      imageArray: [],
      deviceid: '',
      serviceUUID: '',
      characteristicsUUID: '',
      text1: 'No connections',
      makedata: [],
      showToast: false,
      notificationReceiving: false,
      bluetoothState: '',
      currentstate: null,
      sources: null,
      sources1: null,
      showtext: null,
      devices: null,
      result: null,
      image: null,
      images: null,
      video: '',
      isVideo: true,
      videoSource: '',
      isFaceDetected: false,
      filepath: {
        data: '',
        uri: '',
      },
      fileData: '',
      fileUri: '',
      arr: [],
      count: 1,
      lat: props.route.params.lat,
      lon: props.route.params.lon,
      imageDate: null,
      isVisible: false,
      logRead: null,
      Content: [],
      tableHead: ['Sno', 'Date', 'Time', 'Events'],
      tableData: [
        ['1', '1', '2', '3'],
        ['2', 'a', 'b', 'c'],
        ['3', '1', '2', '3'],
        ['4', 'a', 'b', 'c']
      ],
      Imagedata:null
    };
  }

  componentWillMount() {
    // this.downloaddata()
    // console.log(this.state.lat);
    // this.manager = new BleManager();
    this.manager.onStateChange(state => {
      this.setState({ bluetoothState: state });
      if (state == 'PoweredOn') {
        this.scanAndConnect();
      } else if (state == 'PoweredOff') {
        if (Platform.OS == 'android') {
          this.manager.enable();
          this.setState({ currentstate: state });
          this.scanAndConnect();
        } else {
          Alert.alert(
            '"App" would like to use Bluetooth.',
            'This app uses Bluetooth to connect to and share information with your .....',
            [
              {
                text: "Don't allow",
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Turn ON',
                onPress: () => {
                  Linking.openURL('App-prefs:root=Bluetooth');
                  this.scanAndConnect();
                },
              },
            ]
          );
        }
      } else {
        console.log('sss' + state);
      }
    }, true);
    this.readData();
  }
  takePicture = async () => {
    var d = new Date();
    var date =
      ('0' + (d.getMonth() + 1)).slice(-2) +
      '/' +
      ('0' + d.getDate()).slice(-2) +
      '/' +
      d.getFullYear() +
      ' ' +
      ('0' + d.getHours()).slice(-2) +
      ':' +
      ('0' + d.getMinutes()).slice(-2) +
      ':' +
      ('0' + d.getSeconds()).slice(-2);
    this.setState({ imageDate: date });
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      try {
        const data = await this.camera.takePictureAsync(options);
        // console.log(
        //   data.uri,
        //   this.state.imageDate,
        //   this.state.deviceid.replace(/:/g, '_')
        // );
        this.setState({ imagebase64: data.base64 });
        imgdata = {
          devid: this.state.deviceid.replace(/:/g, '_'),
          time_stamp: this.state.imageDate,
          content: this.state.imagebase64,
        };
        this.imgpost(imgdata);
      } catch (err) {
        console.log(
          'Error',
          'Failed to take picture: ' + (err.message || err)
        );
        return;
      }
    }
  };

  imgpost(val) {
    axios({
      url: 'http://robopower.xyz/breathalyzer/tests',
      method: 'POST',
      data: val,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then(resp => {
        // console.log(resp.config.data, 'imagesuccess')
        setTimeout(() => {
          this.setState({ imagebase64: null });
        }, 1000)
      })
      .catch(error => console.error(error));
  }

  scanAndConnect() {
    // 000000ee-0000-1000-8000-00805f9b34fb
    this.setState({ text1: 'Scanning...' });
    this.manager.startDeviceScan(
      null,
      { allowDuplicates: false },
      (error, device) => {
        if (null) {
          // console.log('null');
          this.manager.stopDeviceScan();
        }
        if (error) {
          console.log('Error in scan=> ' + error);
          this.setState({ text1: error.message });
          this.manager.stopDeviceScan();
          return;
        }
        console.log(device)
        if (device.name == 'ALCOBRAKE300') {
          // console.log(device);
          this.setState({ serviceUUID: device.serviceUUIDs[0] });
          this.setState({ text1: device.name });
          this.setState({ deviceid: device.id });
          this.setState({ devices: device });
          // console.log(this.state.devices)
          this.manager.stopDeviceScan();
          // console.log(device.id, '=====> MACID');
          let d = new Date();
          var dat =
            'T' +
            ('0' + d.getHours()).slice(-2) +
            ':' +
            ('0' + d.getMinutes()).slice(-2) +
            ':' +
            ('0' + d.getSeconds()).slice(-2) +
            ' ' +
            ('0' + (d.getMonth() + 1)).slice(-2) +
            '/' +
            ('0' + d.getDate()).slice(-2) +
            '/' +
            d.getFullYear();
          let date = dat.toString();
          device
            .connect({ requestMTU: 512 })
            .then((device) => {
              // this.info("Discovering services and characteristics")
              // console.log("Connected...Discovering services and characteristics");
              return device.discoverAllServicesAndCharacteristics()
            })
            .then((device) => {
              console.log(device)
              console.log('data')
              console.log(this.state.deviceid)
              this.manager.monitorCharacteristicForDevice(this.state.deviceid,
                '000000ff-0000-1000-8000-00805f9b34fb',
                '0000ff01-0000-1000-8000-00805f9b34fb',
                (error, characteristic) => {
                  console.log('data1')
                  if (error) {
                    console.log(error.message);
                    return;
                  }
                  // console.log(characteristic.uuid, characteristic.value)
                  var buffer = Buffer.from(
                    characteristic.value,
                    'base64'
                  )
                  var dec = buffer.toString('hex');
                  var str = '';
                  for (var i = 0; i < dec.length; i += 2) {
                    var v = parseInt(dec.substr(i, 2), 16);
                    if (v) str += String.fromCharCode(v);
                  }
                  dec = str;
                  console.log('currentState');
                  console.log(dec);
                  this.setState({ currentState: dec.toString() })
                  this.setcurrentState(dec)
                })

            })
            // .then(device => {
            //   const senddata = base64.encode(date);
            //   this.manager
            //     .writeCharacteristicWithResponseForDevice(
            //       this.state.deviceid,
            //       '000000ff-0000-1000-8000-00805f9b34fb',
            //       '0000ff01-0000-1000-8000-00805f9b34fb',
            //       senddata
            //     )
            //     .then(characteristic => {
            //       // console.log(characteristic.value, 'characteristic');
            //       this.setState({ characteristicsUUID: characteristic.uuid });
            //       this.setState({ serviceUUID: characteristic.serviceUUID });
            //       console.log(base64.decode(senddata),'date');
            //     });
            // })

            .then(device => {
              this.manager.onDeviceDisconnected(device.id, (error, device) => {
                if (error) {
                  console.log(error);
                }
                console.log('Device is disconnected');
                this.setState({ text1: 'Device Disconnected' });
              });
            })
            .catch(error => {
              console.log('my' + error);
            });
        }
      }
    );
  }

  eventpost(val) {
    axios({
      url: 'http://robopower.xyz/breathalyzer/tests',
      method: 'POST',
      data: JSON.stringify(val),
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Basic YnJva2VyOmJyb2tlcl8xMjM=',
      },
    })
      .then(resp => console.log(resp.config.data, 'datasuccess'))
      .catch(error => console.error(error));
  }

  // downloaddata() {
  //   axios({
  //     url: 'https://robopower.xyz/breathalyzer/brfw/otap',
  //     method: 'GET'
  //   })
  //     .then(resp => {
  //       if (resp.data) {
  //         console.log(resp.data)
  //       }

  //     })
  //   // var RNFS = require('react-native-fs');

  //   // var uploadUrl = 'http://requestb.in/XXXXXXX';  // For testing purposes, go to http://requestb.in/ and create your own link
  //   // // create an array of objects of the files you want to upload
  //   // var files = [
  //   //   {
  //   //     name: 'test1',
  //   //     filename: 'test1.txt',
  //   //     filepath: RNFS.DocumentDirectoryPath + '/LogFiles/test1.txt',
  //   //     filetype: 'pdf'
  //   //   }
  //   // ];

  //   // var upload
  //   //   = (response) => {
  //   //     var jobId = response.jobId;
  //   //     console.log('UPLOAD HAS BEGUN! JobId: ' + jobId);
  //   //   };

  //   // var uploadProgress = (response) => {
  //   //   var percentage = Math.floor((response.totalBytesSent / response.totalBytesExpectedToSend) * 100);
  //   //   console.log('UPLOAD IS ' + percentage + '% DONE!');
  //   // };

  //   // RNFS.uploadFiles({
  //   //   toUrl: uploadUrl,
  //   //   files: files,
  //   //   method: 'POST',
  //   //   headers: {
  //   //     'Accept': 'application/json',
  //   //   },
  //   //   fields: {
  //   //     'hello': 'world',
  //   //   },
  //   //   begin: upload,
  //   //   progress: uploadProgress
  //   // }).promise.then((response) => {
  //   //   if (response.statusCode == 200) {
  //   //     console.log('FILES UPLOADED!'); // response.statusCode, response.headers, response.body
  //   //   } else {
  //   //     console.log('SERVER ERROR');
  //   //   }
  //   // })
  //   //   .catch((err) => {
  //   //     if (err.description === "cancelled") {
  //   //       // cancelled by user
  //   //     }
  //   //     console.log(err);
  //   //   })


  // }
  downloaddata() {
    axios({
      url: 'http://robopower.xyz/breathalyzer/brfw/otap',
      method: 'GET'
    })
      .then(resp => {
        if (resp.data) {
          // console.log(resp.data.file)
          RNFS.downloadFile({
            fromUrl: resp.data.file,
            toFile: `${RNFS.DocumentDirectoryPath}/react-native.bin`,
          }).promise.then((r) => {
            if(r.statusCode == 200){
              RNFS.readDir(RNFS.DocumentDirectoryPath)
              .then((result) => {
                // console.log('GOT RESULT', result);
                return Promise.all([RNFS.stat(result[0].path), result[0].path]);
              })
              .then((statResult) => {
                // console.log(statResult[0].path)
                if (statResult[0].isFile()) {
                  return RNFS.readFile(statResult[0].path, 'base64');
                }
                return 'no file';
              })
              .then((contents) => {
                // console.log(contents)
                // console.log({Imagedata:contents})
                this.setState({Imagedata:contents})
                // var originaldata = contents;
                // console.log(originaldata.length)
                // this.setState({ Content: originaldata });
                // console.log(typeof(originaldata));
              })
              .catch((err) => {
                console.log(err.message, err.code);
              });
            }else{
              console.log("Read Failed")
            }
          });
      
        }

      })

  }


  readData() {
    RNFS.readDir(RNFS.DocumentDirectoryPath + '/LogFiles')
      .then((result) => {
        console.log('GOT RESULT', result);
        return Promise.all([RNFS.stat(result[0].path), result[0].path]);
      })
      .then((statResult) => {
        console.log(statResult[0].path)
        if (statResult[0].isFile()) {
          return RNFS.readFile(statResult[0].path, 'utf8');
        }
        return 'no file';
      })
      .then((contents) => {
        var originaldata = contents;
        console.log(originaldata.length)
        this.setState({ Content: originaldata });
        console.log(typeof(originaldata));
      })
      .catch((err) => {
        console.log(err.message, err.code);
      });
    // NetInfo.fetch().then(state => {
    //   console.log(state.isConnected);
    //   if (state.isConnected) {
    //     // var pth = RNFS.DocumentDirectoryPath + '/Alcobrake' + '/events';
    //     var pth = RNFS.DocumentDirectoryPath + '/LogFiles';
    //     RNFS.readDir(pth) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
    //       .then(result => {
    //         console.log('GOT RESULT', result);
    //         // stat the first file
    //         return Promise.all([RNFS.stat(result[0].path), result[0].path]);
    //       })
    //       .then(statResult => {
    //         console.log(statResult[0], 'file');
    //         if (statResult[0].isFile()) {
    //           console.log(statResult[1]);
    //           // if we have a file, read it
    //           RNFS.readFile(statResult[1], 'utf8')
    //             .then(contents => {
    //               // log the file contents
    //               console.log('success');
    //             })
    //             .catch(err => {
    //               console.log(err.message, err.code);
    //             });
    //         }
    //         return 'no file';
    //       });

    //     // RNFS.exists(pth).then(exists => {
    //     //   if (exists) {
    //     //     RNFS.readDir(pth)
    //     //       .then(result => {
    //     //         var mypaths = [];
    //     //         result.map(res => {
    //     //           var mypth = res.path;
    //     //           if (mypth.substr(mypth.length - 3) == 'txt') {
    //     //             mypaths.push(res.path);
    //     //           }
    //     //         });
    //     //         // RNFS.readFile(`${RNFS.DocumentDirectoryPath}/Logfiles/test.txt`,
    //     //         // "utf8").then(result => {
    //     //         //   console.log('file copied:', result);
    //     //         // })
    //     //         //   .catch(err => {
    //     //         //     console.log(err);
    //     //         //   });
    //     //         console.log(mypaths)
    //     //         return Promise.all(mypaths);
    //     //       })
    //     //   }
    //     // })
    //   } else {
    //     console.log(state.isConnected, 'readData');
    //   }
    // });
  }

  setcurrentState(val) {
    if (val) {
      this.setState({ currentstate: val.toString() });
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          console.log(this.state.currentstate);
          if (this.state.currentstate == '0x81') {
            // setTimeout(()=>{
            this.setState({ showtext: 'Please Blow' });
            this.setState({
              sources: require('../assets/images/BREATHALYZERS.png'),
            });
            // },3000)
          } else if (this.state.currentstate == '0x83') {
            // setTimeout(()=>{
            this.setState({ showtext: 'Hard Blow' });
            this.setState({
              sources: require('../assets/images/hardblow1.png'),
            });
            // },3000)
          } else if (val == '0x84') {
            // setTimeout(()=>{
            this.setState({ showtext: 'Analysing' });
            setTimeout(() => {
              console.log('take image');
              this.takePicture();
            }, 1000);
            // },3000),
          } else if (val == '0x85') {
            this.setState({ showtext: 'Calibrate the Device' });
            this.setState({
              sources: require('../assets/images/calibrate.png'),
            });
          } else if (val == '0x86') {
            this.setState({ showtext: 'Blow Timeout' });
            this.setState({ sources: require('../assets/images/timeout.png') });
          } else if (val == '0x87') {
            this.setState({ showtext: 'Calibration Done' });
            this.setState({
              sources: require('../assets/images/calibrationDone.png'),
            });
            setTimeout(() => {
              this.setState({ currentstate: null });
            }, 1000);
          } else if (this.state.currentstate == '0x82') {
            this.setState({ showtext: 'Insufficient Volume' });
            this.setState({
              sources: require('../assets/images/In-sufficientvolume1.png'),
            });
          } else if (val == '0x88') {
            this.setState({ showtext: 'Calibration Failed' });
            this.setState({
              sources: require('../assets/images/calibrationFail.png'),
            });
            setTimeout(() => {
              this.setState({ currentstate: null });
            }, 2000);
          } else if (this.state.currentstate == '0x5') {
            this.setState({
              sources1: require('../assets/images/battery_5.png'),
            });
            this.setState({ currentstate: null });
          } else if (this.state.currentstate == '0x1E') {
            this.setState({
              sources1: require('../assets/images/battery_30.png'),
            });
            this.setState({ currentstate: null });
            // }, 100);
          } else if (this.state.currentstate == '0x46') {
            this.setState({
              sources1: require('../assets/images/battery_70.png'),
            });
            // setTimeout(() => {
            this.setState({ currentstate: null });
            // }, 100);
          } else if (this.state.currentstate == '0x64') {
            // this.setState ({showtext: 'battery_100'});
            this.setState({
              sources1: require('../assets/images/battery_100.png'),
            });
            // setTimeout(() => {
            this.setState({ currentstate: null });
            // },100);
          } else if (this.state.currentstate == '0x65') {
            this.setState({ text1: 'BATTERY_OFF' });
            setTimeout(() => {
              this.setState({ currentstate: null });
            }, 1000)
          } else if (val.startsWith('P') || val.startsWith('F')) {
            console.log(val);
            if (val.startsWith('P')) {
              var spl = val.substring(2) + '.00';
              // console.log(typeof(spl[1]))
              this.setState({ showtext: 'Pass with BAC:' + spl });
              this.setState({
                sources: require('../assets/images/testpass.png'),
              });
              var d = new Date();
              var date =
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '/' +
                ('0' + d.getDate()).slice(-2) +
                '/' +
                d.getFullYear() +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2);

              var mytag = {
                devid: this.state.deviceid.replace(/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              this.eventpost(mytag);
              // this.readData ();
              // setTimeout(()=>{
              //   this.setState({currentstate:null})
              // },2000)
            } else {
              var spl = val.substring(1);
              // console.log(typeof(spl[1]))
              this.setState({ showtext: 'Fail wit BAC:' + spl });
              this.setState({
                sources: require('../assets/images/testfail.png'),
              });
              var d = new Date();
              var date =
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '/' +
                ('0' + d.getDate()).slice(-2) +
                '/' +
                d.getFullYear() +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2);
              var mytag = {
                devid: this.state.deviceid.replace(/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'Fail',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              this.eventpost(mytag);
              // this.readData ();
              // setTimeout(()=>{
              //   this.setState({currentstate:null})
              // },2000)
            }
          } else {
            // console.log(val)
            // setTimeout(() => {
            this.setState({ showtext: 'Nothing to Display' });
            // }, 2000);
            this.setState({ currentstate: null });
          }
        } else {
          if (this.state.currentstate == '0x81') {
            // setTimeout(()=>{
            this.setState({ showtext: 'Please Blow' });
            this.setState({
              sources: require('../assets/images/BREATHALYZERS.png'),
            });
            // },2000)
          } else if (this.state.currentstate == '0x83') {
            this.setState({ showtext: 'Hard Blow' });
            this.setState({
              sources: require('../assets/images/hardblow1.png'),
            });
          } else if (val == '0x84') {
            this.setState({ showtext: 'Analysing' });
            setTimeout(() => {
              console.log('take image');
              this.takePicture();
            }, 1000);
          } else if (val == '0x85') {
            this.setState({ showtext: 'Calibrate the Device' });
            this.setState({
              sources: require('../assets/images/calibrate.png'),
            });
          } else if (val == '0x86') {
            this.setState({ showtext: 'Blow Timeout' });
            this.setState({ sources: require('../assets/images/timeout.png') });
          } else if (val == '0x87') {
            this.setState({ showtext: 'Calibration Done' });
            this.setState({
              sources: require('../assets/images/calibrationDone.png'),
            });
            setTimeout(() => {
              this.setState({ currentstate: null });
            }, 1000);
          } else if (this.state.currentstate == '0x82') {
            this.setState({ showtext: 'Insufficient Volume' });
            this.setState({
              sources: require('../assets/images/In-sufficientvolume1.png'),
            });
          } else if (val == '0x88') {
            this.setState({ showtext: 'Calibration Failed' });
            this.setState({
              sources: require('../assets/images/calibrationFail.png'),
            });
            setTimeout(() => {
              this.setState({ currentstate: null });
            }, 1000);
          } else if (this.state.currentstate == '0x5') {
            setTimeout(() => {
              // this.setState ({showtext: 'battery_5'});
              this.setState({
                sources1: require('../assets/images/battery_5.png'),
              });
              setTimeout(() => {
                this.setState({ currentstate: null });
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x1E') {
            setTimeout(() => {
              // this.setState ({showtext: 'battery_30'});
              this.setState({
                sources1: require('../assets/images/battery_30.png'),
              });
              setTimeout(() => {
                this.setState({ currentstate: null });
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x46') {
            setTimeout(() => {
              // this.setState ({showtext: 'battery_70'});
              this.setState({
                sources1: require('../assets/images/battery_70.png'),
              });
              setTimeout(() => {
                this.setState({ currentstate: null });
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x64') {
            setTimeout(() => {
              // this.setState ({showtext: 'battery_100'});
              this.setState({
                sources1: require('../assets/images/battery_100.png'),
              });
              setTimeout(() => {
                this.setState({ currentstate: null });
              }, 1000);
            }, 800);
          } else if (this.state.currentstate == '0x65') {
            this.setState({ text1: 'BATTERY_OFF' });
            this.setState({ currentstate: null });
          } else if (val.startsWith('P') || val.startsWith('F')) {
            if (val.startsWith('P')) {
              var spl = val.substring(1);
              // console.log(typeof(spl[1]))
              this.setState({ showtext: 'Pass with BAC:' + spl });
              this.setState({
                sources: require('../assets/images/testpass.png'),
              });
              var d = new Date();
              var date =
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '/' +
                ('0' + d.getDate()).slice(-2) +
                '/' +
                d.getFullYear() +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2);
              var mytag = {
                devid: this.state.deviceid.replace(/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              var folderPath =
                RNFS.DocumentDirectoryPath + '/Alcobrake/' + 'events/';
              RNFS.mkdir(folderPath).catch(err => {
                console.log(err);
              });
              const path =
                folderPath +
                this.state.deviceid.replace(/:/g, '_') +
                '_' +
                date +
                '.txt';
              RNFS.write(path, JSON.stringify(mytag))
                .then(() => {
                  console.log('WRITTEN');
                })
                .catch(error => {
                  console.log(error);
                });
              // setTimeout(()=>{
              //   this.setState({currentstate:null})
              // },1500)
            } else {
              var spl = val.substring(1);
              // console.log(typeof(spl[1]))
              this.setState({ showtext: 'Fail wit BAC:' + spl });
              this.setState({
                sources: require('../assets/images/testfail.png'),
              });
              var d = new Date();
              var date =
                ('0' + (d.getMonth() + 1)).slice(-2) +
                '/' +
                ('0' + d.getDate()).slice(-2) +
                '/' +
                d.getFullYear() +
                ' ' +
                ('0' + d.getHours()).slice(-2) +
                ':' +
                ('0' + d.getMinutes()).slice(-2) +
                ':' +
                ('0' + d.getSeconds()).slice(-2);
              var mytag = {
                devid: this.state.deviceid.replace(/:/g, '_'),
                timestamp: date,
                bac: spl,
                result: 'PASS',
                eventtype: 'BREATHE TEST',
                lat: this.state.lat,
                lon: this.state.lon,
              };
              var folderPath =
                RNFS.DocumentDirectoryPath + '/Alcobrake/' + 'events/';
              RNFS.mkdir(folderPath).catch(err => {
                console.log(err);
              });
              const path =
                folderPath +
                this.state.deviceid.replace(/:/g, '_') +
                '_' +
                date +
                '.txt';
              RNFS.write(path, JSON.stringify(mytag))
                .then(() => {
                  console.log('WRITTEN');
                })
                .catch(error => {
                  console.log(error);
                });
              //   setTimeout(()=>{
              //   this.setState({currentstate:null})
              // },1500)
            }
          } else {
            this.setState({ showtext: 'Nothing to Display' });
            this.setState({ currentstate: null });
          }
        }
      });
    }
  }

  onTaketest() {
    console.log(this.state.deviceid);
    const device = this.state.devices;
    // this.manager
    //   .isDeviceConnected(this.state.deviceid)
    //   .then(res => {
    //     if (res) {
    //       console.log('Discovering services and characteristics test');
    //       device
    //         .discoverAllServicesAndCharacteristics()
    //         .then(device => {
              const senddata = base64.encode('79');
              this.manager
                .writeCharacteristicWithResponseForDevice(
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then(characteristic => {
                  // console.log(characteristic.value, 'characteristic');
                  this.setState({ characteristicsUUID: characteristic.uuid });
                  this.setState({ serviceUUID: characteristic.serviceUUID });
                  // console.log(senddata,'taketest');
                })
                .then(device => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice(
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log(error.message);
                          return;
                        }
                        // console.log(characteristic.uuid, characteristic.value)
                        var buffer = Buffer.from(
                          characteristic.value,
                          'base64'
                        );
                        var dec = buffer.toString('hex');
                        var str = '';
                        for (var i = 0; i < dec.length; i += 2) {
                          var v = parseInt(dec.substr(i, 2), 16);
                          if (v) str += String.fromCharCode(v);
                        }
                        dec = str;
                        // console.log('******');
                        // console.log(dec);
                        // let newarray = this.state.arr.concat(dec)
                        // this.setState({ arr: newarray })
                        // const mynewstring = this.state.arr.toString();
                        // console.log(typeof(mynewstring))
                        // console.log(this.state.arr)
                        // const folderPath = RNFS.DocumentDirectoryPath + "/LogFiles";
                        // RNFS.mkdir(folderPath).catch(err => {
                        //   console.log(err)
                        // })
                        // const path = folderPath+'/test.txt'
                        // RNFS.write(path,mynewstring).then(()=>{
                        //   console.log('WRITTEN')
                        // }).catch ((error) => { //if the function throws an error, log it out.
                        //   console.log(error);
                        // })

                        this.setcurrentState(dec);
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch(err => {
                      console.log('4');
                      console.log(err);
                    });
                })
                .catch(err => {
                  console.log('3');
                  console.log(err.message);
                });
            // })
            // .catch(err => {
            //   console.log('2');
            //   console.log(err);
            // });
        // } else {
        //   this.manager.onDeviceDisconnected(device.id, (error, device) => {
        //     if (error) {
        //       console.log(error);
        //     }
        //     console.log('Device is disconnected');
        //     this.setState({ text1: 'Device Disconnected' });
        //   });
        // }
      // })
      // .catch(err => {
      //   console.log('1');
      //   console.log(err);
      // });
  }
  setTime() {
    let d = new Date();
    // var date =
    // 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopppppppbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbjsjsjsjsjs'
    var dat =
      'T' +
      ('0' + d.getHours()).slice(-2) +
      ':' +
      ('0' + d.getMinutes()).slice(-2) +
      ':' +
      ('0' + d.getSeconds()).slice(-2) +
      ' ' +
      ('0' + (d.getMonth() + 1)).slice(-2) +
      '/' +
      ('0' + d.getDate()).slice(-2) +
      '/' +
      d.getFullYear();
    let date = dat.toString();
    const device = this.state.devices;
    // console.log(this.state.deviceid);
    this.manager.isDeviceConnected(this.state.deviceid).then(res => {
      if (res) {
        // console.log('Discovering services and characteristics test');
        device.discoverAllServicesAndCharacteristics().then(device => {
          const senddata = base64.encode(date);
          this.manager
            .writeCharacteristicWithResponseForDevice(
              this.state.deviceid,
              '000000ff-0000-1000-8000-00805f9b34fb',
              '0000ff01-0000-1000-8000-00805f9b34fb',
              senddata
            )
            .then(characteristic => {
              // console.log(characteristic.value, 'characteristic');
              this.setState({ characteristicsUUID: characteristic.uuid });
              this.setState({ serviceUUID: characteristic.serviceUUID });
              // console.log(base64.decode(senddata),'settime');
            });
        });
      }
    });
  }

  calibration() {
    // console.log(this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected(this.state.deviceid)
      .then(res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics()
            .then(device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode('80');
              this.manager
                .writeCharacteristicWithResponseForDevice(
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then(characteristic => {
                  // console.log(characteristic.value, 'characteristic');
                  this.setState({ characteristicsUUID: characteristic.uuid });
                  this.setState({ serviceUUID: characteristic.serviceUUID });
                  // console.log(senddata,'calibration');
                })
                .then(device => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice(
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log(error.message);
                          return;
                        }
                        // console.log(characteristic.uuid, characteristic.value)
                        var buffer = Buffer.from(
                          characteristic.value,
                          'base64'
                        );
                        var dec = buffer.toString('hex');
                        var str = '';
                        for (var i = 0; i < dec.length; i += 2) {
                          var v = parseInt(dec.substr(i, 2), 16);
                          if (v) str += String.fromCharCode(v);
                        }
                        dec = str;
                        // console.log('******');
                        // console.log(dec);
                        this.setcurrentState(dec);
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch(err => {
                      console.log('4');
                      console.log(err);
                    });
                })
                .catch(err => {
                  console.log('3');
                  console.log(err.message);
                });
            })
            .catch(err => {
              console.log('2');
              console.log(err);
            });
        } else {
          this.manager.onDeviceDisconnected(device.id, (error, device) => {
            if (error) {
              console.log(error);
            }
            console.log('Device is disconnected');
            this.setState({ text1: 'Device Disconnected' });
          });
        }
      })
      .catch(err => {
        console.log('1');
        console.log(err);
      });
  }

  Logread() {
    // console.log(this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected(this.state.deviceid)
      .then(res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics()
            .then(device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode('90');
              this.manager
                .writeCharacteristicWithResponseForDevice(
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then(characteristic => {
                  // console.log(characteristic.value, 'characteristic');
                  this.setState({ characteristicsUUID: characteristic.uuid });
                  this.setState({ serviceUUID: characteristic.serviceUUID });
                  // console.log(senddata,'Logread');
                })
                .then(() => {
                  // console.log("2");
                  // console.log(device,'device')
                  this.manager
                    .monitorCharacteristicForDevice(
                      this.state.deviceid,
                      '000000ff-0000-1000-8000-00805f9b34fb',
                      '0000ff01-0000-1000-8000-00805f9b34fb',
                      (error, characteristic) => {
                        if (error) {
                          console.log(error.message);
                          return;
                        }
                        // console.log(characteristic.value);
                        // var buffer = Buffer.from(characteristic.value, 'base64');
                        // var dec = buffer.toString('hex');
                        // var str = '';
                        // for (var i = 0; i < dec.length; i += 2) {
                        //   var v = parseInt(dec.substr(i, 2), 16);
                        //   if (v) str += String.fromCharCode(v);
                        // }
                        // dec = str
                        var dec = base64.decode(characteristic.value);
                        // console.log('******');
                        // console.log(dec, this.state.count++);
                        // this.setState({logRead:dec})
                        // this.setState ({count: 1});
                        let newarray = this.state.arr.concat(dec);
                        this.setState({ arr: newarray });
                        const mynewstring = this.state.arr.toString();
                        // console.log (this.state.arr);
                        const folderPath =
                          RNFS.DocumentDirectoryPath + '/LogFiles';
                        RNFS.mkdir(folderPath).catch(err => {
                          console.log(err);
                        });
                        const path = folderPath + '/test1.txt';
                        RNFS.writeFile(path, mynewstring, 'utf8')
                          .then(() => {
                            console.log('WRITTEN');
                          })
                          .catch(error => {
                            console.log(error);
                          });
                        this.setState({ currentstate: dec });
                        // console.log(this.state.arr, 'arr')
                      }
                    )
                    .catch(err => {
                      console.log('4');
                      console.log(err);
                    });
                  this.setState({ isVisible: false })
                })
                .catch(err => {
                  console.log('3');
                  console.log(err.message);
                });
            })
            .catch(err => {
              console.log('2');
              console.log(err);
            });
        } else {
          this.manager.onDeviceDisconnected(device.id, (error, device) => {
            if (error) {
              console.log(error);
            }
            console.log('Device is disconnected');
            this.setState({ text1: 'Device Disconnected' });
          });
        }
      })
      .catch(err => {
        console.log('1');
        console.log(err);
      });
  }
  memoryReset() {
    // console.log(this.state.deviceid);
    const device = this.state.devices;
    this.manager
      .isDeviceConnected(this.state.deviceid)
      .then(res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics()
            .then(device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode('91');
              this.manager
                .writeCharacteristicWithResponseForDevice(
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                  senddata
                )
                .then(characteristic => {
                  // console.log(characteristic.value, 'characteristic');
                  this.setState({ characteristicsUUID: characteristic.uuid });
                  this.setState({ serviceUUID: characteristic.serviceUUID });
                  // console.log(senddata,'memset');
                });
            })
            .catch(err => {
              console.log('2');
              console.log(err);
            });
        } else {
          this.manager.onDeviceDisconnected(device.id, (error, device) => {
            if (error) {
              console.log(error);
            }
            console.log('Device is disconnected');
            this.setState({ text1: 'Device Disconnected' });
          });
        }
      })
      .catch(err => {
        console.log('1');
        console.log(err);
      });
  }

  OTAupdate() {
    // console.log(this.state.deviceid);
    // this.downloaddata();
    const device = this.state.devices;
    this.manager
      .isDeviceConnected(this.state.deviceid)
      .then(res => {
        // console.log(this.state.deviceid,"deviceid")
        // console.log(res,'res')
        if (res) {
          console.log('Discovering services and characteristics test');
          device
            .discoverAllServicesAndCharacteristics()
            .then(device => {
              // console.log("2");
              // console.log(device,'device')
              const senddata = base64.encode('66');
              // console.log(typeof(base64.encode(this.state.Imagedata.toString())))
              // const senddata = base64.encode(this.state.Imagedata.toString())
              this.manager
                .writeCharacteristicWithResponseForDevice(
                  this.state.deviceid,
                  '000000ff-0000-1000-8000-00805f9b34fb',
                  '0000ff01-0000-1000-8000-00805f9b34fb',
                   senddata
                )
                .then(characteristic => {
                  // console.log(characteristic.value, 'characteristic');
                  this.setState({ characteristicsUUID: characteristic.uuid });
                  this.setState({ serviceUUID: characteristic.serviceUUID });
                  console.log(base64.decode(senddata),"otaupdate");
                });
            })
            .catch(err => {
              console.log('2');
              console.log(err);
            });
        } else {
          this.manager.onDeviceDisconnected(device.id, (error, device) => {
            if (error) {
              console.log(error);
            }
            console.log('Device is disconnected');
            this.setState({ text1: 'Device Disconnected' });
          });
        }
      })
      .catch(err => {
        console.log('1');
        console.log(err);
      });
  }

  render() {
    // console.log (this.state.arr[0],'gahgcbniakzhcxn');
    // console.log(this.state.deviceid);
    return (
      <View style={{ flex: 1, backgroundColor: '#e8f5e9' }}>
        <View
          style={{
            justifyContent: 'center',
            height: hp('10%'),
            backgroundColor: '#006064',
          }}
        >
          <View style={{ flexDirection: 'row' }}>

            <Image
              style={{ width: wp('38%'), height: hp('3%'), marginLeft: 20 }}
              source={require('../assets/images/alcobrake.jpg')}
            />

          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: hp('2%'),
          }}
        >

          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginLeft: wp('3%'),
            }}
          >
            CONNECTED TO:
            {' '}
            {this.state.bluetoothState == 'PoweredOn'
              ? this.state.text1
              : 'NOT CONNECTED'}
          </Text>
          <Icon
            name={
              this.state.bluetoothState == 'PoweredOn'
                ? 'bluetooth'
                : 'bluetooth-off'
            }
            size={35}
            style={{
              color: this.state.bluetoothState == 'PoweredOn'
                ? 'steelblue'
                : '#9fa8da',
              textAlign: 'center',
              marginRight: wp('4%'),
            }}
          />
        </View>
        <View 
        // style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:hp('2%')}}
        >
        {/* {this.state.Content != [] ?
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Logread',{
            Itemid:1,
            logdata:this.state.Content
          })}>
            <Icon
              name={"clipboard-list"}
              size={38}
              style={{
                color: '#ef5350',
              marginLeft: wp('50%')
              }}
            />
            </TouchableOpacity>
            : null} */}
        <Image
            style={{
              width: wp('25%'),
              height: hp('3.5%'),
              marginLeft:wp('72%')
            }}
            source={this.state.sources1}
          // source={require('../assets/images/battery_30.png')}
          />
        </View>
        {/* <View style={{position: 'absolute', zIndex: 1, marginTop:hp('90%'),marginLeft:wp('73%')}}>
        <TouchableOpacity onPress={() => this.downloaddata()}>
          <Icon name="download-circle" size={70} color={'green'} />
        </TouchableOpacity>
        </View> */}
        {this.state.text1 == 'ALCOBRAKE300' &&
          this.state.bluetoothState == 'PoweredOn' &&
          this.state.deviceid ?
          <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            {this.state.currentstate == null ?
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  padding: 10,
                }}
              >
                <View style={{ position: 'absolute', zIndex: 1, justifyContent: 'center' }}>
                  <Spinner
                    // style={styles.spinner}
                    isVisible={this.state.isVisible}
                    size={120}
                    type="Circle"
                    color="#006064"
                  />
                </View>

                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: hp('-10%'),
                  }}
                >
                  <TouchableHighlight
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#ffb74d',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      marginRight: wp('20%'),
                      width: wp("50%"),
                      height: hp('8%')
                    }}
                    onPress={() => this.onTaketest()}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      TAKE TEST
                    </Text>
                  </TouchableHighlight>
                  <TouchableOpacity
                    onPress={() => this.setTime()}
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#9ccc65',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      width: wp("50%"),
                      height: hp('8%'),
                      marginLeft: wp('20%'),
                      marginTop: hp('2.5%')
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      TimeSet
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.calibration()}
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#81c784',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      marginRight: wp('20%'),
                      width: wp("50%"),
                      height: hp('8%'),
                      marginTop: hp('2.5%')
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      Calibration
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.Logread()}
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#ffab91',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      width: wp("50%"),
                      height: hp('8%'),
                      marginLeft: wp('20%'),
                      marginTop: hp('2.5%')
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      LogRead
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.memoryReset()}
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#ce93d8',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      marginRight: wp('20%'),
                      width: wp("50%"),
                      height: hp('8%'),
                      marginTop: hp('2.5%')
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      MemReset
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.OTAupdate()}
                    style={{
                      justifyContent: 'center',
                      backgroundColor: '#b39ddb',
                      borderTopRightRadius: 60,
                      borderBottomLeftRadius: 60,
                      marginLeft: wp('20%'),
                      width: wp("50%"),
                      height: hp('8%'),
                      marginTop: hp('2.5%')
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 20,
                        color: 'white',
                        textAlign: 'center',
                        fontWeight: 'bold',
                      }}
                    >
                      OTAUpdate
                    </Text>
                  </TouchableOpacity>
                </View>

              </View>
              : <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'column',
                }}
              >
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: hp('-1%')
                  }}
                >
                  <Text style={styles.titleText}>
                    {this.state.showtext}
                  </Text>
                  {this.state.currentstate == '0x84'
                    ? <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        flexDirection: 'column',
                        marginTop: hp('5%')
                      }}
                    >
                      <Text>
                        <Spinner
                          isVisible={true}
                          size={120}
                          type="Wave"
                          color="green"
                        />
                      </Text>
                      <View>
                        {this.state.imagebase64 == null
                          ? <RNCamera
                            ref={ref => (this.camera = ref)}
                            style={{
                              height: hp('35%'),
                              width: wp('50%'),
                              marginTop: hp('3%')
                            }}
                            type={this.state.cameratype}
                            captureAudio={false}
                            androidCameraPermissionOptions={{
                              title: 'Permission to use camera',
                              message: 'We need your permission to use your camera',
                              buttonPositive: 'Ok',
                              buttonNegative: 'Cancel',
                            }}
                          />
                          : <Image
                            style={{
                              height: hp('35%'),
                              width: wp('50%'), marginTop: hp('3%')
                            }}
                            source={{
                              uri: `data:image/jpg;base64,${this.state.imagebase64}`,
                            }}
                          />}
                      </View>
                    </View>
                    : <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: hp('-20%'),
                      }}
                    >
                      <Image
                        style={{
                          width: wp('70%'),
                          height: hp('60%'),
                          resizeMode: 'contain',
                        }}
                        source={this.state.sources}
                      />
                    </View>}
                </View>
              </View>}
          </View>
          : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.9,
    backgroundColor: '#F5FCFF',
  },
  head: { height: 40, backgroundColor: '#f1f8ff' },
  text: {
    margin: 6,
    textAlign: 'center',
    justifyContent: 'center'
  },
  titleText: {
    fontSize: 30,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color: 'green',
    marginTop: 20,
  }
});

export default Dashboard;
